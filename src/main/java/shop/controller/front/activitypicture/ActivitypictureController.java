/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.activitypicture;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.activitypicture.ActivitypictureService;
import shop.services.front.activitypicture.bean.Activitypicture;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**   
 * @类名称：ActivitypictureController
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:05:28      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/activitypicture")
public class ActivitypictureController extends FrontBaseController<Activitypicture> {
    @Resource
    private ActivitypictureService activitypictureService;
    @Override
    public Services<Activitypicture> getService() {
        return activitypictureService;
    }

    /**
     * 查询活动照片（品牌商，城市合伙人）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectPictureList")
    @ResponseBody
    public JSONResult selectPictureList(Activitypicture activitypicture) throws Exception {
        List<Activitypicture> rsList =activitypictureService.selectList(activitypicture);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

//    /**
//     * 查询门店列表（城市合伙人）
//     *
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping("/selectStoreList")
//    @ResponseBody
//    public JSONResult selectStoreList(Activitypicture activitypicture) throws Exception {
//        List<Activitypicture> rsList =activitypictureService.selectStoreList(activitypicture);
//        jsonResult = new JSONResult();
//        jsonResult.setData(rsList);
//        return jsonResult;
//    }

    /**
     * 反馈照片（ 城市合伙人）
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertActivitypicture", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertActivitypicture(Activitypicture activitypicture) throws Exception {
        jsonResult = new JSONResult();
        activitypictureService.insert(activitypicture);

        return jsonResult;
    }
}
