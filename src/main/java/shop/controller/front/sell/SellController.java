/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.front.sell;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.sell.SellService;
import shop.services.front.sell.bean.Sell;


import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @类名称：SellController
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:07:31
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/front/sell")
public class SellController extends FrontBaseController<Sell> {
    @Resource
    private SellService sellService;

    @Override
    public Services<Sell> getService() {
        return sellService;
    }


    /**
     * 跳转到个人中心地区页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toAreaList")
    public String toAreaList() {
        return "/front/sell/areaList";
    }

    /**
     * 跳转到个人中心合伙人页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toCityList")
    public String toCityList() {
        return "/front/sell/cityList";
    }

    /**
     * 跳转到个人中心门店页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toStoreList")
    public String toStoreList() {
        return "/front/sell/storeList";
    }

    /**
     * 跳转到个人中心商品页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toProductList")
    public String toProductList() {
        return "/front/sell/productList";
    }

    /**
     * 跳转到商品列表页面（根据商品查找）
     *
     * @return
     */
    @RequestMapping("/toAllProductList")
    public String toAllProductList() {
        return "/front/sell/allProductList";
    }

    /**
     * 跳转到地区列表页面（根据商品查找）
     *
     * @return
     */
    @RequestMapping("/toProductAreaList")
    public String toProductAreaList() {
        return "/front/sell/productAreaList";
    }

    /**
     * 跳转到合伙人列表页面（根据商品查找）
     *
     * @return
     */
    @RequestMapping("/toProductCityList")
    public String toProductCityList() {
        return "/front/sell/productCityList";
    }

    /**
     * 跳转到店铺列表页面（根据商品查找）
     *
     * @return
     */
    @RequestMapping("/toProductStoreList")
    public String toProductStoreList() {
        return "/front/sell/productStoreList";
    }


    /**
     * 查询总流水,销售量（品牌商）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectSalesAmount")
    @ResponseBody
    public JSONResult selectSalesAmount(Sell sell) throws Exception {
        //查询总流水,销售量，需要满足条件查询以及模糊查询
        List<Sell> rsList = sellService.selectSellList(sell);
        String salesAmount = "0";
        String salesVolume = "0";
        if (rsList!=null && !rsList.isEmpty()) {
            salesAmount = rsList.get(0).getSalesAmount();
            salesVolume = rsList.get(0).getSalesVolume();
        }
        Map<String, String> map = new HashMap<>();
        map.put("salesAmount", salesAmount);
        map.put("salesVolume", salesVolume);
        jsonResult = new JSONResult();
        jsonResult.setData(map);
        logger.debug("selectSalesAmount方法返回数据"+jsonResult.getData());
        return jsonResult;
    }

    /**
     * 查询当月总流水,销售量（品牌商）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectMonthSalesAmount")
    @ResponseBody
    public JSONResult selectMonthSalesAmount(Sell sell) throws Exception {
        List<Sell> rsList = sellService.selectMonthSellList(sell);
        String salesAmount = "0";
        String salesVolume = "0";
        if (rsList.get(0)!=null) {
            salesAmount = rsList.get(0).getSalesAmount();
            salesVolume = rsList.get(0).getSalesVolume();
        }
        Map<String, String> map = new HashMap<>();
        map.put("salesAmount", salesAmount);
        map.put("salesVolume", salesVolume);
        jsonResult = new JSONResult();
        jsonResult.setData(map);
        logger.debug("selectMonthSalesAmount方法返回数据"+jsonResult.getData());
        return jsonResult;
    }

    /**
     * 查询活动列表方法
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectActivityList")
    @ResponseBody
    public JSONResult selectActivityList(Sell sell) throws Exception {
        List<Sell> rsList = sellService.selectActivityList(sell);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 查询地区列表方法
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectAreaList")
    @ResponseBody
    public JSONResult selectAreaList(Sell sell) throws Exception {
        List<Sell> rsList = sellService.selectAreaList(sell);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        logger.debug("selectAreaList方法返回数据"+jsonResult.getData());
        return jsonResult;
    }

    /**
     * 查询合伙人列表方法
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectCityList")
    @ResponseBody
    public JSONResult selectCityList(Sell sell) throws Exception {
        List<Sell> rsList = sellService.selectCityList(sell);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 查询门店列表方法
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectStoreList")
    @ResponseBody
    public JSONResult selectStoreList(Sell sell) throws Exception {
        List<Sell> rsList = sellService.selectStoreList(sell);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 查询商品列表方法
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectProductList")
    @ResponseBody
    public JSONResult selectProductList(Sell sell) throws Exception {
        List<Sell> rsList = sellService.selectProductList(sell);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }
}
