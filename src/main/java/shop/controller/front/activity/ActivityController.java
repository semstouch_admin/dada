/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.front.activity;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.AccessToken;
import shop.core.common.bean.JSONResult;
import shop.core.plugins.bmap.BaiduMap;
import shop.core.plugins.wxlogin.WxContainer;
import shop.core.plugins.wxlogin.WxUrlType;
import shop.core.plugins.wxlogin.WxUtil;
import shop.core.plugins.wxmsg.WxMsgUtil;
import shop.core.plugins.wxpay.WXPayConstants;
import shop.core.plugins.wxpay.WXPayUtil;
import shop.core.util.MD5;
import shop.services.front.activity.ActivityService;
import shop.services.front.activity.bean.Activity;
import shop.services.front.activitycharge.ActivitychargeService;
import shop.services.front.activitycharge.bean.Activitycharge;
import shop.services.front.activityjoin.ActivityjoinService;
import shop.services.front.activityjoin.bean.Activityjoin;
import shop.services.front.kcrelation.KcrelationService;
import shop.services.front.kcrelation.bean.Kcrelation;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @类名称：ActivityController
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午19:53:49
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/front/activity")
public class ActivityController extends FrontBaseController<Activity> {
    @Resource
    private ActivityService activityService;
    @Resource
    private UserService userService;
    @Resource
    private KcrelationService kclationService;
    @Resource
    private ActivityjoinService activityjoinService;
    @Resource
    private ActivitychargeService activitychargeService;

    @Override
    public Services<Activity> getService() {
        return activityService;
    }


    /**
     * 跳转到微信选择账号页面
     *
     * @return
     */
    @RequestMapping("/toChoose")
    public String toChoose() {
        return "/front/loginChoose";
    }

    /**
     * 跳转到微信登陆页面
     *
     * @return
     */
    @RequestMapping("/toLogin")
    public String toLogin() {
        return "/front/login";
    }

    /**
     * 跳转到活动列表页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toActivityList")
    public String toActivityList() {
        return "/front/activity/activityList";
    }

    /**
     * 跳转到活动详情页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toActivityDetail")
    public String toActivityDetail() {
        return "/front/activity/activityDetail";
    }

    /**
     * 跳转到活动添加页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toActivityAdd")
    public String toActivityAdd() {
        return "/front/activity/activityAdd";
    }

    /**
     * 跳转到查看城市页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toAreaList")
    public String toAreaList() {
        return "/front/activity/areaList";
    }

    /**
     * 跳转到查看合伙人页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toCityManList")
    public String toCityManList() {
        return "/front/activity/cityManList";
    }

    /**
     * 跳转到查看门店页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toStoreList")
    public String toStoreList() {
        return "/front/activity/storeList";
    }

    /**
     * 跳转到查看照片页面（城市合伙人）
     *
     * @return
     */
    @RequestMapping("/toPictureList")
    public String toPictureList() {
        return "/front/activity/pictureList";
    }

    /**
     * 跳转到查看照片页面（品牌商）
     *
     * @return
     */
    @RequestMapping("/toBrandPictureList")
    public String toBrandPictureList() {
        return "/front/activity/brandPictureList";
    }

    /**
     * 跳转到编辑活动页面（KA经理）
     *
     * @return
     */
    @RequestMapping("/toActivityEdit")
    public String toActivityEdit() {
        return "/front/activity/activityEdit";
    }

    /**
     * 跳转到选择合伙人页面（KA经理）
     *
     * @return
     */
    @RequestMapping("/toChooseMan")
    public String toChooseMan() {
        return "/front/activity/chooseManList";
    }

    /**
     * 跳转到编辑合伙人页面（KA经理）
     *
     * @return
     */
    @RequestMapping("/toChooseManEdit")
    public String toChooseManEdit() {
        return "/front/activity/chooseManEdit";
    }

    /**
     * 跳转到查看门店页面（KA经理）
     *
     * @return
     */
    @RequestMapping("/toKaStoreList")
    public String toKaStoreList() {
        return "/front/activity/kaStoreList";
    }

    /**
     * 跳转到查看活动费用页面（KA经理）
     *
     * @return
     */
    @RequestMapping("/toActivityCharge")
    public String toActivityCharge() {
        return "/front/activity/activityCharge";
    }

    /**
     * 跳转到选择门店页面（城市合伙人）
     *
     * @return
     */
    @RequestMapping("/toCityStoreList")
    public String toCityStoreList() {
        return "/front/activity/partnerStoreList";
    }

    /**
     * 跳转到填写费用页面（城市合伙人）
     *
     * @return
     */
    @RequestMapping("/toActivityChargeAdd")
    public String toActivityChargeAdd() {
        return "/front/activity/addActivityCharge";
    }

    /**
     * 跳转到添加照片页面（城市合伙人）
     *
     * @return
     */
    @RequestMapping("/toAddPicture")
    public String toAddPicture() {
        return "/front/activity/addPicture";
    }

    /**
     * 跳转到签到页面（城市合伙人）
     *
     * @return
     */
    @RequestMapping("/toSignList")
    public String toSignList() {
        return "/front/sign/signList";
    }

    /**
     * 跳转到门店选择页面（KA经理）
     *
     * @return
     */
    @RequestMapping("/toKasList")
    public String toKasList() {
        return "/front/activity/kasList";
    }

    /**
     * 跳转到查看照片页面（KA经理）
     *
     * @return
     */
    @RequestMapping("/toKapList")
    public String toKapList() {
        return "/front/activity/kapList";
    }

    /**
     * 跳转到工作路径
     *
     * @return
     */
    @RequestMapping("/toWorkRoute")
    public String toWorkRoute() {
        return "/front/workRoute";
    }

    /**
     * 跳转到活动费用
     *
     * @return
     */
    @RequestMapping("/toActivityCost")
    public String toActivityCost() {
        return "/front/activityCost";
    }

    /**
     * 跳转到微信登陆选择页面
     *
     * @return
     */
    @RequestMapping("/toLoginChoose")
    public String toLoginChoose() {
        return "/front/loginChoose";
    }

    /**
     * 微信获取地理位置将gps坐标转化成物理地址
     *
     * @return
     */
    @RequestMapping("/getLocationAddress")
    @ResponseBody
    public JSONResult getLocationAddress(String lat, String lng) {
        jsonResult = new JSONResult();
        Map res = BaiduMap.geoconv(lat, lng);
        String address = BaiduMap.getLatlon(res.get("y").toString(), res.get("x").toString());
        jsonResult.setSuccess(true);
        logger.debug("地理位置为：" + address);
        jsonResult.setData(address);
        return jsonResult;
    }

    /**
     * 微信扫一扫测试
     *
     * @return
     */
    @RequestMapping("/toScan")
    @ResponseBody
    public JSONResult wxScan(String pageUrl) {
        jsonResult = new JSONResult();
        //随机生成签名的时间戳
        String timeStamp = String.valueOf(new Date().getTime() / 1000);
        //随机生成签名，使用UUID来获取唯一签名
        String nonceStr = UUID.randomUUID().toString().replace("-", "");
        Map resMap = new HashMap<>();
        try {
            Map<String, String> reqData = new HashMap<String, String>();
            reqData.put("jsapi_ticket", systemManager.getJsapiTicket());
            reqData.put("timestamp", timeStamp);
            reqData.put("noncestr", nonceStr);
            //微信上传图片所对应的接口，使用全部路径
            reqData.put("url", pageUrl);
            String signature = WXPayUtil.generateSignature(reqData, null, WXPayConstants.SignType.SHA1);
            resMap.put("appId", WxContainer.appid);
            resMap.put("timestamp", timeStamp);
            resMap.put("noncestr", nonceStr);
            resMap.put("signature", signature);
            logger.info("获取微信参数:" + resMap);
        } catch (Exception e) {
            logger.error("获取微信jsapi参数失败:" + e.toString());
            jsonResult.setSuccess(false);
            jsonResult.setMessage("获取微信jsapi参数失败");
            return jsonResult;
        }
        jsonResult.setSuccess(true);
        jsonResult.setData(resMap);
        return jsonResult;
    }


    /**
     * 微信授权登陆
     *
     * @return
     */
    @RequestMapping(value = "/toWx", method = RequestMethod.GET)
    public String toWx() {
        String appidUrl = "?appid=wx8c1fbead19c21f6d&";
        String redirect_uri = "redirect_uri=http://www.xiaomiaojia.cn/rest/front/activity/callBack&";
        String typeUrl = "response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
        return "redirect:" + WxUrlType.authorizeUrl + appidUrl + redirect_uri + typeUrl;
    }

    /**
     * 微信回调方法
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/callBack", method = RequestMethod.GET)
    public String callBack(HttpSession session, HttpServletRequest request) {
        String code = request.getParameter("code");
        Map<String, String> params = new HashMap<>();
        params.put("appid", WxContainer.appid);
        params.put("secret", WxContainer.secret);
        params.put("code", code);
        params.put("grant_type", "authorization_code");
        AccessToken accessToken = WxUtil.sendRequest(WxUrlType.accessTokenUrl, HttpMethod.GET, params, null, AccessToken.class);
        Map<String, String> getUserInfoParams = new HashMap<>();
        getUserInfoParams.put("access_token", accessToken.getAccess_token());
        getUserInfoParams.put("openid", accessToken.getOpenid());
        getUserInfoParams.put("lang", "zh_CN");
        Map<String, String> r = WxUtil.sendRequest(WxUrlType.userInfoUrl, HttpMethod.GET, getUserInfoParams, null, Map.class);
        logger.debug("微信登陆返回信息：" + r);
        String openID = accessToken.getOpenid();
        logger.debug("userOpenId：" + openID);
        session.setAttribute("userOpenId", openID);
        User user = userService.selectByOpenId(accessToken.getOpenid());
        if (user != null) {
            session.setAttribute("userInfo", user);
            return "redirect:/rest/front/activity/toLoginChoose";
        } else {
            return "redirect:/rest/front/activity/toLogin";
        }
    }

    /**
     * 微信登录方法
     *
     * @param user
     * @return 成功时返回用户信息 失败时返回失败信息
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult<User> login(User user, HttpSession session) {
        JSONResult<User> rs = new JSONResult<User>();
        if (StringUtils.isBlank(user.getUsername()) || StringUtils.isBlank(user.getPassword())) {
            rs.setMessage("账户和密码不能为空!");
            rs.setSuccess(false);
            return rs;
        }
        logger.debug("用户登录:{}", user.getUsername());
        user.setPassword(MD5.md5(user.getPassword()));
        User u = userService.selectOne(user);
        if (u == null) {
            rs.setMessage("登陆失败，账户或密码错误！");
            rs.setSuccess(false);
            logger.debug("登陆失败，账户或密码错误,{}", user.getUsername());
            return rs;
        }
        session.setAttribute("userInfo", u);
        if (session.getAttribute("userOpenId") != null) {
            if (!StringUtils.isBlank(u.getOpenID()) && !u.getOpenID().equals(session.getAttribute("userOpenId").toString())) {
                rs.setMessage("该微信号已与其他账号绑定，登录失败 ");
                rs.setSuccess(false);
                return rs;
            }
            u.setOpenID(session.getAttribute("userOpenId").toString());
            userService.update(u);
        }
        rs.setData(u);
        rs.setMessage("登录成功");
        rs.setSuccess(true);
        return rs;
    }

    /**
     * 获取当前登陆用户信息
     *
     * @param session
     * @return
     */
    @RequestMapping(value = "/getSession", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult getSession(HttpSession session) {
        JSONResult rs = new JSONResult();
        Map result = new HashMap<>();
        if (session.getAttribute("userInfo") != null) {
            result.put("user", session.getAttribute("userInfo"));
        } else {
            result.put("user", null);
        }
        rs.setData(result);
        rs.setSuccess(true);
        return rs;
    }

    /**
     * 发布活动（品牌商）
     *
     * @return jsonResult
     */
    @RequestMapping(value = "/insertActivity", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertActivity(HttpServletRequest request, Activity activity) throws Exception {
        jsonResult = new JSONResult();
        Activity rs = new Activity();
        rs.setKaName(activity.getKaName());
        rs = activityService.selectOne(rs);//查询该条记录
        if (rs != null) {//判断活动名称是否重复
            jsonResult.setSuccess(false);
            jsonResult.setMessage("活动名称重复，请重新填写！");
            return jsonResult;
        }
        User user = (User) request.getSession().getAttribute("userInfo");//获取登陆用户信息
        Kcrelation kcrelation = new Kcrelation();
        kcrelation.setBrandID(user.getId());
        List<Kcrelation> rsList = kclationService.selectList(kcrelation);//查询品牌商绑定的KA经理信息
        if (rsList != null) {
            kcrelation = rsList.get(0);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date2 = sdf.format(new Date());//日期格式化
        if (date2.compareTo(activity.getKaStarTime()) <= 0) {//判断活动是否提前一天发布
            activity.setBrandID(user.getId());
            activity.setKaID(kcrelation.getKaID());
            activity.setInsertStatus("1");
            activity.setUpdateStatus("0");//赋值
            getService().insert(activity);//插入活动表
            user.setIssueNumber(user.getIssueNumber() + 1);//更新品牌商发布活动次数
            userService.update(user);
            User ka = new User();
            ka.setId(kcrelation.getKaID());
            ka = userService.selectOne(ka);//查询绑定KA经理信息
            //发送微信模版消息
            Map<String, String> msgParam = new HashMap<>();
            msgParam.put("first", "尊敬的" + ka.getNickname() + "，您有一条新的活动信息请及时查收！");
            msgParam.put("keyword1", activity.getKaName());
            msgParam.put("keyword2", activity.getKaStarTime() + " 至 " + activity.getKaEndTime());
            msgParam.put("keyword3", "——");
            msgParam.put("keyword4", activity.getKaDemand());
            msgParam.put("remark", "请登录平台查看活动信息");
            WxMsgUtil.sendActivityStatusMsg(msgParam, systemManager.getAccessToken(), ka.getOpenID());
            return jsonResult;
        } else {
            jsonResult.setMessage("请提前一天发布活动！");
            jsonResult.setSuccess(false);
            return jsonResult;
        }
    }

    /**
     * 查询活动列表（品牌商，KA经理）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectActivityList")
    @ResponseBody
    public JSONResult selectActivityList(HttpServletRequest request, Activity activity) throws Exception {
        jsonResult = new JSONResult();
        User user = (User) request.getSession().getAttribute("userInfo");//获取登陆用户
        if ("2".equals(user.getRid())) {//判断用户角色
            activity.setBrandID(user.getId());//赋值
        } else if ("3".equals(user.getRid())) {//判断用户角色
            activity.setKaID(user.getId());//赋值
        } else {
        }
        List<Activity> rsList = getService().selectList(activity);//查询活动列表
        DecimalFormat df = new DecimalFormat("0.00");//格式化数据
        if (rsList.size() != 0) {//判断活动列表是否为空
            for (Activity activityInfo : rsList) {//遍历活动列表
                Activitycharge activitycharge = new Activitycharge();
                activitycharge.setActivityID(activityInfo.getId());//赋值
                activitycharge.setStatus("1");
                List<Activitycharge> rsList2 = activitychargeService.selectList(activitycharge);//查询该活动费用信息列表
                if (rsList2.size() != 0) {//判断费用列表是否为空
                    for (Activitycharge activitychargeInfo : rsList2) {//遍历费用列表
                        String[] personCosts = activitychargeInfo.getPersonCost().split(",");//分割人员费用
                        double personCostAmount = 0.00;
                        for (int i = 0; i < personCosts.length; i++) {
                            personCostAmount += Double.parseDouble(personCosts[i]);//计算人员费用总数
                        }
                        activitychargeInfo.setCostAmount(activitychargeInfo.getSiteCost() + activitychargeInfo.getTgCost() + personCostAmount);//计算单个门店费用总和
                    }
                    double chargeAmount = 0.00;
                    for (int i = 0; i < rsList2.size(); i++) {
                        chargeAmount += rsList2.get(i).getCostAmount();//计算参与该活动的门店费用总和
                    }
                    df.format(chargeAmount);//格式化数据
                    activityInfo.setChargeAmount(chargeAmount);//赋值
                } else {
                    activityInfo.setChargeAmount(0.00);//赋值
                }
            }
            jsonResult.setData(rsList);
            return jsonResult;
        }else{
            jsonResult.setData(rsList);
            return jsonResult;
        }
    }

    /**
     * 查询活动列表（城市合伙人）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectCityActivityList")
    @ResponseBody
    public JSONResult selectCityActivityList(HttpServletRequest request, Activity activity) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        activity.setCityID(user.getId());
        List<Activity> rsList = activityService.selectCityActivityList(activity);
        for (Activity activityInfo : rsList) {
            Activitycharge activitycharge = new Activitycharge();
            activitycharge.setActivityID(activityInfo.getId());
            activitycharge.setCityID(user.getId());
            activitycharge.setStatus("1");
            List<Activitycharge> list = activitychargeService.selectList(activitycharge);
            if (list.size() != 0) {
                activityInfo.setAcStatus("1");
            } else {
                activityInfo.setAcStatus("0");
            }
        }
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 查询活动详情（品牌商，KA经理）
     *
     * @param activity
     * @return
     */
    @RequestMapping(value = "/selectActivityDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectActivityDetail(HttpServletRequest request, Activity activity) {
        User user = (User) request.getSession().getAttribute("userInfo");//查询登陆用户
        activity = activityService.selectOne(activity);//查询该条活动记录
        Activityjoin activityjoin = new Activityjoin();
        activityjoin.setActivityID(activity.getId());//赋值
        activityjoin.setKaID(user.getId());//赋值
        List<Activityjoin> rsList = activityjoinService.selectList(activityjoin);//查询KA经理选择合伙人记录
        if (rsList.size() != 0) {
            activity.setCityStatus(rsList.get(0).getStatus());//给合伙人选择状态赋值
        } else {
        }
        jsonResult = new JSONResult();
        jsonResult.setData(activity);
        return jsonResult;
    }

    /**
     * 查询活动详情（城市合伙人）
     *
     * @param activity
     * @return
     */
    @RequestMapping(value = "/selectCityActivityDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectCityActivityDetail(HttpServletRequest request, Activity activity) {
        User user = (User) request.getSession().getAttribute("userInfo");//查询登陆用户
        activity = activityService.selectOne(activity);//查询该条活动记录
        Activitycharge activitycharge = new Activitycharge();
        activitycharge.setActivityID(activity.getId());//赋值
        activitycharge.setCityID(user.getId());//赋值
        activitycharge.setStatus("1");
        List<Activitycharge> rsList = activitychargeService.selectList(activitycharge);//查询KA经理选择合伙人记录
        if (rsList.size() != 0) {
            activity.setCityStatus("1");//给合伙人提交状态赋值
        } else {
            activity.setCityStatus("0");
        }
        jsonResult = new JSONResult();
        jsonResult.setData(activity);
        return jsonResult;
    }

    /**
     * 查询登录用户
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/selectUser", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectUser(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("userInfo");
        jsonResult = new JSONResult();
        jsonResult.setData(user);
        return jsonResult;
    }

    /**
     * 选择合伙人（KA经理）
     *
     * @param request
     * @param cityIDs
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/insertActivityjoin", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertActivityjoin(HttpServletRequest request, @RequestParam(value = "cityIDs[]") String[] cityIDs, String activityID) throws Exception {
        jsonResult = new JSONResult();
        User user = (User) request.getSession().getAttribute("userInfo");//获取登陆用户信息
        Kcrelation kcrelation = new Kcrelation();
        kcrelation.setKaID(user.getId());
        List<Kcrelation> kcList = kclationService.selectList(kcrelation);//查询KA经理绑定信息
        for (int i = 0; i < cityIDs.length; i++) {
            User rs = new User();
            rs.setId(cityIDs[i]);
            rs = userService.selectOne(rs);//获取KA经理选择的合伙人信息
            Activityjoin activityjoin = new Activityjoin();
            activityjoin.setArea(rs.getAddress());//合伙人所属城市
            activityjoin.setKaID(user.getId());//赋值
            activityjoin.setActivityID(activityID);//赋值
            activityjoin.setBrandID(kcList.get(0).getBrandID());//赋值brandID
            activityjoin.setCityID(cityIDs[i]);//赋值
            activityjoin.setStatus("0");//合伙人状态未提交
            Activityjoin aj = activityjoinService.selectOne(activityjoin);//查询该条记录
            if (aj == null) {//判断该条记录是否存在
                activityjoinService.insert(activityjoin);//插入活动参与表
                Activity activity = new Activity();
                activity.setId(activityID);
                activity = activityService.selectOne(activity);//查询该条活动信息
                //发送微信模版消息
                Map<String, String> msgParam = new HashMap<>();
                msgParam.put("first", "尊敬的" + rs.getNickname() + "，您有一条新的活动信息请及时查收！");
                msgParam.put("keyword1", activity.getKaName());
                msgParam.put("keyword2", activity.getKaStarTime() + " 至 " + activity.getKaEndTime());
                msgParam.put("keyword3", "——");
                msgParam.put("keyword4", activity.getKaDemand());
                msgParam.put("remark", "请登录平台查看活动信息");
                WxMsgUtil.sendActivityStatusMsg(msgParam, systemManager.getAccessToken(), rs.getOpenID());
            } else {
            }
        }
        return jsonResult;
    }

    /**
     * 编辑活动（KA经理）
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateActivity", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateActivity(Activity activity) throws Exception {
        jsonResult = new JSONResult();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date2 = sdf.format(new Date());//格式化时间
        activity.setUpdateStatus("1");//修改KA编辑状态
        if (!StringUtils.isBlank(activity.getKaEndTime())) {
            if (date2.compareTo(activity.getKaEndTime()) > 0) {//判断活动结束时间是否大于当前时间
                jsonResult.setMessage("活动结束时间不能小于当前时间，请重新编辑！");
                jsonResult.setSuccess(false);
                return jsonResult;
            }
        }
        getService().update(activity);//更新活动数据
        Activityjoin activityjoin = new Activityjoin();
        activityjoin.setKaID(activity.getKaID());
        activityjoin.setActivityID(activity.getId());
        List<Activityjoin> rsList = activityjoinService.selectList(activityjoin);//查询KA经理选择参与活动的合伙人
        for (Activityjoin rs : rsList) {
            rs.setStatus("1");//修改合伙人提交状态
            activityjoinService.update(rs);
        }
        return jsonResult;
    }
}
