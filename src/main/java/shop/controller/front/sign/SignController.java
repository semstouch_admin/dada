/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.sign;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.sign.SignService;
import shop.services.front.sign.bean.Sign;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**   
 * @类名称：SignController
 * @创建人：Ltz   
 * @创建时间：2017-09-12 下午21:35:30      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/sign")
public class SignController extends FrontBaseController<Sign> {
    @Resource
    private SignService signService;
    @Override
    public Services<Sign> getService() {
        return signService;
    }


    /**
     * 跳转到活动列表（打卡）
     *
     * @return
     */
    @RequestMapping("/toSignActivityList")
    public String toSignActivityList() {
        return "/front/sign/signActivityList";
    }

    /**
     * 跳转到门店列表（打卡）
     *
     * @return
     */
    @RequestMapping("/toSignStoreList")
    public String toSignStoreList() {
        return "/front/sign/signStoreList";
    }

    /**
     * 跳转到签到列表（打卡）
     *
     * @return
     */
    @RequestMapping("/toCitySignList")
    public String toCitySignList() {
        return "/front/sign/citySignList";
    }

    /**
     * 查询签到列表
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectSignList")
    @ResponseBody
    public JSONResult selectSignList(Sign sign) throws Exception {
        List<Sign> rsList = signService.selectSignList(sign);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }
}
