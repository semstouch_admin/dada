/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.front.activityjoin;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.core.plugins.wxmsg.WxMsgUtil;
import shop.services.front.activity.ActivityService;
import shop.services.front.activity.bean.Activity;
import shop.services.front.activityjoin.ActivityjoinService;
import shop.services.front.activityjoin.bean.Activityjoin;
import shop.services.front.kcrelation.KcrelationService;
import shop.services.front.kcrelation.bean.Kcrelation;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @类名称：ActivityjoinController
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:04:39      
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/front/activityjoin")
public class ActivityjoinController extends FrontBaseController<Activityjoin> {
    @Resource
    private ActivityjoinService activityjoinService;
    @Resource
    private KcrelationService kclationService;
    @Resource
    private UserService userService;
    @Resource
    private ActivityService activityService;

    @Override
    public Services<Activityjoin> getService() {
        return activityjoinService;
    }


    /**
     * 查看选择参与活动的合伙人（品牌商）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectCityChoose")
    @ResponseBody
    public JSONResult selectCityChoose(Activityjoin activityjoin) throws Exception {
        List<Activityjoin> rsList = activityjoinService.selectCityChoose(activityjoin);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }


    /**
     * KA经理再次编辑合伙人
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateCityList")
    @ResponseBody
    public JSONResult updateCityList(HttpServletRequest request, @RequestParam(value = "cityIDs[]") String[] cityIDs, String activityID) throws Exception {
        jsonResult = new JSONResult();
        User user = (User) request.getSession().getAttribute("userInfo");//获取KA经理信息
        Activityjoin activityjoin = new Activityjoin();
        activityjoin.setKaID(user.getId());
        activityjoin.setActivityID(activityID);
        List<Activityjoin> rsList = activityjoinService.selectList(activityjoin);//查询KA选择参与活动的合伙人
        if (rsList.size() != 0) {
            for (Activityjoin activityjoinInfo : rsList) {//循环遍历参与活动的合伙人
                activityjoinService.delete(activityjoinInfo);//删除记录
            }
        }
        Kcrelation kcrelation = new Kcrelation();
        kcrelation.setKaID(user.getId());
        List<Kcrelation> kcList = kclationService.selectList(kcrelation);//查询KA经理的绑定信息
        for (int i = 0; i < cityIDs.length; i++) {//循环遍历数组cityIDs
            User rs = new User();
            rs.setId(cityIDs[i]);
            rs = userService.selectOne(rs);//查询合伙人信息
            Activityjoin rs2 = new Activityjoin();
            rs2.setArea(rs.getAddress());//获取城市合伙人地址
            rs2.setKaID(user.getId());
            rs2.setActivityID(activityID);
            if (kcList.size() != 0) {
                rs2.setBrandID(kcList.get(0).getBrandID());//获取brandID
            }
            rs2.setCityID(cityIDs[i]);
            rs2.setStatus("1");//赋值
            Activityjoin aj = activityjoinService.selectOne(rs2);
            if (aj == null) {//判断该条记录是否存在
                activityjoinService.insert(rs2);//插入活动参与表
                User city = new User();
                city.setId(rs2.getCityID());
                city = userService.selectOne(city);//查询绑定品牌商信息
                Activity activity = new Activity();
                activity.setId(rs2.getActivityID());
                activity = activityService.selectOne(activity);//查询该条活动
                //发送微信模版消息
                Map<String, String> msgParam = new HashMap<>();
                msgParam.put("first", "尊敬的" + city.getNickname() + "，您有一条新的活动信息请及时查收！");
                msgParam.put("keyword1", activity.getKaName());
                msgParam.put("keyword2", activity.getKaStarTime() + " 至 " + activity.getKaEndTime());
                msgParam.put("keyword3", "——");
                msgParam.put("keyword4", activity.getKaDemand());
                msgParam.put("remark", "请登录平台查看活动信息");
                if (!StringUtils.isBlank(rs.getOpenID()))
                    WxMsgUtil.sendActivityStatusMsg(msgParam, systemManager.getAccessToken(), city.getOpenID());
            } else {
            }
        }
        return jsonResult;
    }
}
