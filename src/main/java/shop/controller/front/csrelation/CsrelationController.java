/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.csrelation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.activitycharge.ActivitychargeService;
import shop.services.front.activitycharge.bean.Activitycharge;
import shop.services.front.activitypicture.ActivitypictureService;
import shop.services.front.activitypicture.bean.Activitypicture;
import shop.services.front.csrelation.CsrelationService;
import shop.services.front.csrelation.bean.Csrelation;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**   
 * @类名称：CsrelationController
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:06:26      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/csrelation")
public class CsrelationController extends FrontBaseController<Csrelation> {
    @Resource
    private CsrelationService csrelationService;
    @Resource
    private ActivitychargeService activitychargeService;
    @Resource
    private ActivitypictureService activitypictureService;
    @Override
    public Services<Csrelation> getService() {
        return csrelationService;
    }



    /**
     * 选择门店（城市合伙人）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectStoreList")
    @ResponseBody
    public JSONResult selectStoreList(Csrelation csrelation,String activityID) throws Exception {
        List<Csrelation> rsList = csrelationService.selectList(csrelation);
        if(rsList.size()!=0){
            for(Csrelation csrelationInfo : rsList){
                Activitycharge activitycharge=new Activitycharge();
                activitycharge.setActivityID(activityID);
                activitycharge.setCityID(csrelationInfo.getCityID());
                activitycharge.setStoreID(csrelationInfo.getId());
                Activitycharge rs=activitychargeService.selectOne(activitycharge);
                if(rs!=null){
                    csrelationInfo.setAcStatus("1");
                }else{
                    csrelationInfo.setAcStatus("0");
                }
            }
        }
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }
}
