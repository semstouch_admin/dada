/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.front.kcrelation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.FrontBaseController;
import shop.core.Services;
import shop.core.common.bean.JSONResult;
import shop.services.front.kcrelation.KcrelationService;
import shop.services.front.kcrelation.bean.Kcrelation;
import shop.services.manage.system.bean.User;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**   
 * @类名称：KcrelationController
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:05      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/front/kcrelation")
public class KcrelationController extends FrontBaseController<Kcrelation> {
    @Resource
    private KcrelationService kcrelationService;
    @Override
    public Services<Kcrelation> getService() {
        return kcrelationService;
    }

    /**
     * 查询绑定的合伙人（KA经理）
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectCityList")
    @ResponseBody
    public JSONResult selectCityList(HttpServletRequest request,Kcrelation kcrelation) throws Exception {
        User user = (User) request.getSession().getAttribute("userInfo");
        kcrelation.setKaID(user.getId());
        List<Kcrelation> rsList = kcrelationService.selectCityList(kcrelation);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }
}
