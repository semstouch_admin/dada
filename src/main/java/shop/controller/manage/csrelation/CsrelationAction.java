/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.controller.manage.csrelation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.csrelation.CsrelationService;
import shop.services.manage.csrelation.bean.Csrelation;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @类名称：CsrelationAction
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:06:26
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/manage/csrelation/")
public class CsrelationAction extends BaseController<Csrelation> {
    private static final Logger logger = LoggerFactory.getLogger(CsrelationAction.class);
    @Autowired
    private CsrelationService csrelationService;

    @Autowired
    private UserService userService;

    private static final String page_toList = "/manage/csrelation/csrelationList";
    private static final String page_toEdit = "/manage/csrelation/csrelationEdit";
    private static final String page_toAdd = "/manage/csrelation/csrelationAdd";

    public CsrelationService getService() {
        return csrelationService;
    }

    private CsrelationAction() {
        super.page_toList = page_toList;
        super.page_toAdd = page_toAdd;
        super.page_toEdit = page_toEdit;
    }


    /**
     * 查询单个对象跳转到编辑页面
     *
     * @param e
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    @RequestMapping("toEdit")
    public String toEdit(@ModelAttribute("e") Csrelation e, ModelMap model) throws Exception {
        e = getService().selectThis(e);
        model.addAttribute("e", e);
        return page_toEdit;
    }


    /**
     * 根据cityID批量删除
     *
     * @return
     * @throws Exception
     */
    @Override
    @RequestMapping(value = "deletesJson", method = RequestMethod.POST)
    @ResponseBody
    public String deletesJson(HttpServletRequest request, @RequestParam(value = "ids[]") String[] ids, @ModelAttribute("e") Csrelation e, RedirectAttributes flushAttrs) throws Exception {
        getService().deletesByCityId(ids);
        return "success";
    }

    /**
     * 查询城市合伙人对应的门店
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectListByCityID")
    @ResponseBody
    public JSONResult selectListByCityID(HttpServletRequest request, Csrelation e) throws Exception {
        List<Csrelation> rsList = getService().selectListByCityID(e);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 查询没有绑定城市合伙人的门店
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectWithoutCtiyID")
    @ResponseBody
    public JSONResult selectWithoutCtiyID(HttpServletRequest request, @ModelAttribute("e") Csrelation e) throws Exception {
        List<Csrelation> rsList = csrelationService.selectnoCityList(e);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 批量更新
     * */
    @RequestMapping(value = "updateAllCityID", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult changeStatus(HttpServletRequest request, @RequestParam(value = "ids[]") String[] ids) {
        jsonResult = new JSONResult();
        String addCityID = request.getParameter("cityIDs");
        String address = request.getParameter("CityAddress");
        User user = userService.selectById(addCityID);
        user.setAddress(address);
        userService.update(user);
        Csrelation csrelation = new Csrelation();
        csrelationService.updateAllCityID(ids, addCityID, csrelation.getCityID());
        return jsonResult;
    }


    /**
     * 分页获取数据列表
     *
     * @param request 请求
     * @param e       对象参数
     * @return 分页数据模型
     */
    @Override
    @RequestMapping("loadData")
    @ResponseBody
    public PagerModel loadData(HttpServletRequest request, Csrelation e) {
        int offset = 0;
        int pageSize = 10;
        if (request.getParameter("start") != null) {
            offset = Integer.parseInt(request.getParameter("start"));
        }
        if (request.getParameter("length") != null) {
            pageSize = Integer.parseInt(request.getParameter("length"));
        }
        if (offset < 0)
            offset = 0;
        if (pageSize < 0) {
            pageSize = 10;
        }
        e.setOffset(offset);
        e.setPageSize(pageSize);
        PagerModel pager = getService().selectPageListFirst(e);
        pager.setRecordsTotal(pager.getTotal());
        pager.setRecordsFiltered(pager.getTotal());
        return pager;
    }

    /**
     * 异步表单验证，验证门店号是否已经存在
     * @param e
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "toValid",method = RequestMethod.POST)
    @ResponseBody
    public JSONResult toValid(Csrelation e) throws Exception{
        e=getService().selectOne(e);
        JSONResult jsonResult=new JSONResult();
        jsonResult.setData(e);
        return jsonResult;
    }
}
