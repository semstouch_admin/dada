/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @类名称：AccountAction
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午19:53:49      
 * @版本号：1.0
 * @描述：
 */
@Controller
@RequestMapping("/manage/account/")
public class AccountAction extends BaseController<User> {
    private static final Logger logger = LoggerFactory.getLogger(AccountAction.class);
    @Autowired
    private UserService userService;
    private static final String page_toList = "/manage/account/accountLists";
    public UserService getService() {
        return userService;
    }

    private AccountAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }

    /**
     * 查询所有城市合伙人
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectAllCity")
    @ResponseBody
    public JSONResult selectAllCity(HttpServletRequest request, @ModelAttribute("e") User e) throws Exception {
        List<User> rsList = getService().selectAllCity(e);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 获取品牌商列表
     * */
    @RequestMapping("/selectAllBrand")
    @ResponseBody
    public JSONResult selectAllBrand(User e) throws Exception {
        List<User> rsList =userService.selectAllBrand(e);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

}