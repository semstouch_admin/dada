/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.kcrelation;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.services.manage.kcrelation.KcrelationService;
import shop.services.manage.kcrelation.bean.Kcrelation;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**   
 * @类名称：KcrelationAction      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:05      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/kcrelation/")
public class KcrelationAction extends BaseController<Kcrelation> {
    private static final Logger logger = LoggerFactory.getLogger(KcrelationAction.class);
    @Autowired
    private KcrelationService kcrelationService;
    @Autowired
    private UserService userService;
    private static final String page_toList = "/manage/kcrelation/kcrelationList";
    public KcrelationService getService() {
        return kcrelationService;
    }

    private KcrelationAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }

    /**
     * KA经理查询绑定的合伙人
     *
     * @param kcrelation
     * @return
     * @throws Exception
     */
    @RequestMapping("selectCityList")
    @ResponseBody
    public JSONResult selectCityList(Kcrelation kcrelation) throws Exception {
        List<Kcrelation> rsList = kcrelationService.selectCityList(kcrelation);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 合伙人查询绑定的KA经理
     *
     * @param kcrelation
     * @return
     * @throws Exception
     */
    @RequestMapping("selectKaList")
    @ResponseBody
    public JSONResult selectKaList(Kcrelation kcrelation) throws Exception {
        List<Kcrelation> rsList = kcrelationService.selectKaList(kcrelation);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }


    /**
     * 查询未绑定的用户
     *
     * @param user
     * @return
     * @throws Exception
     */
    @RequestMapping("selectUnBindList")
    @ResponseBody
    public JSONResult selectUnBindList(User user) throws Exception {
        List<User> rsList = userService.selectList(user);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 批量绑定合伙人
     *
     * @param kcrelation（kaID,brandID）
     * @param cityIDs
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "insertKcrelations", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertKcrelations(Kcrelation kcrelation,@RequestParam(value = "cityIDs[]")String[] cityIDs) throws Exception {
        jsonResult = new JSONResult();
        for(int i=0;i<cityIDs.length;i++){//遍历数组
            kcrelation.setCityID(cityIDs[i]);//赋值
            Kcrelation rs=new Kcrelation();
            rs.setKaID(kcrelation.getKaID());
            rs.setBrandID(kcrelation.getBrandID());
            if(StringUtils.isBlank(kcrelation.getBrandID())){
                jsonResult.setSuccess(false);
                jsonResult.setMessage("请先绑定品牌商！");
                return jsonResult;
            }else {
                rs.setCityID(cityIDs[i]);
                rs = kcrelationService.selectOne(rs);//查询该条记录
                if (rs == null) {//判断该条记录是否存在
                    kcrelationService.insert(kcrelation);//插入关系表
                    User user = new User();
                    user.setId(kcrelation.getCityID());
                    user = userService.selectOne(user);
                    user.setBindStatus("1");
                    userService.update(user);//修改绑定状态
                    User user2 = new User();
                    user2.setId(kcrelation.getKaID());
                    user2 = userService.selectOne(user2);
                    user2.setBindStatus("1");
                    userService.update(user2);//修改绑定状态
                    User user3 = new User();
                    user3.setId(kcrelation.getBrandID());
                    user3 = userService.selectOne(user3);
                    user3.setBindStatus("1");
                    userService.update(user3);//修改绑定状态
                } else {
                }
            }
        }
        return jsonResult;
    }

    /**
     * 批量解绑合伙人
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "deleteKcrelations", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteKcrelations(Kcrelation kcrelation,@RequestParam(value = "cityIDs[]") String[] cityIDs) throws Exception {
        jsonResult = new JSONResult();
        for(int i=0;i<cityIDs.length;i++){//遍历数组
            kcrelation.setCityID(cityIDs[i]);//赋值
            Kcrelation rs=new Kcrelation();
            rs.setKaID(kcrelation.getKaID());
            rs.setBrandID(kcrelation.getBrandID());
            rs.setCityID(cityIDs[i]);
            rs=kcrelationService.selectOne(rs);//查询该条记录
            if(rs!=null){//判断该条记录是否存在
                kcrelationService.delete(rs);//删除该条记录
                Kcrelation rs2=new Kcrelation();
                rs2.setKaID(rs.getKaID());
                rs2.setBrandID(rs.getBrandID());
                List<Kcrelation> rsList=kcrelationService.selectList(rs2);//查找KA绑定关系
                Kcrelation rs3=new Kcrelation();
                rs3.setCityID(rs.getCityID());
                List<Kcrelation> rsList2=kcrelationService.selectList(rs3);//查找城市合伙人绑定关系
                if(rsList.size()==0){//判断KA经理绑定状态
                    User user =new User();
                    user.setId(kcrelation.getKaID());
                    user=userService.selectOne(user);
                    user.setBindStatus("0");
                    userService.update(user);//修改绑定状态
                    User user2 =new User();
                    user2.setId(kcrelation.getBrandID());
                    user2=userService.selectOne(user2);
                    user2.setBindStatus("0");
                    userService.update(user2);//修改绑定状态
                }else{}
                if(rsList2.size()==0){//判断城市合伙人绑定状态
                    User user3=new User();
                    user3.setId(kcrelation.getCityID());
                    user3=userService.selectOne(user3);
                    user3.setBindStatus("0");
                    userService.update(user3);//修改绑定状态
                }else{}
            }else{}
        }
        return jsonResult;
    }

    /**
     * 批量绑定KA经理
     *
     * @param kcrelation
     * @param kaIDs
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "insertKcrelation", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult insertKcrelation(Kcrelation kcrelation,@RequestParam(value = "kaIDs[]")String[] kaIDs) throws Exception {
        jsonResult = new JSONResult();
        for (int i = 0; i < kaIDs.length; i++) {//遍历数组
            kcrelation.setKaID(kaIDs[i]);//获取kaID
            Kcrelation rs = new Kcrelation();
            rs.setKaID(kaIDs[i]);
            List<Kcrelation> rsList = kcrelationService.selectList(rs);
            rs.setBrandID(rsList.get(0).getBrandID());//获取brandID
            rs.setCityID(kcrelation.getCityID());
            rs=kcrelationService.selectOne(rs);//查询该条记录
            if(rs==null){//判断该条记录是否存在
                kcrelation.setBrandID(rsList.get(0).getBrandID());//获取brandID
                kcrelationService.insert(kcrelation);//插入记录
                User user =new User();
                User user2=new User();
                User user3=new User();
                user.setId(kcrelation.getCityID());
                user=userService.selectOne(user);
                user.setBindStatus("1");
                userService.update(user);//修改绑定状态
                user2.setId(kcrelation.getKaID());
                user2=userService.selectOne(user2);
                user2.setBindStatus("1");
                userService.update(user2);//修改绑定状态
                user3.setId(kcrelation.getBrandID());
                user3=userService.selectOne(user3);
                user3.setBindStatus("1");
                userService.update(user3);//修改绑定状态
            }else{}
        }
        return jsonResult;
    }

    /**
     * 批量解绑KA经理
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "deleteKcrelation", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult deleteKcrelation(Kcrelation kcrelation,@RequestParam(value = "kaIDs[]") String[] kaIDs) throws Exception {
        jsonResult = new JSONResult();
        for(int i=0;i<kaIDs.length;i++){//遍历数组
            kcrelation.setKaID(kaIDs[i]);//赋值
            kcrelation =kcrelationService.selectOne(kcrelation);
            if(kcrelation!=null){//判断该条记录是否存在
                kcrelationService.delete(kcrelation);
                Kcrelation rs=new Kcrelation();
                rs.setKaID(kcrelation.getKaID());
                List<Kcrelation> rsList=kcrelationService.selectList(rs);//查找KA绑定关系
                Kcrelation rs2=new Kcrelation();
                rs2.setCityID(kcrelation.getCityID());
                List<Kcrelation> rsList2=kcrelationService.selectList(rs2);//查找城市合伙人绑定关系
                if(rsList.size()==0){//判断KA经理绑定状态
                    User user =new User();
                    user.setId(kcrelation.getKaID());
                    user=userService.selectOne(user);
                    user.setBindStatus("0");
                    userService.update(user);//修改绑定状态
                    User user2 =new User();
                    user2.setId(kcrelation.getBrandID());
                    user2=userService.selectOne(user2);
                    user2.setBindStatus("0");
                    userService.update(user2);//修改绑定状态
                }else{}
                if(rsList2.size()==0){//判断城市合伙人绑定状态
                    User user3 =new User();
                    user3.setId(kcrelation.getCityID());
                    user3=userService.selectOne(user3);
                    user3.setBindStatus("0");
                    userService.update(user3);//修改绑定状态
                }
            }else{}
        }
        return jsonResult;
    }

}
