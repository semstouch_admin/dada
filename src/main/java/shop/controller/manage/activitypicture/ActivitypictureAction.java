/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.activitypicture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.BaseController;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.activitypicture.ActivitypictureService;
import shop.services.manage.activitypicture.bean.Activitypicture;

import javax.servlet.http.HttpServletRequest;

/**   
 * @类名称：ActivitypictureAction      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:05:28      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/activitypicture/")
public class ActivitypictureAction extends BaseController<Activitypicture> {
    private static final Logger logger = LoggerFactory.getLogger(ActivitypictureAction.class);
    @Autowired
    private ActivitypictureService activitypictureService;
    private static final String page_toList = "/manage/activitypicture/activitypictureList";
    public ActivitypictureService getService() {
        return activitypictureService;
    }

    private ActivitypictureAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }
}
