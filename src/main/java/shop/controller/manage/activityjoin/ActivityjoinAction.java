/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.activityjoin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.core.BaseController;
import shop.services.manage.activityjoin.ActivityjoinService;
import shop.services.manage.activityjoin.bean.Activityjoin;

/**   
 * @类名称：ActivityjoinAction      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:04:39      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/activityjoin/")
public class ActivityjoinAction extends BaseController<Activityjoin> {
    private static final Logger logger = LoggerFactory.getLogger(ActivityjoinAction.class);
    @Autowired
    private ActivityjoinService activityjoinService;
    private static final String page_toList = "/manage/activityjoin/activityjoinList";
    public ActivityjoinService getService() {
        return activityjoinService;
    }

    private ActivityjoinAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }
}
