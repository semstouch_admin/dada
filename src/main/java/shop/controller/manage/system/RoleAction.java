package shop.controller.manage.system;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import shop.core.BaseController;
import shop.core.Services;
import shop.core.common.bean.ManageContainer;
import shop.core.common.exception.NotThisMethod;
import shop.services.manage.system.bean.Role;
import shop.services.manage.system.impl.RoleService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 角色action
 *
 * @author huangf
 * @author jeeshopfans
 */
@Controller
@RequestMapping("/manage/role")
public class RoleAction extends BaseController<Role> {
    @Resource
    private RoleService roleService;

    public RoleAction() {
        super.page_toList = "/manage/system/role/roleList";
        super.page_toEdit = "/manage/system/role/editRole";
        super.page_toAdd = "/manage/system/role/addRole";
    }

    @Override
    @RequestMapping(value = "deletes", method = RequestMethod.POST)
    public String deletes(HttpServletRequest request, String[] ids, @ModelAttribute("e") Role e, RedirectAttributes flushAttrs) throws Exception {
        throw new NotThisMethod(ManageContainer.not_this_method);
    }

    @Override
    public Services<Role> getService() {
        return this.roleService;
    }

}
