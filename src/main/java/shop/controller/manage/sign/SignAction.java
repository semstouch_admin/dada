/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.sign;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.BaseController;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.sign.SignService;
import shop.services.manage.sign.bean.Sign;

import javax.servlet.http.HttpServletRequest;

/**   
 * @类名称：SignAction      
 * @创建人：Ltz   
 * @创建时间：2017-09-12 下午21:35:30      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/sign/")
public class SignAction extends BaseController<Sign> {
    private static final Logger logger = LoggerFactory.getLogger(SignAction.class);
    @Autowired
    private SignService signService;
    private static final String page_toList = "/manage/salesman/salesmanList";
    public SignService getService() {
        return signService;
    }

    private SignAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }


    /**
     * 查询签到列表
     *
     * @param request 请求
     * @param e       对象参数
     * @return 分页数据模型
     */
    @RequestMapping("loadSignData")
    @ResponseBody
    public PagerModel loadSignData(HttpServletRequest request, Sign e) {
        int offset = 0;
        int pageSize = 10;
        if (request.getParameter("start") != null) {
            offset = Integer.parseInt(request.getParameter("start"));
        }
        if (request.getParameter("length") != null) {
            pageSize = Integer.parseInt(request.getParameter("length"));
        }
        if (offset < 0)
            offset = 0;
        if (pageSize < 0) {
            pageSize = 10;
        }
        e.setOffset(offset);
        e.setPageSize(pageSize);
        PagerModel pager = signService.selectSignPageList(e);
        pager.setRecordsTotal(pager.getTotal());
        pager.setRecordsFiltered(pager.getTotal());
        return pager;
    }
}
