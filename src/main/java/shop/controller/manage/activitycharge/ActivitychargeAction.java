/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.controller.manage.activitycharge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.BaseController;
import shop.core.common.bean.JSONResult;
import shop.services.manage.activitycharge.ActivitychargeService;
import shop.services.manage.activitycharge.bean.Activitycharge;
import shop.services.manage.activitypicture.ActivitypictureService;
import shop.services.manage.activitypicture.bean.Activitypicture;

import java.util.List;

/**   
 * @类名称：ActivitychargeAction      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午21:58:55      
 * @版本号：1.0
 * @描述：     
 */
@Controller
@RequestMapping("/manage/activitycharge/")
public class ActivitychargeAction extends BaseController<Activitycharge> {
    private static final Logger logger = LoggerFactory.getLogger(ActivitychargeAction.class);
    @Autowired
    private ActivitychargeService activitychargeService;
    @Autowired
    private ActivitypictureService activitypictureService;
    private static final String page_toList = "/manage/activitycharge/activitychargeList";
    public ActivitychargeService getService() {
        return activitychargeService;
    }

    private ActivitychargeAction(){
        super.page_toList = page_toList;
        super.page_toAdd = null;
        super.page_toEdit = null;
    }


    /**
     * 查询参与门店列表
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("selectStoreList")
    @ResponseBody
    public JSONResult selectStoreList(Activitycharge activitycharge) throws Exception {
        List<Activitycharge> rsList = activitychargeService.selectStoreList(activitycharge);
        if(rsList.size()!=0){
            for(Activitycharge activitychargeInfo  :rsList){
                Activitypicture activitypicture=new Activitypicture();
                activitypicture.setActivityID(activitychargeInfo.getActivityID());
                activitypicture.setCityID(activitychargeInfo.getCityID());
                activitypicture.setStoreID(activitychargeInfo.getStoreID());
                List<Activitypicture> rsList2=activitypictureService.selectList(activitypicture);
                if(rsList2.size()!=0){
                    activitychargeInfo.setPictureNumber(String.valueOf(rsList2.size()));
                }else{
                    activitychargeInfo.setPictureNumber("0");
                }
            }
        }

        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }

    /**
     * 查询活动费用信息
     * @param activitycharge
     * @return
     */
    @RequestMapping(value = "/selectActivitychargeDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectActivitychargeDetail(Activitycharge activitycharge) {
        activitycharge =  activitychargeService.selectOne(activitycharge);
        jsonResult = new JSONResult();
        jsonResult.setData(activitycharge);
        return jsonResult;
    }

}
