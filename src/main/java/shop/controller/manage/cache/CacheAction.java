package shop.controller.manage.cache;

import shop.core.common.oscache.FrontCache;
import shop.core.common.oscache.ManageCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 缓存管理
 * @author lintz
 * */
@Controller
@RequestMapping("/manage/cache/")
public class CacheAction {
    private static final Logger logger = LoggerFactory.getLogger(CacheAction.class);
    private static final String page_toIndex = "/manage/cache/cacheIndex";


    @RequestMapping(method = RequestMethod.GET)
    public String toIndex() throws Exception {
        return page_toIndex;
    }

    @RequestMapping(value = "loadAllCache", method = RequestMethod.GET)
    @ResponseBody
    public String loadAllCache(HttpServletRequest request) throws Exception{
        WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        ManageCache manageCache = (ManageCache) app.getBean("manageCache");
        manageCache.loadAllCache();
        logger.info("后台缓存更新成功");
        return "success";
    }

    @RequestMapping(value = "frontCache", method = RequestMethod.GET)
    @ResponseBody
    public String frontCache(HttpServletRequest request){
        WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        FrontCache frontCache = (FrontCache) app.getBean("frontCache");
        try{
            frontCache.loadAllCache();
        }catch(Exception e){
            e.printStackTrace();
        }
        return "success";
    }

    @RequestMapping(value = "wxTokenCache", method = RequestMethod.GET)
    @ResponseBody
    public String wxTokenCache(HttpServletRequest request){
        WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        ManageCache manageCache = (ManageCache) app.getBean("manageCache");
        try{
            manageCache.loadAccessToken();
        }catch(Exception e){
            e.printStackTrace();
        }
        return "success";
    }

    @RequestMapping(value = "wxJsApiCache", method = RequestMethod.GET)
    @ResponseBody
    public String wxJsApiCache(HttpServletRequest request){
        WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        ManageCache manageCache = (ManageCache) app.getBean("manageCache");
        try{
            manageCache.loadJsapiTicket();
        }catch(Exception e){
            e.printStackTrace();
        }
        return "success";
    }

}
