package shop.controller.common;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by lee on 2017/9/10.
 */
public class myMultipartResolver extends org.springframework.web.multipart.commons.CommonsMultipartResolver {
    @Override
    public boolean isMultipart(HttpServletRequest request) {
        if (request.getRequestURI().contains("rest/manage/ued/config")){
            return false;
        }else {
            return super.isMultipart(request);
        }
    }
}
