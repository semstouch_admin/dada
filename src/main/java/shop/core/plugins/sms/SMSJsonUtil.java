package shop.core.plugins.sms;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;

public class SMSJsonUtil {
    private static final Logger logger = LoggerFactory.getLogger(SMSJsonUtil.class);

	public static String toJson(Object obj, Type type) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(obj);
        }catch (Exception e){
            logger.error("类型转换失败："+e);
        }
        return "";
	}

	public static Object fromJson(String str, Class type) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(str,type);
        }catch (Exception e){
            logger.error("类型转换失败："+e);
        }
		return null;
	}


}
