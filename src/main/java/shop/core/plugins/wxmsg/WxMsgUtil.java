package shop.core.plugins.wxmsg;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import shop.core.common.bean.AccessToken;
import shop.core.plugins.wxlogin.WxUrlType;
import shop.core.plugins.wxlogin.WxUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description  微信发送模板消息
 * @Author semstouch
 * @Date 2017/7/16
 **/
public class WxMsgUtil {
    protected static final Logger logger = LoggerFactory.getLogger(WxMsgUtil.class);

    /**
     * 发送微信信息模版案例
     * @param param  要发送的信息内容
     * @param accessToken  微信公众号accessToken
     * @param openid    需要接收的微信号openid
     * @return
     */
    public static boolean sendActivityStatusMsg(Map<String,String> param,AccessToken accessToken,String openid){
        String templateId="B9LAHwtCsD-00XXmXiECxbH4MkBxRnBeSOo7aucWAlM";   //消息模板编号
        Map params = new HashMap();
        params.put("touser",openid);
        params.put("template_id",templateId);
        params.put("url","http://www.xiaomiaojia.cn/rest/front/activity/toActivityList");

        Map msgParam = new HashMap();
        //处理一下内容参数，将内容从String转成map格式
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Map<String,String> keyMap= new HashMap();
            keyMap.put("value",entry.getValue());
            keyMap.put("color","#173177");
            msgParam.put(entry.getKey(),keyMap);
        }

        Map<String,String> getParams = new HashMap<String,String>();
        getParams.put("access_token",accessToken.getAccess_token());
        params.put("data",msgParam);
        String postData="";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData= objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果："+ wxRespCodeEntity.getErrcode());
        }catch (Exception e){
            logger.error("",e);
        }
        return true;
    }

    public static boolean sendSellStatusMsg(Map<String,String> param,AccessToken accessToken,String openid){
        String templateId="hiUxsnCYeLPjgmM3NIvnBt5DNLg2h7-_pr94VGOcHrw";   //消息模板编号
        Map params = new HashMap();
        params.put("touser",openid);
        params.put("template_id",templateId);
        params.put("url","http://www.xiaomiaojia.cn/rest/front/sell/toAreaList");

        Map msgParam = new HashMap();
        //处理一下内容参数，将内容从String转成map格式
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Map<String,String> keyMap= new HashMap();
            keyMap.put("value",entry.getValue());
            keyMap.put("color","#173177");
            msgParam.put(entry.getKey(),keyMap);
        }

        Map<String,String> getParams = new HashMap<String,String>();
        getParams.put("access_token",accessToken.getAccess_token());
        params.put("data",msgParam);
        String postData="";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData= objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果："+ wxRespCodeEntity.getErrcode());
        }catch (Exception e){
            logger.error("",e);
        }
        return true;
    }

    public static boolean sendCitySellStatusMsg(Map<String,String> param,AccessToken accessToken,String openid){
        String templateId="Q8SnAt5wQJ4vyfSS_NcThUhsbwjLOLNFFf6xvbkJ2eo";   //消息模板编号
        Map params = new HashMap();
        params.put("touser",openid);
        params.put("template_id",templateId);
        params.put("url","http://www.xiaomiaojia.cn/rest/front/sell/toStoreList");

        Map msgParam = new HashMap();
        //处理一下内容参数，将内容从String转成map格式
        for (Map.Entry<String, String> entry : param.entrySet()) {
            Map<String,String> keyMap= new HashMap();
            keyMap.put("value",entry.getValue());
            keyMap.put("color","#173177");
            msgParam.put(entry.getKey(),keyMap);
        }

        Map<String,String> getParams = new HashMap<String,String>();
        getParams.put("access_token",accessToken.getAccess_token());
        params.put("data",msgParam);
        String postData="";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            postData= objectMapper.writeValueAsString(params);
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果："+ wxRespCodeEntity.getErrcode());
        }catch (Exception e){
            logger.error("",e);
        }
        return true;
    }

}
