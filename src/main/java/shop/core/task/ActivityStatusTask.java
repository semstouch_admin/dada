package shop.core.task;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import shop.core.common.oscache.SystemManager;
import shop.core.plugins.wxmsg.WxMsgUtil;
import shop.services.front.activity.ActivityService;
import shop.services.front.activity.bean.Activity;
import shop.services.manage.system.bean.User;
import shop.services.manage.system.impl.UserService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wzl on 2017/7/25.
 */
@Component
public class ActivityStatusTask implements Runnable {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ActivityStatusTask.class);
    @Autowired
    private ActivityService activityService;
    @Autowired
    private UserService userService;

    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    @Override
    public void run() {
        Activity activity = new Activity();
        List<Activity> activityList = activityService.selectList(activity);
        if (activityList != null) {
            logger.error("activityList=" + activityList.size());
            for (int i = 0; i < activityList.size(); i++) {
                Activity activityInfo = activityList.get(i);//获取list第i+1个元素
                String status = activityInfo.getInsertStatus();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date2 = sdf.format(new Date());
                if("1".equals(activityInfo.getUpdateStatus())) {
                    if ("1".equals(status)) {
                        if (date2.compareTo(activityInfo.getKaStarTime()) >= 0 && date2.compareTo(activityInfo.getKaEndTime()) <= 0) {
                            activityInfo.setInsertStatus("2");
                            activityService.update(activityInfo);
                        } else if (date2.compareTo(activityInfo.getKaEndTime()) > 0) {
                            activityInfo.setInsertStatus("3");
                            activityService.update(activityInfo);
                            User rs=new User();
                            rs.setId(activityInfo.getBrandID());
                            rs=userService.selectOne(rs);
                            //发送微信模版消息
                            Map<String,String>  msgParam= new HashMap<>();
                            msgParam.put("first","尊敬的"+rs.getNickname()+"，您有一条新的活动信息请及时查收！");
                            msgParam.put("keyword1",activityInfo.getKaName());
                            msgParam.put("keyword2",activityInfo.getKaStarTime()+" 至 "+activityInfo.getKaEndTime());
                            msgParam.put("keyword3","——");
                            msgParam.put("keyword4",activityInfo.getKaDemand());
                            msgParam.put("remark","请登录平台查看活动信息");
                            SystemManager systemManager=new SystemManager();
                            WxMsgUtil.sendActivityStatusMsg(msgParam,systemManager.getAccessToken(),rs.getOpenID());
                            User rs2=new User();
                            rs2.setId(activityInfo.getKaID());
                            rs2=userService.selectOne(rs2);
                            //发送微信模版消息
                            Map<String,String>  msgParam2= new HashMap<>();
                            msgParam2.put("first","尊敬的"+rs2.getNickname()+"，您有一条新的活动信息请及时查收！");
                            msgParam2.put("keyword1",activityInfo.getKaName());
                            msgParam2.put("keyword2",activityInfo.getKaStarTime()+" 至 "+activityInfo.getKaEndTime());
                            msgParam2.put("keyword3","——");
                            msgParam2.put("keyword4",activityInfo.getKaDemand());
                            msgParam2.put("remark","请登录平台查看活动信息");
                            SystemManager systemManager2=new SystemManager();
                            WxMsgUtil.sendActivityStatusMsg(msgParam2,systemManager2.getAccessToken(),rs.getOpenID());
                            User rs3=new User();
                            rs3.setId(activityInfo.getCityID());
                            rs3=userService.selectOne(rs3);
                            //发送微信模版消息
                            Map<String,String>  msgParam3= new HashMap<>();
                            msgParam3.put("first","尊敬的"+rs3.getNickname()+"，您有一条新的活动信息请及时查收！");
                            msgParam3.put("keyword1",activityInfo.getKaName());
                            msgParam3.put("keyword2",activityInfo.getKaStarTime()+" 至 "+activityInfo.getKaEndTime());
                            msgParam3.put("keyword3","——");
                            msgParam3.put("keyword4",activityInfo.getKaDemand());
                            msgParam3.put("remark","请登录平台查看活动信息");
                            SystemManager systemManager3=new SystemManager();
                            WxMsgUtil.sendActivityStatusMsg(msgParam3,systemManager3.getAccessToken(),rs.getOpenID());
                        } else {

                        }
                    } else if ("2".equals(status)) {
                        if (date2.compareTo(activityInfo.getKaEndTime()) > 0) {
                            activityInfo.setInsertStatus("3");
                            activityService.update(activityInfo);
                            User rs=new User();
                            rs.setId(activityInfo.getBrandID());
                            rs=userService.selectOne(rs);
                            //发送微信模版消息
                            Map<String,String>  msgParam= new HashMap<>();
                            msgParam.put("first","尊敬的"+rs.getNickname()+"，您有一条新的活动信息请及时查收！");
                            msgParam.put("keyword1",activityInfo.getKaName());
                            msgParam.put("keyword2",activityInfo.getKaStarTime()+" 至 "+activityInfo.getKaEndTime());
                            msgParam.put("keyword3","——");
                            msgParam.put("keyword4",activityInfo.getKaDemand());
                            msgParam.put("remark","请登录平台查看活动信息");
                            SystemManager systemManager=new SystemManager();
                            WxMsgUtil.sendActivityStatusMsg(msgParam,systemManager.getAccessToken(),rs.getOpenID());
                            User rs2=new User();
                            rs2.setId(activityInfo.getKaID());
                            rs2=userService.selectOne(rs2);
                            //发送微信模版消息
                            Map<String,String>  msgParam2= new HashMap<>();
                            msgParam2.put("first","尊敬的"+rs2.getNickname()+"，您有一条新的活动信息请及时查收！");
                            msgParam2.put("keyword1",activityInfo.getKaName());
                            msgParam2.put("keyword2",activityInfo.getKaStarTime()+" 至 "+activityInfo.getKaEndTime());
                            msgParam2.put("keyword3","——");
                            msgParam2.put("keyword4",activityInfo.getKaDemand());
                            msgParam2.put("remark","请登录平台查看活动信息");
                            SystemManager systemManager2=new SystemManager();
                            WxMsgUtil.sendActivityStatusMsg(msgParam2,systemManager2.getAccessToken(),rs.getOpenID());
                            User rs3=new User();
                            rs3.setId(activityInfo.getCityID());
                            rs3=userService.selectOne(rs3);
                            //发送微信模版消息
                            Map<String,String>  msgParam3= new HashMap<>();
                            msgParam3.put("first","尊敬的"+rs3.getNickname()+"，您有一条新的活动信息请及时查收！");
                            msgParam3.put("keyword1",activityInfo.getKaName());
                            msgParam3.put("keyword2",activityInfo.getKaStarTime()+" 至 "+activityInfo.getKaEndTime());
                            msgParam3.put("keyword3","——");
                            msgParam3.put("keyword4",activityInfo.getKaDemand());
                            msgParam3.put("remark","请登录平台查看活动信息");
                            SystemManager systemManager3=new SystemManager();
                            WxMsgUtil.sendActivityStatusMsg(msgParam3,systemManager3.getAccessToken(),rs.getOpenID());
                        } else {

                        }
                    } else {

                    }
                }else{}
            }
        }
    }
}
