package shop.core.common.exception;

/**
 * 自定义异常-修改订单状态异常
 **
 */
public class UpdateOrderStatusException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * @param arg0
	 */
	public UpdateOrderStatusException(String arg0) {
		super(arg0);
	}

}
