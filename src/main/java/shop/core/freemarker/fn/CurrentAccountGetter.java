package shop.core.freemarker.fn;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import shop.core.common.bean.ManageContainer;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 获取当前登录的用户(前端用户)
 */
public class CurrentAccountGetter implements TemplateMethodModelEx {
    @Autowired
    private HttpSession session;
    @Override
    public Object exec(List arguments) throws TemplateModelException {
        return session.getAttribute(ManageContainer.manage_session_user_info);
    }
}
