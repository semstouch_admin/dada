package shop.services.common;


import shop.core.common.dao.page.PagerModel;


public class IndexImg extends PagerModel {
    private String id;//id
    private String title;//标题
    private String picture;//图片
    private int order1;//排序
    private String desc1;// 描述
    private String link;// 链接地址
    private String type;// 类型（n新闻、s商品）

    @Override
    public void clear() {
        this.id = null;
        this.title = null;
        this.picture = null;
        this.order1 = 0;
        desc1 = null;
        link = null;
    }

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getOrder1() {
        return order1;
    }

    public void setOrder1(int order1) {
        this.order1 = order1;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
