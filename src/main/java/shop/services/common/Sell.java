package shop.services.common;

import shop.core.common.dao.page.PagerModel;

import java.math.BigDecimal;

/**
*销售
*/
public class Sell extends PagerModel {
    //主键
    private String id;
    //报表名称
    private String reportName;
    //品牌商ID
    private String brandID;
    //KA经理ID
    private String kaID;
    //城市合伙人ID
    private String cityID;
    //门店ID
    private String storeID;
    //商品名称
    private String product;
    //条形码
    private String barCode;
    //商品价格
    private BigDecimal amount;
    //商品销售量
    private Integer sales;
    //地区
    private String area;
    //城市合伙人名称
    private String cityName;
    //门店名称
    private String storeName;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setBrandID(String brandID){
        this.brandID = brandID;
    }

    public String getBrandID(){
        return this.brandID;
    }

    public void setKaID(String kaID){
        this.kaID = kaID;
    }

    public String getKaID(){
        return this.kaID;
    }

    public void setCityID(String cityID){
        this.cityID = cityID;
    }

    public String getCityID(){
        return this.cityID;
    }

    public void setStoreID(String storeID){
        this.storeID = storeID;
    }

    public String getStoreID(){
        return this.storeID;
    }

    public void setProduct(String product){
        this.product = product;
    }

    public String getProduct(){
        return this.product;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
}
