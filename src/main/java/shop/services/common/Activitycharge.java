package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*活动费用
*/
public class Activitycharge extends PagerModel {
    //主键
    private String id;
    //活动ID
    private String activityID;
    //城市合伙人ID
    private String cityID;
    //门店ID
    private String storeID;
    //场地费用
    private Double siteCost;
    //堆头费用
    private Double tgCost;
    //促销员费用
    private String personCost;
    //补货数量
    private Integer productAmount;
    //提交状态（0不提交、1提交）
    private String status;
    //地区（城市合伙人）
    private String area;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setActivityID(String activityID){
        this.activityID = activityID;
    }

    public String getActivityID(){
        return this.activityID;
    }

    public void setCityID(String cityID){
        this.cityID = cityID;
    }

    public String getCityID(){
        return this.cityID;
    }

    public void setStoreID(String storeID){
        this.storeID = storeID;
    }

    public String getStoreID(){
        return this.storeID;
    }

    public Double getSiteCost() {
        return siteCost;
    }

    public void setSiteCost(Double siteCost) {
        this.siteCost = siteCost;
    }

    public Double getTgCost() {
        return tgCost;
    }

    public void setTgCost(Double tgCost) {
        this.tgCost = tgCost;
    }

    public void setPersonCost(String personCost){
        this.personCost = personCost;
    }

    public String getPersonCost(){
        return this.personCost;
    }

    public Integer getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(Integer productAmount) {
        this.productAmount = productAmount;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
