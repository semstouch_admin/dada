package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*门店
*/
public class Csrelation extends PagerModel {
    //主键ID
    private String id;
    //门店号
    private String storeID;
    //城市合伙人ID
    private String cityID;
    //门店名称
    private String storeName;
    //门店地址
    private String storeAddress;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public void setCityID(String cityID){
        this.cityID = cityID;
    }

    public String getCityID(){
        return this.cityID;
    }

    public void setStoreName(String storeName){
        this.storeName = storeName;
    }

    public String getStoreName(){
        return this.storeName;
    }

    public void setStoreAddress(String storeAddress){
        this.storeAddress = storeAddress;
    }

    public String getStoreAddress(){
        return this.storeAddress;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

}
