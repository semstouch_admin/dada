package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*合伙人签到
*/
public class Sign extends PagerModel {
    //主键
    private String id;
    //活动ID
    private String activityID;
    //城市合伙人ID
    private String cityID;
    //门店ID
    private String storeID;
    //签到定位地址
    private String location;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setActivityID(String activityID){
        this.activityID = activityID;
    }

    public String getActivityID(){
        return this.activityID;
    }

    public void setCityID(String cityID){
        this.cityID = cityID;
    }

    public String getCityID(){
        return this.cityID;
    }

    public void setStoreID(String storeID){
        this.storeID = storeID;
    }

    public String getStoreID(){
        return this.storeID;
    }

    public void setLocation(String location){
        this.location = location;
    }

    public String getLocation(){
        return this.location;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

}
