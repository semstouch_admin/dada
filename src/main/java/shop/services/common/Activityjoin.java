package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*活动参与
*/
public class Activityjoin extends PagerModel {
    //主键id
    private String id;
    //品牌商ID
    private String brandID;
    //KA经理ID
    private String kaID;
    //城市合伙人ID
    private String cityID;
    //活动ID
    private String activityID;
    //城市合伙人提交状态（0未提交、1已提交）
    private String status;
    //地区（城市合伙人）
    private String area;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setBrandID(String brandID){
        this.brandID = brandID;
    }

    public String getBrandID(){
        return this.brandID;
    }

    public void setKaID(String kaID){
        this.kaID = kaID;
    }

    public String getKaID(){
        return this.kaID;
    }

    public void setCityID(String cityID){
        this.cityID = cityID;
    }

    public String getCityID(){
        return this.cityID;
    }

    public void setActivityID(String activityID){
        this.activityID = activityID;
    }

    public String getActivityID(){
        return this.activityID;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
