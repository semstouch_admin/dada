package shop.services.common;

import shop.core.common.dao.page.PagerModel;
/**
*活动
*/
public class Activity extends PagerModel {
    //主键
    private String id;
    //品牌商ID
    private String brandID;
    //KA经理ID
    private String kaID;
    //活动名称
    private String name;
    //ka活动名称
    private String kaName;
    //活动要求
    private String demand;
    //ka活动要求
    private String kaDemand;
    //开始时间
    private String starTime;
    //ka开始时间
    private String kaStarTime;
    //结束时间
    private String endTime;
    //ka结束时间
    private String kaEndTime;
    //活动预算
    private String budge;
    //ka活动预算
    private String kaBudge;
    //照片
    private String picture;
    //ka照片
    private String kaPicture;
    //活动商品名称（多个用逗号隔开）
    private String productName;
    //ka活动商品名称（多个用逗号隔开）
    private String kaProductName;
    //活动商品条形码（多个用逗号隔开）
    private String productCode;
    //ka活动商品条形码（多个用逗号隔开）
    private String kaProductCode;
    //活动状态（1筹备中、2进行中、3已结束）
    private String insertStatus;
    //编辑状态(0未编辑、1已编辑）
    private String updateStatus;
    //参与门店数量
    private Integer storeMember;
    //创建时间
    private String createTime;
    //创建者
    private String createUser;
    //更新时间
    private String updateTime;
    //更新者
    private String updateUser;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setBrandID(String brandID){
        this.brandID = brandID;
    }

    public String getBrandID(){
        return this.brandID;
    }

    public void setKaID(String kaID){
        this.kaID = kaID;
    }

    public String getKaID(){
        return this.kaID;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setKaName(String kaName){
        this.kaName = kaName;
    }

    public String getKaName(){
        return this.kaName;
    }

    public void setDemand(String demand){
        this.demand = demand;
    }

    public String getDemand(){
        return this.demand;
    }

    public void setKaDemand(String kaDemand){
        this.kaDemand = kaDemand;
    }

    public String getKaDemand(){
        return this.kaDemand;
    }

    public void setStarTime(String starTime){
        this.starTime = starTime;
    }

    public String getStarTime(){
        return this.starTime;
    }

    public void setKaStarTime(String kaStarTime){
        this.kaStarTime = kaStarTime;
    }

    public String getKaStarTime(){
        return this.kaStarTime;
    }

    public void setEndTime(String endTime){
        this.endTime = endTime;
    }

    public String getEndTime(){
        return this.endTime;
    }

    public void setKaEndTime(String kaEndTime){
        this.kaEndTime = kaEndTime;
    }

    public String getKaEndTime(){
        return this.kaEndTime;
    }

    public void setBudge(String budge){
        this.budge = budge;
    }

    public String getBudge(){
        return this.budge;
    }

    public void setKaBudge(String kaBudge){
        this.kaBudge = kaBudge;
    }

    public String getKaBudge(){
        return this.kaBudge;
    }

    public void setPicture(String picture){
        this.picture = picture;
    }

    public String getPicture(){
        return this.picture;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getKaProductName() {
        return kaProductName;
    }

    public void setKaProductName(String kaProductName) {
        this.kaProductName = kaProductName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getKaProductCode() {
        return kaProductCode;
    }

    public void setKaProductCode(String kaProductCode) {
        this.kaProductCode = kaProductCode;
    }

    public void setKaPicture(String kaPicture){
        this.kaPicture = kaPicture;
    }

    public String getKaPicture(){
        return this.kaPicture;
    }

    public void setInsertStatus(String insertStatus){
        this.insertStatus = insertStatus;
    }

    public String getInsertStatus(){
        return this.insertStatus;
    }

    public void setUpdateStatus(String updateStatus){
        this.updateStatus = updateStatus;
    }

    public String getUpdateStatus(){
        return this.updateStatus;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateUser(String createUser){
        this.createUser = createUser;
    }

    public String getCreateUser(){
        return this.createUser;
    }

    public void setUpdateTime(String updateTime){
        this.updateTime = updateTime;
    }

    public String getUpdateTime(){
        return this.updateTime;
    }

    public void setUpdateUser(String updateUser){
        this.updateUser = updateUser;
    }

    public String getUpdateUser(){
        return this.updateUser;
    }

    public Integer getStoreMember() {
        return storeMember;
    }

    public void setStoreMember(Integer storeMember) {
        this.storeMember = storeMember;
    }
}
