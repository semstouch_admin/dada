/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activitypicture.impl;

import shop.core.ServicesManager;
import shop.services.front.activitypicture.ActivitypictureService;
import shop.services.front.activitypicture.bean.Activitypicture;
import shop.services.front.activitypicture.dao.ActivitypictureDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：ActivitypictureServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:05:28        
 * @版本号：1.0
 * @描述：     
 */
@Service("activitypictureServiceFront")
public class ActivitypictureServiceImpl extends ServicesManager<Activitypicture, ActivitypictureDao> implements
ActivitypictureService {
    @Resource(name = "activitypictureDaoFront")
    @Override
    public void setDao(ActivitypictureDao activitypictureDao) {
    this.dao = activitypictureDao;
    }

    @Override
    public List<Activitypicture> selectStoreList(Activitypicture activitypicture) {
        return dao.selectStoreList(activitypicture);
    }
}

