/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.activitypicture.bean;
import java.io.Serializable;

/**
* @类名称：Activitypicture前台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午22:05:28 
* @版本号：1.0
* @描述：
*/
public class Activitypicture extends shop.services.common.Activitypicture implements Serializable{
    private static final long serialVersionUID = 1L;
    //参与活动门店名称（查询用）
    private String storeName;
    //参与活动门店地址（查询用）
    private String storeAddress;
    //门店上传的图片数量（查询用）
    private String pictureNumber;


    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getPictureNumber() {
        return pictureNumber;
    }

    public void setPictureNumber(String pictureNumber) {
        this.pictureNumber = pictureNumber;
    }
}
