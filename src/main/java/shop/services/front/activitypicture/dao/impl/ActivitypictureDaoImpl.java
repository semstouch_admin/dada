/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activitypicture.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.activitypicture.bean.Activitypicture;
import shop.services.front.activitypicture.dao.ActivitypictureDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：ActivitypictureDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:05:28  
 * @版本号：1.0
 * @描述：
 */
@Repository("activitypictureDaoFront")
public class ActivitypictureDaoImpl  implements ActivitypictureDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Activitypicture e) {
        return dao.selectPageList("front.activitypicture.selectPageList","front.activitypicture.selectPageCount", e);
    }

    public List selectList(Activitypicture e) {
        return dao.selectList("front.activitypicture.selectList", e);
    }

    public Activitypicture selectOne(Activitypicture e) {
        return (Activitypicture) dao.selectOne("front.activitypicture.selectOne", e);
    }

    public int delete(Activitypicture e) {
        return dao.delete("front.activitypicture.delete", e);
    }

    public int update(Activitypicture e) {
        return dao.update("front.activitypicture.update", e);
    }

    public int deletes(String[] ids) {
        Activitypicture e = new Activitypicture();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Activitypicture e) {
        return dao.insert("front.activitypicture.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.activitypicture.deleteById", id);
    }

    @Override
    public Activitypicture selectById(String id) {
        return (Activitypicture) dao.selectOne("front.activitypicture.selectById", id);
    }

    @Override
    public List<Activitypicture> selectStoreList(Activitypicture activitypicture) {
        return dao.selectList("front.activitypicture.selectStoreList", activitypicture);
    }
}

