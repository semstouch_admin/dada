/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activitypicture.dao;

import shop.core.DaoManager;
import shop.services.front.activitypicture.bean.Activitypicture;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：ActivitypictureDao      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:05:28        
 * @版本号：1.0
 * @描述：     
 */
public interface ActivitypictureDao extends DaoManager<Activitypicture> {

    List<Activitypicture> selectStoreList(Activitypicture activitypicture);
}

