/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activityjoin.dao;

import shop.core.DaoManager;
import shop.services.front.activityjoin.bean.Activityjoin;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：ActivityjoinDao      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:04:39        
 * @版本号：1.0
 * @描述：     
 */
public interface ActivityjoinDao extends DaoManager<Activityjoin> {

    List<Activityjoin> selectCityChoose(Activityjoin activityjoin);
}

