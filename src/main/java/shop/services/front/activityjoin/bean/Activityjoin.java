/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.activityjoin.bean;
import java.io.Serializable;

/**
* @类名称：Activityjoin前台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午22:04:39 
* @版本号：1.0
* @描述：
*/
public class Activityjoin extends shop.services.common.Activityjoin implements Serializable{
    private static final long serialVersionUID = 1L;
    //城市合伙人名称（查询用）
    private String cityName;


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

}
