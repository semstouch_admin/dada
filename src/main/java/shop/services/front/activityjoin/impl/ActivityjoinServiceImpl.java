/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activityjoin.impl;

import shop.core.ServicesManager;
import shop.services.front.activityjoin.ActivityjoinService;
import shop.services.front.activityjoin.bean.Activityjoin;
import shop.services.front.activityjoin.dao.ActivityjoinDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：ActivityjoinServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:04:39        
 * @版本号：1.0
 * @描述：     
 */
@Service("activityjoinServiceFront")
public class ActivityjoinServiceImpl extends ServicesManager<Activityjoin, ActivityjoinDao> implements
ActivityjoinService {
    @Resource(name = "activityjoinDaoFront")
    @Override
    public void setDao(ActivityjoinDao activityjoinDao) {
    this.dao = activityjoinDao;
    }

    @Override
    public List<Activityjoin> selectCityChoose(Activityjoin activityjoin) {
        return dao.selectCityChoose(activityjoin);
    }
}

