/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activitycharge;

import shop.core.Services;
import shop.services.front.activitycharge.bean.Activitycharge;

import java.util.List;

/**   
 * @类名称：ActivitychargeService      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午21:58:55       
 * @版本号：1.0
 * @描述：     
 */
public interface ActivitychargeService extends Services<Activitycharge>{

    List<Activitycharge> selectStoreList(Activitycharge activitycharge);

    List<Activitycharge> selectAreaList(Activitycharge activitycharge);

    List<Activitycharge> selectCityList(Activitycharge activitycharge);
}

