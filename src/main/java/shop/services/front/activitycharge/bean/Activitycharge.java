/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.activitycharge.bean;
import java.io.Serializable;

/**
* @类名称：Activitycharge前台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午21:58:55 
* @版本号：1.0
* @描述：
*/
public class Activitycharge extends shop.services.common.Activitycharge implements Serializable{
    private static final long serialVersionUID = 1L;
    //参与活动门店名称（查询用）
    private String storeName;
    //参与活动门店地址（查询用）
    private String storeAddress;
    //活动费用总数（单个门店，查询用）
    private Double costAmount;
    //推广总费用（查询用）
    private Double chargeAmount;
    //总补货（查询用）
    private Integer cargoAmount;
    //地区数量（查询用）
    private String areaNumber;
    //合伙人姓名（查询用）
    private String cityName;
    //合伙人数量（查询用）
    private String cityNumber;
    //门店上传图片数量（查询用）
    private String pictureNumber;
    //合伙人签到次数（查询用）
    private String signAmount;


    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Double getCostAmount() {
        return costAmount;
    }

    public void setCostAmount(Double costAmount) {
        this.costAmount = costAmount;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public Double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(Double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public Integer getCargoAmount() {
        return cargoAmount;
    }

    public void setCargoAmount(Integer cargoAmount) {
        this.cargoAmount = cargoAmount;
    }

    public String getAreaNumber() {
        return areaNumber;
    }

    public void setAreaNumber(String areaNumber) {
        this.areaNumber = areaNumber;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityNumber() {
        return cityNumber;
    }

    public void setCityNumber(String cityNumber) {
        this.cityNumber = cityNumber;
    }

    public String getPictureNumber() {
        return pictureNumber;
    }

    public void setPictureNumber(String pictureNumber) {
        this.pictureNumber = pictureNumber;
    }

    public String getSignAmount() {
        return signAmount;
    }

    public void setSignAmount(String signAmount) {
        this.signAmount = signAmount;
    }
}
