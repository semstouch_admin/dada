/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activitycharge.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.activitycharge.bean.Activitycharge;
import shop.services.front.activitycharge.dao.ActivitychargeDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：ActivitychargeDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午21:58:55  
 * @版本号：1.0
 * @描述：
 */
@Repository("activitychargeDaoFront")
public class ActivitychargeDaoImpl  implements ActivitychargeDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Activitycharge e) {
        return dao.selectPageList("front.activitycharge.selectPageList","front.activitycharge.selectPageCount", e);
    }

    public List selectList(Activitycharge e) {
        return dao.selectList("front.activitycharge.selectList", e);
    }

    public Activitycharge selectOne(Activitycharge e) {
        return (Activitycharge) dao.selectOne("front.activitycharge.selectOne", e);
    }

    public int delete(Activitycharge e) {
        return dao.delete("front.activitycharge.delete", e);
    }

    public int update(Activitycharge e) {
        return dao.update("front.activitycharge.update", e);
    }

    public int deletes(String[] ids) {
        Activitycharge e = new Activitycharge();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Activitycharge e) {
        return dao.insert("front.activitycharge.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.activitycharge.deleteById", id);
    }

    @Override
    public Activitycharge selectById(String id) {
        return (Activitycharge) dao.selectOne("front.activitycharge.selectById", id);
    }

    @Override
    public List<Activitycharge> selectStoreList(Activitycharge activitycharge) {
        return dao.selectList("front.activitycharge.selectStoreList", activitycharge);
    }

    @Override
    public List<Activitycharge> selectAreaList(Activitycharge activitycharge) {
        return dao.selectList("front.activitycharge.selectAreaList", activitycharge);
    }

    @Override
    public List<Activitycharge> selectCityList(Activitycharge activitycharge) {
        return dao.selectList("front.activitycharge.selectCityList", activitycharge);
    }
}

