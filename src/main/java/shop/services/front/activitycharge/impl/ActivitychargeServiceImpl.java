/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activitycharge.impl;

import shop.core.ServicesManager;
import shop.services.front.activitycharge.ActivitychargeService;
import shop.services.front.activitycharge.bean.Activitycharge;
import shop.services.front.activitycharge.dao.ActivitychargeDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：ActivitychargeServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午21:58:55        
 * @版本号：1.0
 * @描述：     
 */
@Service("activitychargeServiceFront")
public class ActivitychargeServiceImpl extends ServicesManager<Activitycharge, ActivitychargeDao> implements
ActivitychargeService {
    @Resource(name = "activitychargeDaoFront")
    @Override
    public void setDao(ActivitychargeDao activitychargeDao) {
    this.dao = activitychargeDao;
    }

    @Override
    public List<Activitycharge> selectStoreList(Activitycharge activitycharge) {
        return dao.selectStoreList(activitycharge);
    }

    @Override
    public List<Activitycharge> selectAreaList(Activitycharge activitycharge) {
        return dao.selectAreaList(activitycharge);
    }

    @Override
    public List<Activitycharge> selectCityList(Activitycharge activitycharge) {
        return dao.selectCityList(activitycharge);
    }
}

