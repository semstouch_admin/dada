/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.sell.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.sell.bean.Sell;
import shop.services.front.sell.dao.SellDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：SellDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:07:31  
 * @版本号：1.0
 * @描述：
 */
@Repository("sellDaoFront")
public class SellDaoImpl  implements SellDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Sell e) {
        return dao.selectPageList("front.sell.selectPageList","front.sell.selectPageCount", e);
    }

    public List selectList(Sell e) {
        return dao.selectList("front.sell.selectList", e);
    }

    public Sell selectOne(Sell e) {
        return (Sell) dao.selectOne("front.sell.selectOne", e);
    }

    public int delete(Sell e) {
        return dao.delete("front.sell.delete", e);
    }

    public int update(Sell e) {
        return dao.update("front.sell.update", e);
    }

    public int deletes(String[] ids) {
        Sell e = new Sell();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Sell e) {
        return dao.insert("front.sell.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.sell.deleteById", id);
    }

    @Override
    public Sell selectById(String id) {
        return (Sell) dao.selectOne("front.sell.selectById", id);
    }

    @Override
    public List<Sell> selectAreaList(Sell sell) {
        return dao.selectList("front.sell.selectAreaList", sell);
    }

    @Override
    public List<Sell> selectCityList(Sell sell) {
        return dao.selectList("front.sell.selectCityList", sell);
    }

    @Override
    public List<Sell> selectStoreList(Sell sell) {
        return dao.selectList("front.sell.selectStoreList", sell);
    }

    @Override
    public List<Sell> selectProductList(Sell sell) {
        return dao.selectList("front.sell.selectProductList", sell);
    }

    @Override
    public List<Sell> selectActivityList(Sell sell) {
        return dao.selectList("front.sell.selectActivityList", sell);
    }

    @Override
    public List<Sell> selectSellList(Sell sell) {
        return dao.selectList("front.sell.selectSellList", sell);
    }

    @Override
    public List<Sell> selectMonthSellList(Sell sell) {
        return dao.selectList("front.sell.selectMonthSellList", sell);
    }
}

