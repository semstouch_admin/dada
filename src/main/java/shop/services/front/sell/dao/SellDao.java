/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.sell.dao;

import shop.core.DaoManager;
import shop.services.front.sell.bean.Sell;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：SellDao      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:31        
 * @版本号：1.0
 * @描述：     
 */
public interface SellDao extends DaoManager<Sell> {

    List<Sell> selectAreaList(Sell sell);

    List<Sell> selectCityList(Sell sell);

    List<Sell> selectStoreList(Sell sell);

    List<Sell> selectProductList(Sell sell);

    List<Sell> selectActivityList(Sell sell);

    List<Sell> selectSellList(Sell sell);

    List<Sell> selectMonthSellList(Sell sell);
}

