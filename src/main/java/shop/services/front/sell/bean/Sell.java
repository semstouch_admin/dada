/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.sell.bean;
import java.io.Serializable;

/**
* @类名称：Sell前台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午22:07:31 
* @版本号：1.0
* @描述：
*/
public class Sell extends shop.services.common.Sell implements Serializable{
    private static final long serialVersionUID = 1L;
    //销售金额（查询用）
    private String salesAmount;
    //销售量（查询用）
    private String salesVolume;
    //开始时间（查询用）
    private String startDate;
    //结束时间（查询用）
    private String endDate;
    //地区（模糊查询用）
    private String areaLike;
    //合伙人名称（模糊查询用）
    private String cityNameLike;
    //门店名称（模糊查询用）
    private String storeNameLike;
    //商品名称（模糊查询用）
    private String productLike;


    public String getSalesAmount() {
        return salesAmount;
    }

    public void setSalesAmount(String salesAmount) {
        this.salesAmount = salesAmount;
    }

    public String getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(String salesVolume) {
        this.salesVolume = salesVolume;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAreaLike() {
        return areaLike;
    }

    public void setAreaLike(String areaLike) {
        this.areaLike = areaLike;
    }

    public String getCityNameLike() {
        return cityNameLike;
    }

    public void setCityNameLike(String cityNameLike) {
        this.cityNameLike = cityNameLike;
    }

    public String getStoreNameLike() {
        return storeNameLike;
    }

    public void setStoreNameLike(String storeNameLike) {
        this.storeNameLike = storeNameLike;
    }

    public String getProductLike() {
        return productLike;
    }

    public void setProductLike(String productLike) {
        this.productLike = productLike;
    }
}
