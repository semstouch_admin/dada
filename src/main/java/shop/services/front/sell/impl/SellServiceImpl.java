/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.sell.impl;

import shop.core.ServicesManager;
import shop.services.front.sell.SellService;
import shop.services.front.sell.bean.Sell;
import shop.services.front.sell.dao.SellDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：SellServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:31        
 * @版本号：1.0
 * @描述：     
 */
@Service("sellServiceFront")
public class SellServiceImpl extends ServicesManager<Sell, SellDao> implements
SellService {
    @Resource(name = "sellDaoFront")
    @Override
    public void setDao(SellDao sellDao) {
    this.dao = sellDao;
    }

    @Override
    public List<Sell> selectAreaList(Sell sell) {
        return dao.selectAreaList(sell);
    }

    @Override
    public List<Sell> selectCityList(Sell sell) {
        return dao.selectCityList(sell);
    }

    @Override
    public List<Sell> selectStoreList(Sell sell) {
        return dao.selectStoreList(sell);
    }

    @Override
    public List<Sell> selectProductList(Sell sell) {
        return dao.selectProductList(sell);
    }

    @Override
    public List<Sell> selectActivityList(Sell sell) {
        return dao.selectActivityList(sell);
    }

    @Override
    public List<Sell> selectSellList(Sell sell) {
        return dao.selectSellList(sell);
    }

    @Override
    public List<Sell> selectMonthSellList(Sell sell) {
        return dao.selectMonthSellList(sell);
    }
}

