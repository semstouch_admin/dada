/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.kcrelation.dao;

import shop.core.DaoManager;
import shop.services.front.kcrelation.bean.Kcrelation;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：KcrelationDao      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:06        
 * @版本号：1.0
 * @描述：     
 */
public interface KcrelationDao extends DaoManager<Kcrelation> {

    List<Kcrelation> selectCityList(Kcrelation kcrelation);

    List<Kcrelation> selectKaList(Kcrelation kcrelation);
}

