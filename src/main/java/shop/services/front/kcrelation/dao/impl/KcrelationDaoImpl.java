/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.kcrelation.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.kcrelation.bean.Kcrelation;
import shop.services.front.kcrelation.dao.KcrelationDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：KcrelationDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:07:06  
 * @版本号：1.0
 * @描述：
 */
@Repository("kcrelationDaoFront")
public class KcrelationDaoImpl  implements KcrelationDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Kcrelation e) {
        return dao.selectPageList("front.kcrelation.selectPageList","front.kcrelation.selectPageCount", e);
    }

    public List selectList(Kcrelation e) {
        return dao.selectList("front.kcrelation.selectList", e);
    }

    public Kcrelation selectOne(Kcrelation e) {
        return (Kcrelation) dao.selectOne("front.kcrelation.selectOne", e);
    }

    public int delete(Kcrelation e) {
        return dao.delete("front.kcrelation.delete", e);
    }

    public int update(Kcrelation e) {
        return dao.update("front.kcrelation.update", e);
    }

    public int deletes(String[] ids) {
        Kcrelation e = new Kcrelation();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Kcrelation e) {
        return dao.insert("front.kcrelation.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.kcrelation.deleteById", id);
    }

    @Override
    public Kcrelation selectById(String id) {
        return (Kcrelation) dao.selectOne("front.kcrelation.selectById", id);
    }

    @Override
    public List<Kcrelation> selectCityList(Kcrelation kcrelation) {
        return dao.selectList("front.kcrelation.selectCityList", kcrelation);
    }

    @Override
    public List<Kcrelation> selectKaList(Kcrelation kcrelation) {
        return dao.selectList("front.kcrelation.selectKaList", kcrelation);
    }
}

