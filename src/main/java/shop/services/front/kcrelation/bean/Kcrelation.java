/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.kcrelation.bean;
import java.io.Serializable;

/**
* @类名称：Kcrelation前台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午22:07:05 
* @版本号：1.0
* @描述：
*/
public class Kcrelation extends shop.services.common.Kcrelation implements Serializable{
    private static final long serialVersionUID = 1L;
    //城市合伙人名称
    private String cityName;


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
