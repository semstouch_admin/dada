/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activity.impl;

import shop.core.ServicesManager;
import shop.services.front.activity.ActivityService;
import shop.services.front.activity.bean.Activity;
import shop.services.front.activity.dao.ActivityDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：ActivityServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午19:53:49        
 * @版本号：1.0
 * @描述：     
 */
@Service("activityServiceFront")
public class ActivityServiceImpl extends ServicesManager<Activity, ActivityDao> implements
ActivityService {
    @Resource(name = "activityDaoFront")
    @Override
    public void setDao(ActivityDao activityDao) {
    this.dao = activityDao;
    }

    @Override
    public List<Activity> selectCityActivityList(Activity activity) {
        return dao.selectCityActivityList(activity);
    }

    @Override
    public Activity selectKaActivityDetail(Activity activity) {
        return dao.selectKaActivityDetail(activity);
    }
}

