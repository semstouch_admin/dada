/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activity.dao;

import shop.core.DaoManager;
import shop.services.front.activity.bean.Activity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：ActivityDao      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午19:53:49        
 * @版本号：1.0
 * @描述：     
 */
public interface ActivityDao extends DaoManager<Activity> {

    List<Activity> selectCityActivityList(Activity activity);

    Activity selectKaActivityDetail(Activity activity);
}

