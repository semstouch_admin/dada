/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.activity.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.activity.bean.Activity;
import shop.services.front.activity.dao.ActivityDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：ActivityDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午19:53:49  
 * @版本号：1.0
 * @描述：
 */
@Repository("activityDaoFront")
public class ActivityDaoImpl  implements ActivityDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Activity e) {
        return dao.selectPageList("front.activity.selectPageList","front.activity.selectPageCount", e);
    }

    public List selectList(Activity e) {
        return dao.selectList("front.activity.selectList", e);
    }

    public Activity selectOne(Activity e) {
        return (Activity) dao.selectOne("front.activity.selectOne", e);
    }

    public int delete(Activity e) {
        return dao.delete("front.activity.delete", e);
    }

    public int update(Activity e) {
        return dao.update("front.activity.update", e);
    }

    public int deletes(String[] ids) {
        Activity e = new Activity();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Activity e) {
        return dao.insert("front.activity.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.activity.deleteById", id);
    }

    @Override
    public Activity selectById(String id) {
        return (Activity) dao.selectOne("front.activity.selectById", id);
    }

    @Override
    public List<Activity> selectCityActivityList(Activity activity) {
        return dao.selectList("front.activity.selectCityActivityList", activity);
    }

    @Override
    public Activity selectKaActivityDetail(Activity activity) {
        return (Activity)dao.selectOne("front.activity.selectKaActivityDetail", activity);
    }
}

