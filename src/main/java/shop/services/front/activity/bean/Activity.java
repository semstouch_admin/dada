/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.activity.bean;
import java.io.Serializable;

/**
* @类名称：Activity前台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午19:53:49 
* @版本号：1.0
* @描述：
*/
public class Activity extends shop.services.common.Activity implements Serializable{
    private static final long serialVersionUID = 1L;
    //月份（查询用）
    private String activityMonth;
    //KA经理提交城市合伙人状态（查询用）
    private String cityStatus;
    //城市合伙人提交费用状态（查询用）
    private String acStatus;
    //城市合伙人ID（查询用）
    private String cityID;
    //推广总费用（查询用）
    private Double chargeAmount;


    public String getActivityMonth() {
        return activityMonth;
    }

    public void setActivityMonth(String activityMonth) {
        this.activityMonth = activityMonth;
    }

    public String getCityStatus() {
        return cityStatus;
    }

    public void setCityStatus(String cityStatus) {
        this.cityStatus = cityStatus;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    public String getAcStatus() {
        return acStatus;
    }

    public void setAcStatus(String acStatus) {
        this.acStatus = acStatus;
    }

    public Double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(Double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }
}
