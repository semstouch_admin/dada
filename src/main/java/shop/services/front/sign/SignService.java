/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.sign;

import shop.core.Services;
import shop.services.front.sign.bean.Sign;

import java.util.List;

/**   
 * @类名称：SignService      
 * @创建人：Ltz   
 * @创建时间：2017-09-12 下午21:35:30       
 * @版本号：1.0
 * @描述：     
 */
public interface SignService extends Services<Sign>{

    List<Sign> selectSignList(Sign sign);
}

