/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.sign.impl;

import shop.core.ServicesManager;
import shop.services.front.sign.SignService;
import shop.services.front.sign.bean.Sign;
import shop.services.front.sign.dao.SignDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：SignServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-12 下午21:35:30        
 * @版本号：1.0
 * @描述：     
 */
@Service("signServiceFront")
public class SignServiceImpl extends ServicesManager<Sign, SignDao> implements
SignService {
    @Resource(name = "signDaoFront")
    @Override
    public void setDao(SignDao signDao) {
    this.dao = signDao;
    }

    @Override
    public List<Sign> selectSignList(Sign sign) {
        return dao.selectSignList(sign);
    }
}

