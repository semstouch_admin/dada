/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.sign.dao;

import shop.core.DaoManager;
import shop.services.front.sign.bean.Sign;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：SignDao      
 * @创建人：Ltz   
 * @创建时间：2017-09-12 下午21:35:30        
 * @版本号：1.0
 * @描述：     
 */
public interface SignDao extends DaoManager<Sign> {

    List<Sign> selectSignList(Sign sign);
}

