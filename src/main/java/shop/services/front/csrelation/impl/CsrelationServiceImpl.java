/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.csrelation.impl;

import shop.core.ServicesManager;
import shop.services.front.csrelation.CsrelationService;
import shop.services.front.csrelation.bean.Csrelation;
import shop.services.front.csrelation.dao.CsrelationDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：CsrelationServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:06:26        
 * @版本号：1.0
 * @描述：     
 */
@Service("csrelationServiceFront")
public class CsrelationServiceImpl extends ServicesManager<Csrelation, CsrelationDao> implements
CsrelationService {
    @Resource(name = "csrelationDaoFront")
    @Override
    public void setDao(CsrelationDao csrelationDao) {
    this.dao = csrelationDao;
    }

}

