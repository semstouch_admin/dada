/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.front.csrelation.bean;
import java.io.Serializable;

/**
* @类名称：Csrelation前台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午22:06:26 
* @版本号：1.0
* @描述：
*/
public class Csrelation extends shop.services.common.Csrelation implements Serializable{
    private static final long serialVersionUID = 1L;
    //城市合伙人提交费用状态（查询用）
    private String acStatus;


    public String getAcStatus() {
        return acStatus;
    }

    public void setAcStatus(String acStatus) {
        this.acStatus = acStatus;
    }
}
