/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.csrelation.dao;

import shop.core.DaoManager;
import shop.services.front.csrelation.bean.Csrelation;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：CsrelationDao      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:06:26        
 * @版本号：1.0
 * @描述：     
 */
public interface CsrelationDao extends DaoManager<Csrelation> {
}

