/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.front.csrelation.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.csrelation.bean.Csrelation;
import shop.services.front.csrelation.dao.CsrelationDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：CsrelationDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:06:26  
 * @版本号：1.0
 * @描述：
 */
@Repository("csrelationDaoFront")
public class CsrelationDaoImpl  implements CsrelationDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Csrelation e) {
        return dao.selectPageList("front.csrelation.selectPageList","front.csrelation.selectPageCount", e);
    }

    public List selectList(Csrelation e) {
        return dao.selectList("front.csrelation.selectList", e);
    }

    public Csrelation selectOne(Csrelation e) {
        return (Csrelation) dao.selectOne("front.csrelation.selectOne", e);
    }

    public int delete(Csrelation e) {
        return dao.delete("front.csrelation.delete", e);
    }

    public int update(Csrelation e) {
        return dao.update("front.csrelation.update", e);
    }

    public int deletes(String[] ids) {
        Csrelation e = new Csrelation();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Csrelation e) {
        return dao.insert("front.csrelation.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("front.csrelation.deleteById", id);
    }

    @Override
    public Csrelation selectById(String id) {
        return (Csrelation) dao.selectOne("front.csrelation.selectById", id);
    }

}

