package shop.services.manage.task;

import shop.core.Services;
import shop.services.manage.task.bean.Task;

import java.util.List;
import java.util.Map;

public interface TaskService extends Services<Task> {
	/**
	 * 获取所有表达式
	 * @return
	 */
	public Map<String,String> getAllCron();
	
	/**
	 * 获取任务列表 
	 * @return
	 */
	public List<Task> getAllTask();
	
	/**
	 * 根据任务
	 * @param taskId
	 * @return
	 */
	public Task getTaskById(String taskId);
	
	/**
	 * 创建一个新任务
	 * @param task
	 * @return
	 * @throws Exception
	 */
	public Task addTask(Task task) throws Exception;

	                                                                            
	
	/**
	 * 修改一个任务的触发时间（使用默认的任务组名，触发器名，触发器组名）
	 * @param taskId
	 * @param cron
	 * @return
	 */
	public Task modifyTaskCron(String taskId,String cron);
	
	/**
	 * 移除一个任务(使用默认的任务组名，触发器名,触发器组名)
	 * @param task
	 * @return
	 */
	public Task removeTask(Task task);
	/**
	 * 重启任务
	 * @return
	 */
	public Task restartTask(String taskId);
	
	/**
	 * 暂停定时任务
	 * @param taskId
	 * @return
	 */
	public Task pauseTask(String taskId);
	
	/**
	 * 禁用
	 * @param taskId
	 * @return
	 */
	public Task disableTask(String taskId);
	
	/**
	 * 关闭定时任务
	 * @param taskId
	 * @return
	 */
	public Task shutdownTask(String taskId);
	
	/**
	 * 启动所有定时任务
	 * @return
	 */
	public void startAllTask();
	
	/**
	 * 关闭所有定时任务
	 */
	public void shutdownAllTask();
}
