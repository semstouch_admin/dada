/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.activity;

import shop.core.Services;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.activity.bean.Activity;

/**   
 * @类名称：ActivityService      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午19:53:49       
 * @版本号：1.0
 * @描述：     
 */
public interface ActivityService extends Services<Activity>{

    PagerModel selectIssuePageList(Activity e);

    PagerModel selectJoinPageList(Activity e);

    PagerModel selectPageListFist(Activity e);

    Activity selectThis(Activity e);
}

