/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.activity.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.activity.bean.Activity;
import shop.services.manage.activity.dao.ActivityDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：ActivityDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午19:53:49  
 * @版本号：1.0
 * @描述：
 */
@Repository("activityDaoManage")
public class ActivityDaoImpl  implements ActivityDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Activity e) {
        return dao.selectPageList("manage.activity.selectPageList","manage.activity.selectPageCount", e);
    }

    public List selectList(Activity e) {
        return dao.selectList("manage.activity.selectList", e);
    }

    public Activity selectOne(Activity e) {
        return (Activity) dao.selectOne("manage.activity.selectOne", e);
    }

    public int delete(Activity e) {
        return dao.delete("manage.activity.delete", e);
    }

    public int update(Activity e) {
        return dao.update("manage.activity.update", e);
    }

    public int deletes(String[] ids) {
        Activity e = new Activity();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Activity e) {
        return dao.insert("manage.activity.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.activity.deleteById", id);
    }

    @Override
    public Activity selectById(String id) {
        return (Activity) dao.selectOne("manage.activity.selectById", id);
    }

    @Override
    public PagerModel selectIssuePageList(Activity e) {
        return dao.selectPageList("manage.activity.selectIssuePageList","manage.activity.selectIssuePageCount", e);
    }

    @Override
    public PagerModel selectJoinPageList(Activity e) {
        return dao.selectPageList("manage.activity.selectJoinPageList","manage.activity.selectJoinPageList", e);
    }

    public PagerModel selectPageListFist(Activity e) {
        return dao.selectPageList("manage.activity.selectPageListFist","manage.activity.selectPageCountFirst", e);
    }

    public Activity selectThis(Activity e) {
        return (Activity) dao.selectOne("manage.activity.selectThis", e);
    }
}

