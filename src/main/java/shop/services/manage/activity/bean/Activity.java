/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.activity.bean;

/**
* @类名称：Activity后台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午19:53:49 
* @版本号：1.0
* @描述：
*/
public class Activity extends shop.services.common.Activity {
    //发布者（参与者）
    private String nickname;
    //门店数量
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
