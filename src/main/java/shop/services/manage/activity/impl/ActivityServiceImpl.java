/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.activity.impl;

import shop.core.ServicesManager;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.activity.ActivityService;
import shop.services.manage.activity.bean.Activity;
import shop.services.manage.activity.dao.ActivityDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：ActivityServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午19:53:49        
 * @版本号：1.0
 * @描述：     
 */
@Service("activityServiceManage")
public class ActivityServiceImpl extends ServicesManager<Activity, ActivityDao> implements
ActivityService {
    @Resource(name = "activityDaoManage")
    @Override
    public void setDao(ActivityDao activityDao) {
    this.dao = activityDao;
    }

    @Override
    public PagerModel selectIssuePageList(Activity e) {
        return dao.selectIssuePageList(e);
    }

    @Override
    public PagerModel selectJoinPageList(Activity e) {
        return dao.selectJoinPageList(e);
    }

    /**
    * 活动管理首页
    * */
    public PagerModel selectPageListFist(Activity e) {
        return dao.selectPageListFist(e);
    }

    /**
    * 选择当前对象进行编辑使用
    * */
    public Activity selectThis(Activity e){
        return dao.selectThis(e);
    }
}

