/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.activityjoin.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.activityjoin.bean.Activityjoin;
import shop.services.manage.activityjoin.dao.ActivityjoinDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：ActivityjoinDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:04:39  
 * @版本号：1.0
 * @描述：
 */
@Repository("activityjoinDaoManage")
public class ActivityjoinDaoImpl  implements ActivityjoinDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Activityjoin e) {
        return dao.selectPageList("manage.activityjoin.selectPageList","manage.activityjoin.selectPageCount", e);
    }

    public List selectList(Activityjoin e) {
        return dao.selectList("manage.activityjoin.selectList", e);
    }

    public Activityjoin selectOne(Activityjoin e) {
        return (Activityjoin) dao.selectOne("manage.activityjoin.selectOne", e);
    }

    public int delete(Activityjoin e) {
        return dao.delete("manage.activityjoin.delete", e);
    }

    public int update(Activityjoin e) {
        return dao.update("manage.activityjoin.update", e);
    }

    public int deletes(String[] ids) {
        Activityjoin e = new Activityjoin();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Activityjoin e) {
        return dao.insert("manage.activityjoin.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.activityjoin.deleteById", id);
    }

    @Override
    public Activityjoin selectById(String id) {
        return (Activityjoin) dao.selectOne("manage.activityjoin.selectById", id);
    }

}

