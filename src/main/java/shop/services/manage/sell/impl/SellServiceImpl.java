/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.sell.impl;

import shop.core.ServicesManager;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.sell.SellService;
import shop.services.manage.sell.bean.Sell;
import shop.services.manage.sell.dao.SellDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：SellServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:31        
 * @版本号：1.0
 * @描述：     
 */
@Service("sellServiceManage")
public class SellServiceImpl extends ServicesManager<Sell, SellDao> implements
SellService {
    @Resource(name = "sellDaoManage")
    @Override
    public void setDao(SellDao sellDao) {
    this.dao = sellDao;
    }


    @Override
    public int insert(List<Sell> e) {
        return dao.insert(e);
    }

    public int deletesByTimes(String createTime){
        return dao.deletesByTimes(createTime);
    }

    public PagerModel selectPageListDetail(Sell e){
        return dao.selectPageListDetail(e);
    }

    public List<Sell> selectListByDate(Sell e){
        return dao.selectListByDate(e);
    }

    @Override
    public List<Sell> selectBrandList(Sell rs) {
        return dao.selectBrandList(rs);
    }

    @Override
    public List<Sell> selectKaList(Sell rs) {
        return dao.selectKaList(rs);
    }

    @Override
    public List<Sell> selectCityList(Sell rs) {
        return dao.selectCityList(rs);
    }

//    @Override
//    public int insertWithList(List<Sell> infoList) {
//        return dao.insertWithList(infoList);
//    }
}

