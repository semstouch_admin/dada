/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.sell.dao.impl;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.sell.bean.Sell;
import shop.services.manage.sell.dao.SellDao;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.List;

/**
 * @类名称：SellDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:07:31  
 * @版本号：1.0
 * @描述：
 */
@Repository("sellDaoManage")
public class SellDaoImpl  implements SellDao {

    @Resource(name = "sqlBatchSessionTemplate")
    private SqlSessionTemplate sqlBatchSessionTemplate;


    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Sell e) {
        return dao.selectPageList("manage.sell.selectPageList","manage.sell.selectPageCount", e);
    }

    public List<Sell> selectList(Sell e) {
        return dao.selectList("manage.sell.selectList", e);
    }

    public Sell selectOne(Sell e) {
        return (Sell) dao.selectOne("manage.sell.selectOne", e);
    }

    public int delete(Sell e) {
        return dao.delete("manage.sell.delete", e);
    }

    public int update(Sell e) {
        return dao.update("manage.sell.update", e);
    }

    public int deletes(String[] ids) {
        Sell e = new Sell();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Sell e) {
        return dao.insert("manage.sell.insert", e);
    }

    /**
     * 批量插入数据
     * @param e
     * @return
     */
    public int insert(List<Sell> e) {

        int result = 1;
        SqlSession batchSqlSession = null;
        try {
            batchSqlSession =sqlBatchSessionTemplate
                    .getSqlSessionFactory()
                    .openSession(ExecutorType.BATCH, false);// 获取批量方式的sqlsession
            batchSqlSession.getConnection().setAutoCommit(false);
            int batchCount = 2000;// 每批commit的个数
            int batchLastIndex = batchCount;// 每批最后一个的下标
            for (int index = 0; index < e.size();) {
                if (batchLastIndex >= e.size()) {
                    batchLastIndex = e.size();
                    result = result * batchSqlSession.insert("manage.sell.batchInsert",e.subList(index, batchLastIndex));
                    batchSqlSession.commit();
                    System.out.println("index:" + index+ " batchLastIndex:" + batchLastIndex);
                    break;// 数据插入完毕，退出循环
                } else {
                    result = result * batchSqlSession.insert("manage.sell.batchInsert",e.subList(index, batchLastIndex));
                    batchSqlSession.commit();
                    System.out.println("index:" + index+ " batchLastIndex:" + batchLastIndex);
                    index = batchLastIndex;// 设置下一批下标
                    batchLastIndex = index + (batchCount - 1);
                }
            }
            batchSqlSession.commit();
        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            batchSqlSession.close();
        }
        return result;
    }

    public int deleteById(int id) {
        return dao.delete("manage.sell.deleteById", id);
    }

    @Override
    public Sell selectById(String id) {
        return (Sell) dao.selectOne("manage.sell.selectById", id);
    }

    public int deletesByTimes(String createTime){
        return dao.delete("manage.sell.deletesByTimes",createTime);
    }

    public PagerModel selectPageListDetail(Sell e) {
        return dao.selectPageList("manage.sell.selectPageListDetail","manage.sell.selectPageCountDetail", e);
    }

    public List<Sell> selectListByDate(Sell e){
        return dao.selectList("manage.sell.selectListByDate",e);
    }

    @Override
    public List<Sell> selectBrandList(Sell rs) {
        return dao.selectList("manage.sell.selectBrandList", rs);
    }

    @Override
    public List<Sell> selectKaList(Sell rs) {
        return dao.selectList("manage.sell.selectKaList", rs);
    }

    @Override
    public List<Sell> selectCityList(Sell rs) {
        return dao.selectList("manage.sell.selectCityList", rs);
    }
//
//    @Override
//    public int insertWithList(List<Sell> infoList) {
//        return dao.insert("manage.sell.insertWithList", infoList) ;
//    }
}

