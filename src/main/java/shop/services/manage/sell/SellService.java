/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.sell;

import shop.core.Services;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.sell.bean.Sell;

import java.util.List;

/**   
 * @类名称：SellService      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:31       
 * @版本号：1.0
 * @描述：     
 */
public interface SellService extends Services<Sell>{

    int deletesByTimes(String createTime);

    int insert(List<Sell> e);

    PagerModel selectPageListDetail(Sell e);

    List<Sell> selectListByDate(Sell e);

    List<Sell> selectBrandList(Sell rs);

    List<Sell> selectKaList(Sell rs);

    List<Sell> selectCityList(Sell rs);

//    int insertWithList(List<Sell> infoList);
}

