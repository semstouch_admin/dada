/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.sign.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.sign.bean.Sign;
import shop.services.manage.sign.dao.SignDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：SignDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-09-12 下午21:35:30  
 * @版本号：1.0
 * @描述：
 */
@Repository("signDaoManage")
public class SignDaoImpl  implements SignDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Sign e) {
        return dao.selectPageList("manage.sign.selectPageList","manage.sign.selectPageCount", e);
    }

    public List selectList(Sign e) {
        return dao.selectList("manage.sign.selectList", e);
    }

    public Sign selectOne(Sign e) {
        return (Sign) dao.selectOne("manage.sign.selectOne", e);
    }

    public int delete(Sign e) {
        return dao.delete("manage.sign.delete", e);
    }

    public int update(Sign e) {
        return dao.update("manage.sign.update", e);
    }

    public int deletes(String[] ids) {
        Sign e = new Sign();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Sign e) {
        return dao.insert("manage.sign.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.sign.deleteById", id);
    }

    @Override
    public Sign selectById(String id) {
        return (Sign) dao.selectOne("manage.sign.selectById", id);
    }

    @Override
    public PagerModel selectSignPageList(Sign e) {
        return dao.selectPageList("manage.sign.selectSignPageList","manage.sign.selectSignPageCount", e);
    }
}

