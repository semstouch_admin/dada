/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.sign.bean;

/**
* @类名称：Sign后台对象类
* @创建人：Ltz
* @创建时间：2017-09-12 下午21:35:30 
* @版本号：1.0
* @描述：
*/
public class Sign extends shop.services.common.Sign {

    //活动名称（查询用）
    private String activityName;
    //城市合伙人名称（查询用）
    private String cityName;
    //门店名称（查询用）
    private String storeName;

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
