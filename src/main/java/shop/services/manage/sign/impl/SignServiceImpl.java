/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.sign.impl;

import shop.core.ServicesManager;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.sign.SignService;
import shop.services.manage.sign.bean.Sign;
import shop.services.manage.sign.dao.SignDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**   
 * @类名称：SignServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-09-12 下午21:35:30        
 * @版本号：1.0
 * @描述：     
 */
@Service("signServiceManage")
public class SignServiceImpl extends ServicesManager<Sign, SignDao> implements
SignService {
    @Resource(name = "signDaoManage")
    @Override
    public void setDao(SignDao signDao) {
    this.dao = signDao;
    }

    @Override
    public PagerModel selectSignPageList(Sign e) {
        return dao.selectSignPageList(e);
    }
}

