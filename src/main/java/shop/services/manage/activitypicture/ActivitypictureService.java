/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.activitypicture;

import shop.core.Services;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.activitypicture.bean.Activitypicture;

/**   
 * @类名称：ActivitypictureService      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:05:28       
 * @版本号：1.0
 * @描述：     
 */
public interface ActivitypictureService extends Services<Activitypicture>{
}

