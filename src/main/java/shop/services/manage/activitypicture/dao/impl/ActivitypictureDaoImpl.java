/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.services.manage.activitypicture.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.activitypicture.bean.Activitypicture;
import shop.services.manage.activitypicture.dao.ActivitypictureDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @类名称：ActivitypictureDaoImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:05:28  
 * @版本号：1.0
 * @描述：
 */
@Repository("activitypictureDaoManage")
public class ActivitypictureDaoImpl implements ActivitypictureDao {

    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Activitypicture e) {
        return dao.selectPageList("manage.activitypicture.selectPageList", "manage.activitypicture.selectPageCount", e);
    }

    public List selectList(Activitypicture e) {
        return dao.selectList("manage.activitypicture.selectList", e);
    }

    public Activitypicture selectOne(Activitypicture e) {
        return (Activitypicture) dao.selectOne("manage.activitypicture.selectOne", e);
    }

    public int delete(Activitypicture e) {
        return dao.delete("manage.activitypicture.delete", e);
    }

    public int update(Activitypicture e) {
        return dao.update("manage.activitypicture.update", e);
    }

    public int deletes(String[] ids) {
        Activitypicture e = new Activitypicture();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(Activitypicture e) {
        return dao.insert("manage.activitypicture.insert", e);
    }

    public int deleteById(int id) {
        return dao.delete("manage.activitypicture.deleteById", id);
    }

    @Override
    public Activitypicture selectById(String id) {
        return (Activitypicture) dao.selectOne("manage.activitypicture.selectById", id);
    }
}

