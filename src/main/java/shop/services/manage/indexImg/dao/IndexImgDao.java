/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.services.manage.indexImg.dao;

import java.util.List;

import shop.core.DaoManager;
import shop.services.manage.indexImg.bean.IndexImg;


public interface IndexImgDao extends DaoManager<IndexImg> {

	/**
	 * @param i
	 * @return
	 */
	List<IndexImg> getImgsShowToIndex(int i);

}
