/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.kcrelation;

import shop.core.Services;
import shop.services.manage.kcrelation.bean.Kcrelation;

import java.util.List;

/**   
 * @类名称：KcrelationService      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:05       
 * @版本号：1.0
 * @描述：     
 */
public interface KcrelationService extends Services<Kcrelation>{

    List<Kcrelation> selectCityList(Kcrelation kcrelation);

    List<Kcrelation> selectKaList(Kcrelation kcrelation);

    Kcrelation selectByBrandID(String id);
}

