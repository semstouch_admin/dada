/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.kcrelation.bean;

/**
* @类名称：Kcrelation后台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午22:07:05 
* @版本号：1.0
* @描述：
*/
public class Kcrelation extends shop.services.common.Kcrelation {
    //城市合伙人名称
    private String nickname;
    //绑定状态
    private String bindStatus;
    //城市合伙人ID
    private String cityID;


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String getCityID() {
        return cityID;
    }

    @Override
    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    public String getBindStatus() {
        return bindStatus;
    }

    public void setBindStatus(String bindStatus) {
        this.bindStatus = bindStatus;
    }
}
