/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.kcrelation.impl;

import shop.core.ServicesManager;
import shop.services.manage.kcrelation.KcrelationService;
import shop.services.manage.kcrelation.bean.Kcrelation;
import shop.services.manage.kcrelation.dao.KcrelationDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**   
 * @类名称：KcrelationServiceImpl      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午22:07:05        
 * @版本号：1.0
 * @描述：     
 */
@Service("kcrelationServiceManage")
public class KcrelationServiceImpl extends ServicesManager<Kcrelation, KcrelationDao> implements
KcrelationService {
    @Resource(name = "kcrelationDaoManage")
    @Override
    public void setDao(KcrelationDao kcrelationDao) {
    this.dao = kcrelationDao;
    }

    @Override
    public List<Kcrelation> selectCityList(Kcrelation kcrelation) {
        return dao.selectCityList(kcrelation);
    }

    @Override
    public List<Kcrelation> selectKaList(Kcrelation kcrelation) {
        return dao.selectKaList(kcrelation);
    }

    /**
    * 根据BrandID获取对象信息
    * */
    public Kcrelation selectByBrandID(String id) {
        return dao.selectByBrandID(id);
    }
}

