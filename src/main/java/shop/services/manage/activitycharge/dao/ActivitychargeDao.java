/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.activitycharge.dao;

import shop.core.DaoManager;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.activitycharge.bean.Activitycharge;
import org.springframework.stereotype.Repository;

import java.util.List;

/**   
 * @类名称：ActivitychargeDao      
 * @创建人：Ltz   
 * @创建时间：2017-08-29 下午21:58:55        
 * @版本号：1.0
 * @描述：     
 */
public interface ActivitychargeDao extends DaoManager<Activitycharge> {

    List<Activitycharge> selectStoreList(Activitycharge activitycharge);
}

