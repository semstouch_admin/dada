/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.activitycharge.bean;

/**
* @类名称：Activitycharge后台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午21:58:55 
* @版本号：1.0
* @描述：
*/
public class Activitycharge extends shop.services.common.Activitycharge {
    //合伙人姓名（查询用）
    private String cityName;
    //门店名称（查询用）
    private String storeName;
    //门店地址（查询用）
    private String storeAddress;
    //活动名称（查询用）
    private String activityName;
    //照片数量（查询用）
    private String pictureNumber;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPictureNumber() {
        return pictureNumber;
    }

    public void setPictureNumber(String pictureNumber) {
        this.pictureNumber = pictureNumber;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }
}
