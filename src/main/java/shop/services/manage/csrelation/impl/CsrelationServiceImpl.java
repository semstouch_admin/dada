/**
 * Copyright:Copyright(c)2014-2015
 * Company:厦门市易致达物联网科技有限公司
 *
 * @version 1.0
 */
package shop.services.manage.csrelation.impl;

import org.apache.commons.lang.StringUtils;
import shop.core.ServicesManager;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.csrelation.CsrelationService;
import shop.services.manage.csrelation.bean.Csrelation;
import shop.services.manage.csrelation.dao.CsrelationDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * @类名称：CsrelationServiceImpl
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:06:26
 * @版本号：1.0
 * @描述：
 */
@Service("csrelationServiceManage")
public class CsrelationServiceImpl extends ServicesManager<Csrelation, CsrelationDao> implements
        CsrelationService {
    @Resource(name = "csrelationDaoManage")
    @Override
    public void setDao(CsrelationDao csrelationDao) {
        this.dao = csrelationDao;
    }

    @Override
    public PagerModel selectPageListFirst(Csrelation e) {
        return dao.selectPageListFirst(e);
    }

    /**
     * 根据cityID批量删除
     *
     * @param ids
     * @return
     */
    public int deletesByCityId(String[] ids) {
       ///得到的id不能为空，否则抛出异常
        if (ids == null || ids.length == 0) {
            throw new NullPointerException("id不能全为空！");
        }

        for (int i = 0; i < ids.length; i++) {
            if (StringUtils.isBlank(ids[i])) {
                throw new NullPointerException("id不能为空！");
            }
            dao.deleteByCityId(Integer.parseInt(ids[i]));
        }
        return 0;
    }

    /**
     * 通过城市合伙人的id查询列表
     * */
    public List selectListByCityID(Csrelation e) {
        return dao.selectListByCityID(e);
    }

    /**
     * 获取没有绑定城市合伙人的门店列表
     * */
    public List<Csrelation> selectnoCityList(Csrelation e) {
        return dao.selectnoCityIDList(e);
    }

    /**
     * 获取门店表的对象
     */
    public Csrelation selectThis(Csrelation e) {
        return dao.selectThis(e);
    }

    /**
     * 更新门店信息--绑定合伙人id和更新合伙人的地址
     * */
    public void updateAllCityID(String[] ids, String cityID, String updateCityID) {
        if (ids == null || ids.length == 0) {
            throw new NullPointerException("ID不能为空！");
        }
        for (int i = 0; i < ids.length; i++) {
            if (StringUtils.isBlank(ids[i])) {
                throw new NullPointerException("ID不能存在空的！");
            }
            Csrelation csrelation = new Csrelation();
            csrelation.setId(ids[i]);
            csrelation.setCityID(cityID);
            dao.updateAllCityID(csrelation);
        }
    }

}

