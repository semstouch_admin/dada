/**
 * Copyright:Copyright(c)2014-2017
 * Company:厦门市易致达物联网科技有限公司
 * @version 1.0
 */
package shop.services.manage.csrelation;

import shop.core.Services;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.csrelation.bean.Csrelation;

import java.util.List;

/**
 * @类名称：CsrelationService
 * @创建人：Ltz
 * @创建时间：2017-08-29 下午22:06:26
 * @版本号：1.0
 * @描述：
 */
public interface CsrelationService extends Services<Csrelation>{

    PagerModel selectPageListFirst(Csrelation e);

    List selectListByCityID(Csrelation e);

    int deletesByCityId(String[] ids);

    List<Csrelation> selectnoCityList(Csrelation e);

    void updateAllCityID(String[] ids, String cityID, String updateCityID);

    Csrelation selectThis(Csrelation e);

}

