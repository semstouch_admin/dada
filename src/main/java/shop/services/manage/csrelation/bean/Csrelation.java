/**
* Copyright:Copyright(c)2014-2017
* Company:厦门市易致达物联网科技有限公司
* @version 1.0
*/
package shop.services.manage.csrelation.bean;

/**
* @类名称：Csrelation后台对象类
* @创建人：Ltz
* @创建时间：2017-08-29 下午22:06:26 
* @版本号：1.0
* @描述：
*/
public class Csrelation extends shop.services.common.Csrelation {
    //显示地区
    private String address;
    //显示数量
    private String number;
    //显示合伙人
    private String nickname;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

}
