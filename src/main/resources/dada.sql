/*
Navicat MySQL Data Transfer

Source Server         : xsdada
Source Server Version : 50635
Source Host           : 47.94.200.130:3306
Source Database       : dada

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2017-12-03 15:45:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_activity
-- ----------------------------
DROP TABLE IF EXISTS `t_activity`;
CREATE TABLE `t_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `brandID` varchar(11) DEFAULT NULL COMMENT '品牌商ID',
  `kaID` varchar(11) DEFAULT NULL COMMENT 'KA经理ID',
  `name` varchar(255) DEFAULT NULL COMMENT '活动名称',
  `kaName` varchar(255) DEFAULT NULL COMMENT 'ka活动名称',
  `demand` varchar(3000) DEFAULT NULL COMMENT '活动要求',
  `kaDemand` varchar(3000) DEFAULT NULL COMMENT 'ka活动要求',
  `starTime` varchar(255) DEFAULT NULL COMMENT '开始时间',
  `kaStarTime` varchar(255) DEFAULT NULL COMMENT 'ka开始时间',
  `endTime` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `kaEndTime` varchar(255) DEFAULT NULL COMMENT 'ka结束时间',
  `budge` varchar(11) DEFAULT NULL COMMENT '活动预算',
  `kaBudge` varchar(11) DEFAULT NULL COMMENT 'ka活动预算',
  `picture` varchar(1000) DEFAULT NULL COMMENT '照片',
  `kaPicture` varchar(1000) DEFAULT NULL COMMENT 'ka照片',
  `productName` varchar(255) DEFAULT NULL COMMENT '活动商品名称（多个用逗号隔开）',
  `kaProductName` varchar(255) DEFAULT NULL COMMENT 'ka活动商品名称（多个用逗号隔开）',
  `productCode` varchar(1000) DEFAULT NULL COMMENT '活动商品条形码（多个用逗号隔开）',
  `kaProductCode` varchar(1000) DEFAULT NULL COMMENT 'ka活动商品条形码（多个用逗号隔开）',
  `insertStatus` varchar(5) DEFAULT NULL COMMENT '活动状态（1筹备中、2进行中、3已结束）',
  `updateStatus` varchar(3) DEFAULT NULL COMMENT '编辑状态(0未编辑、1已编辑）',
  `storeMember` varchar(11) DEFAULT '0' COMMENT '参与门店数量',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(255) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(255) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  UNIQUE KEY `kaName` (`kaName`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8 COMMENT='活动表';

-- ----------------------------
-- Table structure for t_activitycharge
-- ----------------------------
DROP TABLE IF EXISTS `t_activitycharge`;
CREATE TABLE `t_activitycharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `activityID` varchar(11) DEFAULT NULL COMMENT '活动ID',
  `cityID` varchar(11) DEFAULT NULL COMMENT '城市合伙人ID',
  `storeID` varchar(255) DEFAULT NULL COMMENT '门店ID',
  `siteCost` decimal(32,2) DEFAULT '0.00' COMMENT '场地费用',
  `tgCost` decimal(32,2) DEFAULT '0.00' COMMENT '堆头费用',
  `personCost` varchar(1000) DEFAULT NULL COMMENT '促销员费用',
  `productAmount` int(32) DEFAULT '0' COMMENT '补货数量',
  `status` varchar(255) DEFAULT '0' COMMENT '提交状态（0不提交、1提交）',
  `area` varchar(255) DEFAULT NULL COMMENT '地区（城市合伙人）',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(255) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(255) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='活动费用表';

-- ----------------------------
-- Table structure for t_activityjoin
-- ----------------------------
DROP TABLE IF EXISTS `t_activityjoin`;
CREATE TABLE `t_activityjoin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `brandID` varchar(11) DEFAULT NULL COMMENT '品牌商ID',
  `kaID` varchar(11) DEFAULT NULL COMMENT 'KA经理ID',
  `cityID` varchar(11) DEFAULT NULL COMMENT '城市合伙人ID',
  `activityID` varchar(11) DEFAULT NULL COMMENT '活动ID',
  `area` varchar(255) DEFAULT NULL COMMENT '地区（城市合伙人）',
  `status` varchar(3) DEFAULT '2' COMMENT 'KA经理选择城市合伙人状态（0未提交、1已提交、2未保存）',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(255) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(255) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=822 DEFAULT CHARSET=utf8 COMMENT='活动参与表';

-- ----------------------------
-- Table structure for t_activitypicture
-- ----------------------------
DROP TABLE IF EXISTS `t_activitypicture`;
CREATE TABLE `t_activitypicture` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `activityID` varchar(11) DEFAULT NULL COMMENT '活动ID',
  `cityID` varchar(11) DEFAULT NULL COMMENT '城市合伙人ID',
  `storeID` varchar(11) DEFAULT NULL COMMENT '门店ID',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片（多个用逗号隔开）',
  `location` varchar(255) DEFAULT NULL COMMENT '图片定位地址',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='活动照片表';

-- ----------------------------
-- Table structure for t_area
-- ----------------------------
DROP TABLE IF EXISTS `t_area`;
CREATE TABLE `t_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `pcode` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `desc1` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16870 DEFAULT CHARSET=utf8 COMMENT='地理区域表';

-- ----------------------------
-- Table structure for t_csrelation
-- ----------------------------
DROP TABLE IF EXISTS `t_csrelation`;
CREATE TABLE `t_csrelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `storeID` varchar(255) DEFAULT NULL COMMENT '门店号',
  `cityID` varchar(255) DEFAULT NULL COMMENT '城市合伙人ID',
  `storeName` varchar(255) DEFAULT NULL COMMENT '门店名称',
  `storeAddress` varchar(255) DEFAULT NULL COMMENT '门店地址',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(255) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(255) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `storeID` (`storeID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=613 DEFAULT CHARSET=utf8 COMMENT='门店表';

-- ----------------------------
-- Table structure for t_index_img
-- ----------------------------
DROP TABLE IF EXISTS `t_index_img`;
CREATE TABLE `t_index_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(245) NOT NULL COMMENT '标题',
  `picture` varchar(100) NOT NULL COMMENT '图片',
  `order1` int(11) NOT NULL COMMENT '排序',
  `desc1` varchar(1145) DEFAULT NULL COMMENT '描述',
  `link` varchar(145) DEFAULT NULL COMMENT '广告链接',
  `type` varchar(3) DEFAULT NULL COMMENT '类型（n新闻、s商品）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='首页图片表';

-- ----------------------------
-- Table structure for t_kcrelation
-- ----------------------------
DROP TABLE IF EXISTS `t_kcrelation`;
CREATE TABLE `t_kcrelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `brandID` varchar(255) DEFAULT NULL COMMENT '品牌商ID',
  `kaID` varchar(255) DEFAULT NULL COMMENT 'KA经理ID',
  `cityID` varchar(255) DEFAULT NULL COMMENT '城市合伙人ID',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(255) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(255) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COMMENT='KA经理与城市合伙人关系表';

-- ----------------------------
-- Table structure for t_keyvalue
-- ----------------------------
DROP TABLE IF EXISTS `t_keyvalue`;
CREATE TABLE `t_keyvalue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key1` varchar(45) NOT NULL,
  `value` varchar(145) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key1`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COMMENT='键值对表';

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT '111',
  `orderNum` int(11) NOT NULL DEFAULT '0',
  `type` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='系统菜单表';

-- ----------------------------
-- Table structure for t_news
-- ----------------------------
DROP TABLE IF EXISTS `t_news`;
CREATE TABLE `t_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL COMMENT '类型（新闻 n  热点 y）',
  `title` varchar(45) DEFAULT NULL COMMENT '标题',
  `title2` varchar(45) DEFAULT NULL COMMENT '副标题',
  `pictures` varchar(1000) DEFAULT NULL COMMENT '图片',
  `content` longtext COMMENT '内容',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
  `readerCount` int(11) DEFAULT '0' COMMENT '阅读人数',
  `status` varchar(2) DEFAULT 'n' COMMENT '状态（n 禁用  y 启用）',
  `createAccount` varchar(45) DEFAULT NULL COMMENT '创建者',
  `order1` int(11) DEFAULT '0' COMMENT '排序',
  `code` varchar(15) DEFAULT NULL COMMENT '关键字',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COMMENT='新闻表';

-- ----------------------------
-- Table structure for t_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_privilege`;
CREATE TABLE `t_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7156 DEFAULT CHARSET=utf8 COMMENT='角色权限关系表';

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) NOT NULL,
  `role_desc` varchar(45) DEFAULT NULL,
  `role_dbPrivilege` varchar(45) DEFAULT NULL,
  `status` varchar(2) DEFAULT 'y',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_name_UNIQUE` (`role_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Table structure for t_sell
-- ----------------------------
DROP TABLE IF EXISTS `t_sell`;
CREATE TABLE `t_sell` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `reportName` varchar(11) DEFAULT NULL COMMENT '报表名称',
  `brandID` varchar(11) DEFAULT NULL COMMENT '品牌商ID',
  `kaID` varchar(11) DEFAULT NULL COMMENT 'KA经理ID',
  `cityID` varchar(11) DEFAULT NULL COMMENT '城市合伙人ID',
  `storeID` varchar(11) DEFAULT NULL COMMENT '门店ID',
  `product` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `barCode` varchar(255) DEFAULT NULL COMMENT '条形码',
  `amount` double(11,2) DEFAULT '0.00' COMMENT '销售金额',
  `sales` int(11) DEFAULT '0' COMMENT '商品销售量',
  `area` varchar(255) DEFAULT NULL COMMENT '地区',
  `cityName` varchar(255) DEFAULT NULL COMMENT '城市合伙人名称',
  `storeName` varchar(255) DEFAULT NULL COMMENT '门店名称',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(255) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(255) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1623828 DEFAULT CHARSET=utf8 COMMENT='销售表';

-- ----------------------------
-- Table structure for t_sign
-- ----------------------------
DROP TABLE IF EXISTS `t_sign`;
CREATE TABLE `t_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `activityID` varchar(11) DEFAULT NULL COMMENT '活动ID',
  `cityID` varchar(11) DEFAULT NULL COMMENT '城市合伙人ID',
  `storeID` varchar(11) DEFAULT NULL COMMENT '门店ID',
  `location` varchar(255) DEFAULT NULL COMMENT '签到定位地址',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(11) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8 COMMENT='合伙人签到表';

-- ----------------------------
-- Table structure for t_sku
-- ----------------------------
DROP TABLE IF EXISTS `t_sku`;
CREATE TABLE `t_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品库存ID',
  `specIDS` varchar(1000) DEFAULT NULL COMMENT '商品规格id组合',
  `skuPrice` decimal(8,2) DEFAULT NULL COMMENT '商品sku原价',
  `skuNowPrice` decimal(8,2) DEFAULT NULL COMMENT '商品sku现价',
  `skuStock` int(11) DEFAULT NULL COMMENT '商品库存数量',
  `productID` varchar(11) DEFAULT NULL COMMENT '商品ID',
  `skuPurchasePrice` decimal(8,2) DEFAULT NULL COMMENT '进价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品库存表（暂时弃用）';

-- ----------------------------
-- Table structure for t_systemlog
-- ----------------------------
DROP TABLE IF EXISTS `t_systemlog`;
CREATE TABLE `t_systemlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `content` varchar(500) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `account` varchar(45) DEFAULT NULL,
  `loginIP` varchar(15) DEFAULT NULL,
  `logintime` varchar(50) DEFAULT NULL,
  `loginArea` varchar(45) DEFAULT NULL,
  `diffAreaLogin` char(1) DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3439 DEFAULT CHARSET=utf8 COMMENT='系统日志表';

-- ----------------------------
-- Table structure for t_systemsetting
-- ----------------------------
DROP TABLE IF EXISTS `t_systemsetting`;
CREATE TABLE `t_systemsetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `systemCode` varchar(45) DEFAULT NULL COMMENT '系统编码',
  `name` varchar(100) NOT NULL COMMENT '系统名称',
  `www` varchar(100) NOT NULL COMMENT '系统路径',
  `logo` varchar(100) DEFAULT NULL COMMENT 'logo路径',
  `title` varchar(45) NOT NULL COMMENT '系统标题',
  `description` varchar(45) NOT NULL COMMENT '系统描述',
  `keywords` varchar(100) NOT NULL COMMENT '关键字',
  `shortcuticon` varchar(100) NOT NULL COMMENT '页面icon',
  `appid` varchar(1000) DEFAULT NULL COMMENT '微信公众号ID',
  `secret` varchar(1000) DEFAULT NULL COMMENT '微信公众号密钥',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统参数设置表';

-- ----------------------------
-- Table structure for t_task
-- ----------------------------
DROP TABLE IF EXISTS `t_task`;
CREATE TABLE `t_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `parentId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '父级任务ID',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务名称',
  `nameDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务描述',
  `planExe` int(11) DEFAULT NULL COMMENT '计划执行次数,默认0,表示满足条件循环执行次数',
  `groupName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务组名称,约定为一个类全名',
  `groupDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务组描述',
  `cron` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务表达式',
  `cronDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务表达式描述',
  `triggerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '触发器，约定为一个方法名，格式com.dongnao.Xdd.methdo',
  `triggerGrop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '触发器组名',
  `triggerGropDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '触发器组描述',
  `execute` int(11) DEFAULT NULL COMMENT '任务被执行过多少次',
  `lastExeTime` int(100) DEFAULT NULL COMMENT '最后一次开始执行时间',
  `lastFinishTime` int(100) DEFAULT NULL,
  `state` int(11) DEFAULT '0' COMMENT '任务状态0禁用，1启动，2删除',
  `deply` int(11) DEFAULT '0' COMMENT '延迟启动，默认0,表示不延迟启动',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='定时任务表';

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nickname` varchar(45) DEFAULT NULL COMMENT '姓名',
  `username` varchar(45) NOT NULL COMMENT '用户名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `status` varchar(3) DEFAULT 'y' COMMENT '状态（y禁用、n启用）',
  `rid` varchar(45) DEFAULT NULL COMMENT '角色类型（1超级管理员）',
  `address` varchar(45) DEFAULT '全国' COMMENT '地址',
  `openID` varchar(1000) DEFAULT NULL COMMENT '用户设备',
  `bindStatus` varchar(255) DEFAULT '0' COMMENT '绑定状态（0未绑定、1已绑定）',
  `issueNumber` int(11) DEFAULT '0' COMMENT '发布活动次数（品牌商，KA经理）',
  `joinNumber` int(11) DEFAULT '0' COMMENT '参与活动次数(城市合伙人）',
  `createTime` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(255) DEFAULT NULL COMMENT '创建者',
  `updateTime` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(255) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`) USING BTREE,
  UNIQUE KEY `nickname_UNIQUE` (`nickname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8 COMMENT='系统用户表';
