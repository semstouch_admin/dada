<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="区域管理">
<div class="row">
    <div id="t1" class="span6 doc-content"></div>
    <div class="span18 doc-content">
        <form class="form-horizontal well" id="J_Form" action="${basepath}/rest/manage/area/saveJson" method="post">
            <h3 style="padding:0 10px 10px 10px;border-bottom: #c3c3d6 1px solid;">区域信息
                <a href="javascript:del()" class="button button-mini button-danger pull-right" id="delBtn" style="margin-left: 10px;">删除区域</a>

                <a href="javascript:add()" class="button button-mini button-primary pull-right" id="addBtn">新增区域</a>
            </h3>
            <div class="row">
                <div class="control-group span8">
                    <label class="control-label">区域名称：</label>

                    <div class="controls">
                        <input type="hidden" name="id" class="control-text">
                        <input type="text" class="control-text" name="name">
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="control-group span8">
                    <label class="control-label">编码：</label>

                    <div class="controls">
                        <input type="text" name="code" class="control-text">
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="control-group span8">
                    <label class="control-label">父级编码：</label>

                    <div class="controls">
                        <input type="text" name="pcode" class="control-text" readonly>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="form-actions offset3">
                    <button type="submit" class="button button-success">立即保存</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    var store = new BUI.Data.TreeStore({
        map: {
            name: 'text'
        },
        url: '${basepath}/rest/manage/area/getAreaTree?pid=0'
    });
    var form = new BUI.Form.HForm({ //创建表单
        srcNode: '#J_Form',
        submitType: 'ajax',
        callback: function (data) {
            store.load();
        }
    }).render();

    BUI.use('bui/tree', function (Tree) {

        var tree = new Tree.TreeList({
            render: '#t1',
            store: store,
            height: 500,
            showLine: true, //显示连接线
            'itemclick': true
        });
        tree.render();
        store.load();

        tree.on('itemclick', function (ev) {
            var item = ev.item;
            item.name = item.text;
            var menuForm = $("#J_Form")[0];
            BUI.FormHelper.setFields(menuForm,item);
            form.set('initRecord', item)
        });
    })

    function del(){
        var id = form.getFieldValue("id");
        if(id==""){
            BUI.Message.Alert('请选择左侧菜单！','error');
        }else {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/area/delete",
                dataType: "json",
                data: {
                    ids: id
                },
                success: function (data) {
                    store.load();
                    form.clearFields();
                }
            });
        }
    }

    function add(){
        var pid = $("input[name='code']").val();
        form.clearFields();
        if(pid==""||pid==null){
            pid="0"
        }
        form.setFieldValue("pcode",pid);
    }


</script>
</@page.pageBase>