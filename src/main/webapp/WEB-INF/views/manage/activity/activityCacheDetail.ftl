<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="活动管理">
<style>
    .button-large {
        font-size: 16px;
    }

    hr {
        border-color: #009688;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a onclick="javascript:history.go(-1);">返回上一级</a>
            </div>
            <div class="panel-body">
            <#--form表单提交，相当于data-->
                <form id="editForm" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            活动名称：
                        </label>

                        <div class="controls">
                            <input type="text" class="offline-activityName control-text span-width span6"
                                   name="activityName" value="" disabled/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            品牌商：
                        </label>

                        <div class="controls">
                            <input type="text" class="offline-nickname control-text span-width span6"
                                   name="nickname" disabled/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            城市合伙人：
                        </label>

                        <div class="controls">
                            <input type="text" class="offline-cityName control-text span-width span6"
                                   name="cityName" value="" disabled/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            门店地址：
                        </label>

                        <div class="controls">
                            <input type="text" class="offline-storeAddress control-text span-width span6"
                                   name="storeAddress" value="" disabled/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            场地费用：
                        </label>

                        <div class="controls">
                            <input type="text" class="offline-siteCost control-text span-width span6"
                                   name="siteCost" value="" disabled/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            堆头费用：
                        </label>

                        <div class="controls">
                            <input type="text" class="offline-tgCost vcontrol-text span-width span6"
                                   name="tgCost" value="" disabled/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            补货量：
                        </label>

                        <div class="controls">
                            <input type="text" class="offline-productAmount control-text span-width span6"
                                   name="productAmount" value="" disabled/>
                        </div>
                    </div>
                    <div class="div-person">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function GetRequest() {
        var url = decodeURI(decodeURI(location.search)); //获取url中"?"符后的字串，使用了两次decodeRUI解码
        var theRequest = new Object();
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
            }
            return theRequest;
        }
    }
    var postData = GetRequest();
    var storeID = postData.id;
    var activityID = postData.activityID;
    var cityID = postData.cityID;
    var nickname = postData.nickname;
    $(".offline-nickname").val(nickname);
    $.ajax({
        type: "get",
        url: "/rest/manage/activitycharge/selectActivitychargeDetail",
        dataType: "json",
        data: {
            activityID: activityID,
            storeID: storeID,
            cityID: cityID
        },
        success: function (data) {
            var list = data.data;
            $(".offline-activityName").val(list.activityName);
            $(".offline-cityName").val(list.cityName);
            $(".offline-productAmount").val(list.productAmount);
            $(".offline-tgCost").val(list.tgCost);
            $(".offline-siteCost").val(list.siteCost);
            $(".offline-storeAddress").val(list.storeAddress);
            var personCostArr = list.personCost.split(",");
            var personHtml = "";
            for (var i = 0; i < personCostArr.length; i++) {
                personHtml+='<div class="control-group"><label class="control-label"> <s>*</s>促销员费用</label>'+
                            '<div class="controls"><input type="text" class="offline-productAmount control-text span-width span6" name="productAmount" value="' + personCostArr[i] + '" disabled/>'+
                           '</div></div>';
            }
            $(".div-person").html(personHtml);

        }
    })
</script>

</@page.pageBase>