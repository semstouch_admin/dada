<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="活动管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .span16 {
        width: 880px;
    }

    .form-horizontal .time-label {
        width: 60px;
        line-height: 40px;
        margin-left: 20px;
    }

    .button-primary {
        margin-left: 5px;
    }

    .form-panel .time-input {
        width: 120px;
    }

    .form-panel select {
        margin-left: 10px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }
</style>
<!--suppress ALL -->
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/activity/loadData">
    <ul class="panel-content">
        <li>
            <div>
                <div class="control-group span5">
                    <div class="search-controls  controls">
                        <input type="text" name="nickname"  value="" placeholder="搜索关键字">
                    </div>
                </div>
                <div class="form-actions span2">
                    <button type="submit" class="button  button-primary">
                        搜索
                    </button>
                </div>
            </div>
            <div>
                <a href="/rest/manage/activity/toAdd" class="button button-success">
                    发布活动
                </a>
                <a class="button button-danger" href="javascript:delFunction();">
                    批量删除
                </a>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '7%'},
                {title: '活动名称', dataIndex: 'name', elCls: 'center', width: '15%'},
                {title: '发布者', dataIndex: 'nickname', elCls: 'center', width: '15%'},
                {title: '参与门店数量', dataIndex: 'number', width: "15%", elCls: 'center',
                    renderer: function (data, obj) {
                        if(data==''|| data == null){
                            return '——';
                        }
                        return '<a href="${basepath}/rest/manage/activity/toStoreList?id=' + obj.id + '&nickname=' + obj.nickname +'">' + data + '</a>';
                    }
                },
                {title: 'ka经理是否编辑', dataIndex: 'updateStatus', elCls: 'center',width: '15%',
                    renderer: function (data) {
                        if(data==0){
                            return '否';
                        }else{
                            return '是';
                        }
                    }
                },
                {
                    title: '活动状态',
                    dataIndex: 'insertStatus',
                    elCls: 'center',
                    width: '15%',
                    renderer: function (data) {
                        if (data == "1") {
                            return '筹备中';
                        } else if (data == "2") {
                            return '进行中';
                        } else if (data == "3") {
                            return '已结束';
                        }
                    }
                },
                {
                    title: '操作', dataIndex: 'id', elCls: 'center', width: '15%', renderer: function (value, data) {
                    if (data.insertStatus == "1" || data.insertStatus == "2") {
                        return '<a  href="${basepath}/rest/manage/activity/toEdit?id=' + value + '">编辑</a><a style="margin-left:10px;"  href="javascript: delFunction();">删除</a>';
                    } else if (data.insertStatus == "3") {
                        return '<a  href="${basepath}/rest/manage/activity/toActivityDetail?id=' + value + '&nickname='+data.nickname+'">查看</a><a style="margin-left:10px;"  href="javascript: delFunction();">删除</a>';
                    }
                }
                }
            ];

    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: $("#status").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();


    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });

    //删除选中的记录
    function delFunction() {
        BUI.Message.Alert('确定要删除该活动?', function () {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString()
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/activity/deletesJson",
            dataType: "json",
            data: {
                ids: ids
            },
            success: function (data) {
                var obj = new Object();
                obj.start = 0; //返回第一页
                store.load(obj);
            }
        });
        }, 'info');
    }
</script>

</@page.pageBase>
