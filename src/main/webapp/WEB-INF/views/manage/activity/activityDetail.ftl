<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="活动管理">
<style>
    .button-large {
        font-size: 16px;
    }

    hr {
        border-color: #009688;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a href="/rest/manage/activity/toList">返回上一级</a>
            </div>
            <div class="panel-body">
            <#--form表单提交，相当于data-->
                <form id="editForm" class="form-horizontal">
                    <input type="hidden" class="control-text span-width span10" name="id" value="${e.id!}">

                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            活动名称：
                        </label>

                        <div class="controls">
                            <input type="text" data-rules="{required:true}" class="control-text span-width span8"
                                   name="kaName" value="${e.kaName!}" placeholder="请输入活动名称" disabled/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            品牌商：
                        </label>

                        <div class="controls">
                            <input type="text" data-rules="{required:true}" class="control-text span-width span8"
                                   name="nickname" value="" id="brandName" disabled/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            活动要求：
                        </label>

                        <div class="controls  control-row-auto">
                            <textarea class="control-row4 input-large" name="demand" id="content"
                                      value="${e.kaDemand!}" disabled></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            开始日期：
                        </label>

                        <div class="controls">
                            <input type="text" style="width: 140px;" name="kaStarTime"
                                   data-rules="{required : true,sid:只能取往后一天的日期}" data-messages="{required:'只能取往后一天的日期'}"
                                   id="beginTime" class="calendar" placeholder="请选择活动开始日期" value="${e.kaStarTime!}"
                                   disabled//>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            结束日期：
                        </label>

                        <div class="controls">
                            <input type="text" style="width: 140px;" class="calendar time" name="kaEndTime"
                                   data-rules="{required : true}" placeholder="请选择活动结束日期" id="endTime"
                                   value="${e.kaEndTime!}" disabled//>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            活动预算：
                        </label>

                        <div class="controls">
                            <input type="text" data-rules="{required:true}" class="control-text span-width span8"
                                   name="kaBudge" placeholder="请输入活动预算" value="${e.kaBudge!}" disabled/>
                        </div>
                    </div>
                    <h3 class="offset2">活动图片
                    </h3>
                    <hr>
                    <input type="hidden" class="control-text span-width " name="kaPicture" id="picture"
                           value="${e.kaPicture!}">

                    <div id="imagesGrid" class="xm-grid">
                    </div>
                    <h3 class="offset2">商品信息
                    </h3>
                    <hr>
                    <div id="productGrid" class="xm-grid">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Data = BUI.Data;
    var Select = BUI.Select;
    var imagesStore = new Store({});
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            if (data.success == true) {
                window.location.href = "${basepath}/manage/news/toList"
            } else {

            }
        }
    }).render();
    function getQueryString(name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }

    var nickname= getQueryString("nickname");//取活动列表的活动列表中的品牌商名称
    $("#brandName").val(nickname);
    /*--------------------------活动图片列表  begin--------------------*/
    /*--------------------------商品信息列表  begin--------------------*/
    /**
     * 商品信息
     * @type {*[]}
     */

    var specColumns = [
        {
            title: '商品名称', dataIndex: 'kaProductName', elCls: "center"

        },
        {
            title: '条形码', dataIndex: 'kaProductCode', elCls: "center", renderer: function (data) {
            return '<img src="${basepath}/' + data + '" width="100px" height="100px"/>';

        }
        }
    ];

    var id = "${e.id!}";
    var specStore = new Store({});
    var specGrid = new Grid.Grid({
        render: '#productGrid',
        columns: specColumns,
        forceFit: true,
        loadMask: true, //加载数据时显示屏蔽层
        store: specStore
    }).render();
    //查询列表的值
    function productStore() {
        var obj = new Object();
        var productNameArr = "${e.kaProductName!}".split(',');
        var productCodeArr = "${e.kaProductCode!}".split(',');
        for (var j = 0; j < productNameArr.length; j++) {
            obj.kaProductCode = productCodeArr[j];
            obj.kaProductName = productNameArr[j];
            specStore.add(obj);
        }
    }
    productStore();
    /*--------------------------商品信息列表  end--------------------*/
    /**
     * 图片信息
     * @type {*[]}
     */
    var imagesColumns = [
        {
            title: '图片', dataIndex: 'url', width: '30%', renderer: function (data) {
            return '<img src="${basepath}/' + data + '" width="100px" height="100px"/>'
        }
        },
        {
            title: '状态', dataIndex: 'state', width: '30%', renderer: function (data) {
            if (data == "SUCCESS") {
                return "上传成功";
            }
            return "上传失败";
        }
        }
    ];
    /**
     * 删除缓存图片信息
     * @param index
     */
    function delImages(index) {
        var record = imagesStore.findByIndex(index);
        imagesStore.remove(record);
        var imgStr = imagesStore.getResult();
        var imgArr = new Array();
        for (var i = 0; i < imgStr.length; i++) {
            imgArr[i] = imgStr[i].url
        }
        $("#picture").val(imgArr.join(","));
    }
    /**
     * 初始化图片列表数据源
     * */
    function initImagesStore() {
        var imgArr = $("#picture").val().split(',');
        for (var i = 0; i < imgArr.length; i++) {
            var obj = new Object();
            obj.url = imgArr[i];
            obj.state = "SUCCESS";
            imagesStore.add(obj);
        }
    }

    initImagesStore();
    var imagesGrid = new Grid.Grid({
        render: '#imagesGrid',
        width: '100%',//如果表格使用百分比，这个属性一定要设置
        columns: imagesColumns,
        idField: 'title',
        store: imagesStore
    });
    imagesGrid.render();
    /*--------------------------活动图片列表  end--------------------*/

    /*------------------------图片上传插件配置  begin-----------------------*/
    var uploader = WebUploader.create({
        auto: true,
        swf: '${staticpath}/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
        pick: '#imagesBtn',
        resize: false,
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });

    uploader.on('uploadSuccess', function (file, response) {
        uploader.removeFile(file);
        imagesStore.add(response);
        var imgStr = imagesStore.getResult();
        var imgArr = new Array();
        for (var i = 0; i < imgStr.length; i++) {
            imgArr[i] = imgStr[i].url
        }
        $("#picture").val(imgArr.join(","));
    });
    /*------------------------图片上传插件配置  end-----------------------*/
</script>

</@page.pageBase>