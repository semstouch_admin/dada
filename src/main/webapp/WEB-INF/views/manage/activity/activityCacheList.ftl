<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="活动管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }
</style>
<form id="searchForm" class="form-panel" action="">
    <ul class="panel-content">
        <li>
            <a onclick="javascript:history.go(-1);" style="float: left;">返回上一级</a>

            <div class="control-group span5" style="margin-left: 20px;">
                <div class="search-controls  controls">
                    <input type="text" name="storeName" id="title" value="" placeholder="搜索关键字">
                </div>
            </div>
            <div class="form-actions span2" style="margin-left: 15px;">
                <button type="submit" class="button  button-primary">
                    搜索
                </button>
            </div>
        </li>
    </ul>
</form>

<div id="grid"></div>
<script>
    /*--------------------------------------------------------表格数据渲染 start-----------------------------------*/

    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '10%'},
                {title: '城市合伙人', dataIndex: 'cityName', elCls: 'center', width: '15%'},
                {title: '门店', dataIndex: 'storeName', elCls: 'center', width: '30%'},
                {title: '参与时间', dataIndex: 'createTime', elCls: 'center', width: '15%'},
                {
                    title: '活动照片', dataIndex: 'pictureNumber', elCls: 'center', width: '15%',
                    renderer: function (data, obj) {
                        return '<a href="${basepath}/rest/manage/activity/toPictureList?id=' + obj.id + '&cityID=' + obj.cityID + '&activityID=' + obj.activityID + '">' + data + '</a>';
                    }
                },
                {
                    title: '活动信息', dataIndex: 'id', elCls: 'center', width: '15%', renderer: function (value,data) {

                    return '<a  href="${basepath}/rest/manage/activity/toChargeDetail?id=' +data.storeID+ '&nickname='+nickname+'&activityID='+activityID+'&cityID='+data.cityID+'">查看</a>';


                }
                }
            ];
    function GetRequest() {
        var url = decodeURI(decodeURI(location.search)); //获取url中"?"符后的字串，使用了两次decodeRUI解码
        var theRequest = new Object();
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
            }
            return theRequest;
        }
    }
    var postData = GetRequest();
    var activityID= postData.id;
    var nickname = postData.nickname;//获取该活动品牌商的名称
    var store = new Store({
                url: '/rest/manage/activitycharge/selectStoreList',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    activityID: activityID

                },
                pageSize: 10,	// 配置分页数目
                root: 'data',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });


</script>

</@page.pageBase>