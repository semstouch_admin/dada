<#import "../tpl/pageTep.ftl" as page>
    <@page.pageBase currentMenu="活动费用">

    <!--suppress ALL -->
    <form id="searchForm" class="form-panel">
        <ul class="panel-content">
            <li>
                <div class="form-actions">
                    <a href="javascript:add()" class="button button-success">
                        <i class="icon-plus-sign icon-white"></i> 添加
                    </a>
                    <a  class="button button-danger" onclick="return delFunction();">
                        <i class="icon-remove-sign icon-white"></i> 删除
                    </a>
                </div>
            </li>
        </ul>
    </form>
    <div id="grid"></div>
<script>
var Grid = BUI.Grid,
        Store = BUI.Data.Store,
        columns = [
            {title : '主键',dataIndex :'id', width:100},
            {title : '活动ID',dataIndex :'activityID', width:100},
            {title : '城市合伙人ID',dataIndex :'cityID', width:100},
            {title : '门店ID',dataIndex :'storeID', width:100},
            {title : '场地费用',dataIndex :'siteCost', width:100},
            {title : '堆头费用',dataIndex :'tgCost', width:100},
            {title : '促销员费用',dataIndex :'personCost', width:100},
            {title : '补货数量',dataIndex :'productAmount', width:100},
            {title : '提交状态（0不提交、1提交）',dataIndex :'status', width:100},
            {title : '创建时间',dataIndex :'createTime', width:100},
            {title : '创建者',dataIndex :'createUser', width:100},
            {title : '更新时间',dataIndex :'updateTime', width:100},
            {title : '更新者',dataIndex :'updateUser', width:100},
            {title : '操作',dataIndex : 'id',width:200,renderer : function (value,obj,index) {
                    return '<a href="javascript:edit(' +value+ ')">编辑</a>';
                }
            }
        ];

var store = new Store({
            url : 'loadData',
            autoLoad:true, //自动加载数据
            params : { //配置初始请求的参数
                length : '10',
                status:$("#status").val()
            },
            pageSize:10,	// 配置分页数目
            root : 'list',
            totalProperty : 'total'
        }),
        grid = new Grid.Grid({
            render:'#grid',
            columns : columns,
            loadMask: true, //加载数据时显示屏蔽层
            store: store,
            plugins : [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
            // 底部工具栏
            bbar:{
                pagingBar:true
            }
        });

grid.render();


//删除选中的记录
function delFunction(){
    var selections = grid.getSelection();
    var ids=new Array();
    for(var i=0;i<selections.length;i++){
        ids[i]=selections[i].id.toString()
    }
    $.ajax({
        type: "POST",
        url: "${basepath}/manage/activitycharge/deletesJson",
        dataType: "json",
        data: {
            ids:ids
        },
        success: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
        }
    });

}
</script>

</@page.pageBase>
