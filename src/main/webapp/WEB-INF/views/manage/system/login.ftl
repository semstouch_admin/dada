<#import "../tpl/htmlTep.ftl" as html>

<@html.htmlBase checkLogin=false >
<style type="text/css">
    html,body{width: 100%;height:100%;min-width: 1024px;font-family: '黑体';font-size: 16px;}
    h3{font-family: '黑体';}
    body{background: url("${staticpath}/front/dist/images/back.png") no-repeat;background-size: cover;}
    .user_register{margin-right: 10px;}
    .login-box .container-div{  text-align: left;  padding: 35% 15% 0 15%;}
    .login-box h1 {  color: #ffffff;  font-size: 27px;  margin-bottom: 20px;}
    .top-head h3{font-size: 32px;color:#fff;z-index: 1;height: 84px;line-height: 84px;padding-left: 20%;}
    .login-box{margin-right: 20%;}
    .well{background: #fff;box-shadow: none;-webkit-box-shadow:none;margin:0;}
    .w310{width: 88%;margin: 6% auto!important;}
    .button-large{border-radius: 2px;}
    .w310 a,.w310 button{width: 100%;  height: 35px; padding: 0;line-height: 35px;font-size: 16px;color: #fff;text-decoration: none;}
    .mb20{margin-bottom: 10px!important; }
    .error-tips{color: red;margin-left: 30px;padding-top: 10px;}
    .center{text-align: center;}
    .xm-login-panel{margin-top:0;}
    .login-box .panel,.login-box .panel-header{background: #FFFFFF;border-bottom: 0;}
    .login-box input{height: 30px;width: 80%;line-height: 30px;margin:0 auto;}
    .login-box {float: right; width: 30%; height: 100%; background-color: rgba(38, 43, 45, 0.5);margin:0;}
    .panel-header h3{text-indent: 10px;background: #fff;font-size: 23px;}
</style>
<body>
<#--header-->
<div class="row span10 pull-right login-box">
    <div class="container-div">
        <h1>
            嗒达通后台系统
        </h1>
        <div class="panel xm-login-panel">
            <div class="panel-body">
                <form class="form-horizontal well" action="${basepath}/rest/manage/user/login" method="post">
                    <div class="row mb20 center">
                        <input type="text" value="${e.username!""}" placeholder="账号" name="username"
                               class="control-text"
                               id="username" autofocus/>
                    </div>
                    <div class="row center ">
                        <input type="text" onfocus="this.type='password'" name="password" placeholder="密码"
                               class="control-text" label="密码"/>
                    </div>
                    <#if errorMsg??>
                        <div class="row">
                            <div class="error-tips">*${errorMsg}</div>
                        </div>
                    </#if>
                    <div class="row w310">
                        <button type="submit" class="pull-right button button-large button-primary"><a>登陆</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
    </div>
</body>
</@html.htmlBase>