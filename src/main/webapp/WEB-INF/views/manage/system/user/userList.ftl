<#import "../../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="用户设置">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary, .button-success, .button-danger {
        margin-left: 5px;
    }

    .form-horizontal .time-label {
        width: 50px;
        line-height: 40px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }

    [class*="span"] {
        margin-left: 5px;
    }
</style>
<form id="searchForm" class="form-panel">
    <ul class="panel-content">
        <li>

            <div class="control-group span5">
                <div class="search-controls  controls">
                    <input type="text" name="nickname" id="title" value="" placeholder="搜索账号、昵称">
                </div>
            </div>

            <div class="form-actions span2">
                <button type="submit" class="button  button-primary">搜索</button>
            </div>
            <div class="form-actions span8">

                <#if checkPrivilege("/rest/manage/user/insert") >
                    <a href="javascript:add()" class="button button-success">添加</a>
                </#if>
                <a class="button button-danger" href="javascript:delFunction()">
                    删除
                </a>
            </div>
        </li>

    </ul>
</form>
<#--新增弹出框-->
<div id="grid"></div>
<div id="addContent" style="display:none;">
    <form id="addForm" class="form-horizontal" action="${basepath}/rest/manage/user/insertJson" method="post">
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">帐号：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="username" data-rules="{required : true}">
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">昵称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="nickname" data-rules="{required : true}" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">密码：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="password" data-rules="{required : true},{regexp:/^[a-zA-Z0-9]+$/,maxlength:64}" data-messages="{regexp:'请输入英文字母或数字'}" id="password">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">确认密码：</label>

                <div class="controls ">
                    <input type="text" class="input-normal control-text" data-rules="{required : true},{equalTo:'#password'}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">选择角色：</label>

                <div class="controls">
                    <div id="ridSelect">
                        <input type="hidden" id="rid" name="rid" data-rules="{required : true}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">所属城市：</label>

                <div class="controls">
                    <input type="text" id="city" class="input-normal control-text" name="address"  data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">状态：</label>

                <div class="controls control-row4">
                    <select data-rules="{required:true}" name="status">
                        <option value="">-请选择-</option>
                        <option value="y">启用</option>
                        <option value="n">禁用</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<#--编辑弹出框-->
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/user/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">帐号：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="username" data-rules="{required : true}"
                           disabled>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">昵称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="nickname" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">密码：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="password" data-rules="{regexp:/^[a-zA-Z0-9]+$/,maxlength:64}" data-messages="{regexp:'请输入英文字母或数字'}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">选择角色：</label>

                <div class="controls">
                    <div id="editSelect">
                        <input type="hidden" id="editRid" name="rid" data-rules="{required : true}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">所属城市：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="address" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">状态：</label>

                <div class="controls control-row4">
                    <select data-rules="{required:true}" name="status">
                        <option value="">-请选择-</option>
                        <option value="y">启用</option>
                        <option value="n">禁用</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '帐号', dataIndex: 'username', width:'20%',elCls: 'center'},
                {title: '昵称', dataIndex: 'nickname',width:'20%',elCls: 'center'},
                {title: '角色', dataIndex: 'role_name', width:'20%',elCls: 'center'},
                {title: '创建时间', dataIndex: 'createTime', width:'20%',elCls: 'center'},
                {
                    title: '状态', dataIndex: 'status', width:'10%',elCls: 'center', renderer: function (data) {
                    if (data == "y") {
                        return '<img src="${basepath}/resource/images/action_check.gif">';
                    } else {
                        return '<img src="${basepath}/resource/images/action_delete.gif">';
                    }
                }
                },
                {
                    title: '操作', dataIndex: 'id', width:'10%',elCls: 'center', renderer: function (value) {

                    <#if checkPrivilege("/rest/manage/user/edit")>
                        return '<a href="javascript:edit(' + value + ')">编辑</a>';
                    <#else>
                        return "";
                    </#if>
                }
                }
            ];

    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: $("#status").val()
                },
                pageSize: 10,    // 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();

    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });

    //删除选中的记录
    function delFunction() {
        BUI.Message.Confirm('确定要删除该信息?', function () {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString()
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/user/deletesJson",
            dataType: "json",
            data: {
                ids: ids
            },
            success: function (data) {
                var obj = form.serializeToObject();
                obj.start = 0; //返回第一页
                store.load(obj);
            }
        });
        }, 'info');
    };
    /*----------------------------------------------新增表单处理 begin------------------------------------*/
    var Select = BUI.Select, Data = BUI.Data;
    var addStore = new Data.Store({
        url: '${basepath}/rest/manage/role/selectAllList',
        autoLoad: true,
        root: 'data'
    });
    var roleSelect = new Select.Select({
        render: '#ridSelect',
        valueField: '#rid',
        list: {
            itemTpl: '<li>{role_name}</li>',
            idField: 'id',
            elCls: 'bui-select-list'
        },
        store: addStore
    });

    roleSelect.render();

    var addForm = new BUI.Form.Form({
        srcNode: '#addForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            addDialog.close();
        }
    }).render();
    //以下是异步表单验证
    var usernameField = addForm.getField('username');
    var nicknameField = addForm.getField('nickname');

    usernameField.set('remote', {
        url: '${basepath}/rest/manage/user/toEditJson',
        dataType: 'json',//默认为字符串
        callback: function (data) {
            if (data.data == null) {
                return '';
            } else {
                return "用户已存在!";
            }
        }
    });

    nicknameField.set('remote', {
        url: '${basepath}/rest/manage/user/toEditBickName',
        dataType: 'json',//默认为字符串
        callback: function (data) {
            if (data.data == null) {
                return '';
            } else {
                return "昵称已存在!";
            }
        }
    });

    var addDialog = new BUI.Overlay.Dialog({
        title: '新增系统用户',
        width: 500,
        height: 380,
        contentId: 'addContent',
        success: function () {
            if (addForm.isValid()) {
                addForm.submit();
            } else {
                addForm.valid();
            }
        }
    });
    //添加按钮事件
    function add() {
        addForm.clearFields();
        addForm.clearErrors();
        addStore.load();
        addDialog.show();
    }
    /*----------------------------------------------新增表单处理 end------------------------------------*/

    /*----------------------------------------------编辑表单处理 begin------------------------------------*/
    var editStore = new Data.Store({
        url: '${basepath}/rest/manage/role/selectAllList',
        autoLoad: true,
        root: 'data'
    });
    var editSelect = new Select.Select({
        render: '#editSelect',
        valueField: '#editRid',
        list: {
            itemTpl: '<li>{role_name}</li>',
            idField: 'id',
            elCls: 'bui-select-list'
        },
        store: editStore
    });
    editSelect.render();
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            editDialog.close();
        }
    });
    editForm.render();
    var editDialog = new BUI.Overlay.Dialog({
        title: '编辑系统用户',
        width: 500,
        height: 320,
        contentId: 'editContent',
        success: function () {
            editForm.submit();
        }
    });
    //编辑按钮事件
    function edit(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "${basepath}/rest/manage/user/toEditJson",//弹窗编辑
            data: {id: id},
            success: function (data) {
                var form = $("#editForm")[0];
                BUI.FormHelper.setFields(form, data.data);
                editStore.load();
                editDialog.show();
            }
        });
    }
    /*----------------------------------------------编辑表单处理 end------------------------------------*/
</script>

</@page.pageBase>