<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="用户管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .span16 {
        width: 880px;
    }

    .form-horizontal .time-label {
        width: 60px;
        line-height: 40px;
        margin-left: 20px;
    }

    .button-primary {
        margin-left: 5px;
    }

    .form-panel .time-input {
        width: 120px;
    }

    .form-panel select {
        margin-left: 10px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }
</style>
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/user/loadWxData">
    <ul class="panel-content">
        <li>
            <div>
                <div class="control-group span5">
                    <div class="search-controls  controls">
                        <input type="text" name="nickname" id="title" value="" placeholder="搜索类型、名称">
                    </div>
                </div>
                <div class="form-actions span2">
                    <button type="submit" class="button  button-primary">搜索</button>
                </div>
            </div>
            <div>
                <select name="rid" class="input-normal" id="rid">
                    <option value="">查看类型</option>
                    <option value="2">品牌商</option>
                    <option value="3">KA经理</option>
                    <option value="4">城市合伙人</option>
                </select>
                <button type="submit" class="button  button-primary">
                    查询
                </button>
                <a class="button button-danger" href="javascript:delFunction()">
                    批量删除
                </a>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<script>
    /*-------------------------------------------------------- 表格数据渲染start-----------------------------------*/
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '9%'},
                {
                    title: '类型', dataIndex: 'rid', elCls: 'center', width: '15%',
                    renderer: function (data) {
                        if (data == "2") {
                            return '品牌商';
                        } else if (data == "3") {
                            return 'KA经理';
                        } else if (data == "4") {
                            return '城市合伙人';
                        }
                    }
                },
                {title: '名称', dataIndex: 'nickname', elCls: 'center', width: '15%'},
                {
                    title: '发布活动次数',
                    dataIndex: 'issueNumber',
                    elCls: 'center',
                    width: '15%',
                    renderer: function (data, obj) {
                        if (obj.rid == "3" || obj.rid == "4") {
                            return '——';
                        } else {
                            return '<a href="${basepath}/rest/manage/user/toIssueActivityList?id=' + obj.id + '">' + data + '</a>';
                        }

                    }
                },
                {
                    title: '参与活动次数',
                    dataIndex: 'joinNumber',
                    elCls: 'center',
                    width: '15%',
                    renderer: function (data, obj) {
                        if (obj.rid == "2" || obj.rid == "3") {
                            return '——';
                        } else {
                            return '<a href="${basepath}/rest/manage/user/toJoinActivityList?id=' + obj.id + '">' + data + '</a>';
                        }
                    }
                },
                {
                    title: '绑定状态', dataIndex: 'bindStatus', elCls: 'center', width: '15%',
                    renderer: function (data) {
                        if (data == "0") {
                            return '未绑定';
                        } else if (data == "1") {
                            return '已绑定';
                        }
                    }
                },
                {
                    title: '操作', dataIndex: 'id', elCls: 'center', width: '15%', renderer: function (value, data) {
                    if (data.rid == "2") {
                        return '<a  href="${basepath}/rest/manage/user/toBrandDetail?id=' + value + '">查看</a><a style="margin-left:10px;"  href="javascript: delFunction();">删除</a>';
                    } else if (data.rid == "3") {
                        return '<a  href="${basepath}/rest/manage/user/toKaEdit?id=' + value + '">编辑</a><a style="margin-left:10px;"  href="javascript: delFunction();">删除</a>';
                    } else if (data.rid == "4") {
                        return '<a  href="${basepath}/rest/manage/user/toCityEdit?id=' + value + '">编辑</a><a style="margin-left:10px;"  href="javascript: delFunction();">删除</a>';
                    }

                }
                }
            ];

    var store = new Store({
                url: '${basepath}/rest/manage/user/loadWxData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    rid: $("#rid").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();

    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    /**
     * 删除选中记录
     */
    function delFunction() {
        BUI.Message.Alert('确定要删除该账号信息?', function () {
            var selections = grid.getSelection();
            var ids = new Array();
            for (var i = 0; i < selections.length; i++) {
                ids[i] = selections[i].id.toString()
            }
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/account/deletesJson",
                dataType: "json",
                data: {
                    ids: ids
                },
                success: function (data) {
                    var obj = form.serializeToObject();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'info');
    }
    /*-------------------------------------------------------- 表格数据渲染end-----------------------------------*/

</script>
</@page.pageBase>