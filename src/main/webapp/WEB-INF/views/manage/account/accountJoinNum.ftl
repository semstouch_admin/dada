<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="用户管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }
</style>
<form id="searchForm" class="form-panel" action="">
    <ul class="panel-content">
        <li>
            <a onclick="javascript:history.go(-1);" style="float: left;">返回上一级</a>

            <div class="control-group span5" style="margin-left: 20px;">
                <div class="search-controls  controls">
                    <input type="text" name="name" id="title" value="" placeholder="搜索关键字">
                </div>
            </div>
            <div class="form-actions span2" style="margin-left: 15px;">
                <button type="submit" class="button  button-primary">
                    搜索
                </button>
            </div>
        </li>
    </ul>
</form>

<div id="grid"></div>
<script>
    /*--------------------------------------------------------表格数据渲染 start-----------------------------------*/

    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '10%'},
                {title: '活动名称', dataIndex: 'kaName', elCls: 'center', width: '15%'},
                {title: '发布者', dataIndex: 'nickname', elCls: 'center', width: '30%'},
                {title: '活动时间', dataIndex: 'createTime', elCls: 'center', width: '15%'},
                {title: '参与门店数量', dataIndex: 'storeMember', elCls: 'center', width: '15%'},
                {
                    title: '活动状态', dataIndex: 'insertStatus', elCls: 'center', width: '15%',
                    renderer: function (data) {
                        if (data == "1") {
                            return '筹备中';
                        }
                        else if (data == "2") {
                            return '进行中';
                        } else if (data == "3") {
                            return '已结束';
                        }
                    }
                }
            ];
    function getQueryString(name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }
    var id = getQueryString("id");
    var store = new Store({
                url: '/rest/manage/activity/loadIssueData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    brandID: id
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });


</script>

</@page.pageBase>