<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="用户管理">
<style>
    /***循环的表单设置***/
    .bui-select .bui-select-input {
        width: 250px;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="returnBtn panel-header">
                <a href="${basepath}/rest/manage/account/toList">返回上一级</a>
            </div>
            <div class="panel-body">
                <form id="detailForm" class="form-horizontal">
                    <input type="hidden" class="control-text span-width span10" name="id" value="${e.id!}"
                           id="id"/>

                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            填写名称：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="nickname"
                                   value="${e.nickname!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            填写帐号：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="username"
                                   value="${e.username!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            填写密码：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="password"
                                   value="${e.password!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            品牌商：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="brandName"
                                   value="${e.brandName!}" disabled="disabled" id="brandName"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            类型：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name=""
                                   value="kA经理" disabled="disabled"/>
                        </div>
                    </div>
                </form>
                <form id="editForm" class="form-horizontal">
                    <input type="hidden" class="control-text span-width span5" name="id" value="${e.id!}">

                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            绑定品牌商：
                        </label>

                        <div class="controls" id="brandSelect">
                            <input type="hidden" id="brand" name="brandID" class="control-text span-width span5"
                                   value="${e.brandID!}">
                        </div>
                    </div>
                    <h3 class="offset2">信息
                        <a class="button button-primary pull-right" style="height:20px;margin-left: 10px"
                           href="javascript:allUnBindFunction()">全部解绑</a>
                        <a class="button button-primary pull-right" style="height:20px;"
                           href="javascript: allBindFunction();">全部绑定</a>

                    </h3>
                    <hr/>
                    <div id="grid"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Overlay = BUI.Overlay;
    var Select = BUI.Select, Data = BUI.Data;
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
        }
    }).render();


    /*--------------------------选择品牌商下拉选择框列表 begin --------------------*/
    var brandStore = new Data.Store({
        url: '/rest/manage/kcrelation/selectUnBindList',
        autoLoad: true,
        params: {
            rid: 2,
            bindStatus: 0
        },
        root: 'data'
    });
    var brandSelect = new Select.Select({
        render: '#brandSelect',
        valueField: '#brand',
        list: {
            itemTpl: '<li>{nickname}</li>',
            idField: 'id',
            elCls: 'bui-select-list'
        },
        store: brandStore
    });
    brandSelect.render();
    /*--------------------------选择品牌商下拉选择框列表  end--------------------*/
    var Columns = [
        {
            title: '序号', dataIndex: 'cityID', width: '10%'
        },
        {
            title: '城市合伙人', dataIndex: 'nickname', width: '70%'
        },
        {
            title: '操作', dataIndex: 'id', width: '20%', renderer: function (value, data) {
            if (data.bindStatus == "0") {
                return '<a href="javascript: allBindFunction();">绑定</a>';
            } else if (data.bindStatus == "1") {
                return '<a href="javascript: allUnBindFunction();">解绑</a>';
            }

        }
        }
    ];
    var store = new Store({
        url: '/rest/manage/kcrelation/selectCityList',
        autoLoad: true,
        params: {
            kaID: $("#id").val()
        },
        root: 'data'
    });
    var grid = new Grid.Grid({
        render: '#grid',
        width: '100%',//如果表格使用百分比，这个属性一定要设置
        columns: Columns,
        idField: 'title',
        store: store,
        plugins: [Grid.Plugins.CheckSelection] // 插件形式引入多选表格
    });
    grid.render();
    var brandid = '${e.brandID!}';
    //全部绑定
    function allBindFunction() {
        if ($("#brandName").val() == '无') {
            if ($("#brand").val() == '' || $("#brand").val() == null) {
                BUI.Message.Alert('请先选择品牌商', function () {
                }, 'info');
            }else{
                BUI.Message.Alert('确定要执行该操作?', function () {
                    var selections = grid.getSelection();
                    var ids = new Array();
                    for (var i = 0; i < selections.length; i++) {
                        ids[i] = selections[i].cityID.toString()
                    }
                    $.ajax({
                        type: "POST",
                        url: "/rest/manage/kcrelation/insertKcrelations",
                        dataType: "json",
                        data: {
                            cityIDs: ids,
                            kaID: $("#id").val(),
                            brandID: $("#brand").val()
                        },
                        success: function (data) {
                            var obj = editForm.serializeToObject();
                            obj.start = 0; //返回第一页
                            store.load(obj);
                        }
                    });
                }, 'info');
            }
        } else{
            BUI.Message.Alert('确定要执行该操作?', function () {
                var selections = grid.getSelection();
                var ids = new Array();
                for (var i = 0; i < selections.length; i++) {
                    ids[i] = selections[i].cityID.toString()
                }
                $.ajax({
                    type: "POST",
                    url: "/rest/manage/kcrelation/insertKcrelations",
                    dataType: "json",
                    data: {
                        cityIDs: ids,
                        kaID: $("#id").val(),
                        brandID: brandid//品牌商id取值不到
                    },
                    success: function (data) {
                        var obj = editForm.serializeToObject();
                        obj.start = 0; //返回第一页
                        store.load(obj);
                    }
                });
            }, 'info');
        }
    }
    //全部解绑
    function allUnBindFunction() {
        BUI.Message.Alert('确定要执行该操作?', function () {
            var selection = grid.getSelection();
            var cityIDs = new Array();
            for (var i = 0; i < selection.length; i++) {
                cityIDs[i] = selection[i].cityID.toString()
            }
            $.ajax({
                type: "POST",
                url: "/rest/manage/kcrelation/deleteKcrelations",
                dataType: "json",
                data: {
                    cityIDs: cityIDs,
                    kaID: $("#id").val(),
                    brandID: brandid
                },
                success: function (data) {
                    var obj = editForm.serializeToObject();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });

        }, 'info');
    }
</script>

</@page.pageBase>