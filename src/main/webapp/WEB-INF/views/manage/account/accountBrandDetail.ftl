<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="用户管理">
<style>
    /***循环的表单设置***/
    .bui-select .bui-select-input {
        width: 250px;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="returnBtn panel-header">
                <a href="${basepath}/rest/manage/account/toList">返回上一级</a>
            </div>
            <div class="panel-body">
                <form id="detailForm" class="form-horizontal" action="${basepath}/manage/offline/updateJson"
                      method="get">
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            填写名称：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="nickname"
                                   value="${e.nickname!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            填写帐号：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="username"
                                   value="${e.username!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            填写密码：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="password"
                                   value="${e.password!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            类型：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name=""
                                   value="品牌商" disabled="disabled"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Overlay = BUI.Overlay;
    var editForm = new BUI.Form.Form({
        srcNode: '#detailForm',
        submitType: 'ajax',
        callback: function (data) {
        }
    }).render();
</script>

</@page.pageBase>