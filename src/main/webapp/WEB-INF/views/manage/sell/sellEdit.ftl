<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="新增报表">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }
</style>
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/sell/loadDataDetail">
    <ul class="panel-content">
        <li>
            <a onclick="javascript:history.go(-1);" style="float: left;">返回上一级</a>

            <div class="control-group span5" style="margin-left: 20px;">
                <div class="search-controls  controls">
                    <input type="text" name="brandName" id="title" value="" placeholder="搜索关键字">
                </div>
            </div>
            <div class="form-actions span2" style="margin-left: 15px;">
                <button type="submit" class="button  button-primary">
                    搜索
                </button>
            </div>
            <div class="form-actions span2">
                <a class="button button-success"
                   href="${basepath}/rest/manage/sell/excelDownload?createTime=${createTime!}">
                    导出
                </a>
            </div>
        </li>
    </ul>
</form>

<div id="grid"></div>
<#---------------------------------------------编辑弹出框--------------------------------------------------------->
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/sell/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">报表名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="reportName"
                           data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">品牌商：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="brandName"
                           data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">KA经理：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="kaName" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">城市合伙人：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="cityName" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">门店号：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="storeID" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">门店名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="storeName" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">商品名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="product" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">条形码：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="barCode" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">销售金额：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="amount"
                           data-rules="{required : true,number : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">销售数量：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="sales"
                           data-rules="{required : true,number : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">地区：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="area" data-rules="{required : true}">
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    /*--------------------------------------------------------表格数据渲染 start-----------------------------------*/
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '报表名称', dataIndex: 'reportName', elCls: 'center', width: '10%'},
                {title: '品牌商', dataIndex: 'brandName', elCls: 'center', width: '9%'},
                {title: 'KA经理', dataIndex: 'kaName', elCls: 'center', width: '9%'},
                {title: '城市合伙人', dataIndex: 'cityName', elCls: 'center', width: '9%'},
                {title: '门店号', dataIndex: 'storeID', elCls: 'center', width: '9%'},
                {title: '门店名称', dataIndex: 'storeName', elCls: 'center', width: '9%'},
                {title: '商品名称', dataIndex: 'product', elCls: 'center', width: '9%'},
                {title: '条形码', dataIndex: 'barCode', elCls: 'center', width: '9%'},
                {title: '销售金额', dataIndex: 'amount', elCls: 'center', width: '9%'},
                {title: '销售数量', dataIndex: 'sales', elCls: 'center', width: '9%'},
                {title: '地区', dataIndex: 'area', elCls: 'center', width: '9%'},
                {
                    title: '操作', dataIndex: 'id', elCls: 'center', width: '9%', renderer: function (value) {
                    var option1 = '<a href="javascript:editFunction(' + value + ')">编辑</a>';
                    var option2 = '<a href="javascript:delFunction(' + value + ')">删除</a>';
                    return option1 + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + option2;
                }
                }
            ];

    var store = new Store({
                url: '/rest/manage/sell/loadDataDetail',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    createTime: '${createTime!}'
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });

    //删除记录
    function delFunction(value) {
        BUI.Message.Confirm('确认要删除吗？', function () {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/sell/delete",
                dataType: "json",
                data: {
                    id: value
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    /*-------------------------------------------编辑弹出框-------------------------------------------*/
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            editDialog.close();
        }
    });
    editForm.render();
    var editDialog = new BUI.Overlay.Dialog({
        title: '编辑',
        width: 480,
        height: 530,
        contentId: 'editContent',
        success: function () {
            editForm.submit();
        }
    });
    //编辑按钮事件
    function editFunction(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "${basepath}/rest/manage/sell/toEditJson",//弹窗编辑
            data: {id: id},
            success: function (data) {
                var form = $("#editForm")[0];
                BUI.FormHelper.setFields(form, data.data);
                editForm.clearErrors();
                editDialog.show();
            }
        });
    }
</script>

</@page.pageBase>