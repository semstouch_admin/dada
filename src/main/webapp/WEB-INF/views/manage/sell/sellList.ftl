<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="数据录入">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
        margin-left: 3px;
    }

    .button-primary {
        margin-left: -10px;
    }

    .button-success {
        margin-left: -26px;
    }

    .button-danger {
        margin-left: -25px;
    }
</style>
<!--suppress ALL -->
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/sell/loadData">
    <ul class="panel-content">
        <li>
            <div class="control-group span5">
                <div class="search-controls  controls">
                    <input type="text" name="createTime" id="title" value="" placeholder="搜索关键字">
                </div>
            </div>
            <div class="form-actions span2">
                <button type="submit" class="button  button-primary">
                    搜索
                </button>
            </div>
            <div class="form-actions span2">
                <a class="button button-success" href="javascript:add()">
                    <i class="icon-plus-sign icon-white"></i> 添加
                </a>
            </div>
            <div class="form-actions span2">
                <a class="button button-danger" href="javascript:delFunction()">批量删除</a>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<#------------------------------------------导入Excel弹出框-------------------------------------------------->

<div id="uploader" class="wu-example" style="display:none">
    <div>
        <form>
            <p>
                <label>选择导入日期：</label><input type="text" name="createTime" class="calendar"
                                             data-rules="{required : true}"/>
            </p>
        </form>
    </div>
    <div class="btns">
        <div id="picker">选择Excel文件</div>
    </div>
    <!--用来存放文件信息-->
    <div id="thelist" class="uploader-list"></div>
</div>

<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '报表时间', dataIndex: 'createTime', elCls: 'center', width: '20%'},
                {title: '上传者', dataIndex: 'createUser', elCls: 'center', width: '15%'},
                {title: '品牌商数量', dataIndex: 'brandNum', elCls: 'center', width: '15%'},
                {title: 'KA经理数量', dataIndex: 'kaNum', elCls: 'center', width: '15%'},
                {title: '城市合伙人数量', dataIndex: 'cityNum', elCls: 'center', width: '15%'},
                {
                    title: '操作', dataIndex: 'createTime', elCls: 'center', width: '15%', renderer: function (value) {
                    var option1 = '<a href="${basepath}/rest/manage/sell/toEditList?createTime=' + value + '">编辑</a>';
                    var option2 = '<a href="javascript:delFunction()">删除</a>';
                    return option1 + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + option2;
                }
                }
            ];

    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();
    //搜索框表单
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    //删除选中的记录
    function delFunction() {
        BUI.Message.Confirm('确认要删除吗？', function () {
            var selections = grid.getSelection();
            var times = new Array();
            for (var i = 0; i < selections.length; i++) {
                times[i] = selections[i].createTime.toString();
            }
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/sell/deletesByTimes",
                dataType: "json",
                data: {
                    times: times
                },
                success: function (data) {
                    var obj = new Object();
                    obj.start = 0; //返回第一页
                    store.load(obj);
                }
            });
        }, 'question');
    }
    /*********************************文件上传弹出框**********************************************/
    var addDialog = new BUI.Overlay.Dialog({
        title: '导入Excel文件',
        width: 400,
        height: 250,
        contentId: 'uploader',  //弹出框显示内容的DOM容器ID
        success: function () {   //每次点击弹出框的确定时执行上传文件
            uploader.upload();
        }
    });
    function add() {//点击添加时弹出框显示
        $("#thelist").empty(); //每次点击添加时清除$("#thelist")元素的子元素
        $(".calendar").val('');
        addDialog.show();
        $(".webuploader-pick").next().css({"width": "107px", "height": "40px"});           //更改样式防止找不到按钮
    }
    /******************************基于webupload插件弹出框文件上传********************************/
    var uploader = WebUploader.create({
        auto: false,
        // swf文件路径
        swf: '${staticpath}/ueditor/third-party/webuploader/Uploader.swf',
        // 文件接收服务端。
        server: '${basepath}/rest/manage/sell/excelUpload',
        // 选择文件的按钮。可选。
        pick: '#picker',
        //设置事件监听超时时间，0表示不设置超时时间
        timeout: 30 * 60 * 1000,
        // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
        resize: false,
        duplicate: true
    });
    uploader.on('beforeFileQueued', function (file) {
        if ($(".calendar").val() == null || $(".calendar").val() == '') {
            BUI.Message.Alert('请选择文件导入日期！', 'info');
            return false;
        } else {
            uploader.options.formData.createTime = $(".calendar").val();
        }
    });
    //验证进来的文件是否为Excel文件
    uploader.on('fileQueued', function (file) {
        $("#thelist").empty();//每次有文件进来先清理之前提示信息
        var fileType = file.name.substr(file.name.lastIndexOf("."));
        if (fileType == '.xls' || fileType == '.xlsx') {
            $("#thelist").append('<div id="' + file.id + '" class="item">' +
                    '<h4 class="info">' + file.name + '</h4>' +
                    '<p class="state">等待上传...</p>' +
                    '</div>');
        }
        else {
            BUI.Message.Alert('请选择Excel文件！', 'error');
        }
    });

    uploader.on('uploadProgress', function (file, percentage) {
        var $li = $('#' + file.id);
        $li.find('p.state').text('上传中，如果数据量较大请耐心等待哦...');
    });
    //文件上传成功
    uploader.on('uploadSuccess', function (file) {
        $('#' + file.id).find('p.state').text('已上传');
        BUI.Message.Alert('文件导入成功！', 'success');
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj); //重新从第一页开始加载数据
        addDialog.close();  //文件上传提交成功，弹出框关闭
    });
    //文件上传失败
    uploader.on('uploadError', function (file) {
        BUI.Message.Alert('上传成功，但文件部分填写错误或未填写完整！', 'success');
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj); //重新从第一页开始加载数据
        addDialog.close();
    });
    //文件上传结束
    uploader.on('uploadComplete', function (file) {
        $('#' + file.id).find('.progress').fadeOut();
    });
    /*-------------------------------BUI日期控件渲染------------------------------------*/
    BUI.use('bui/calendar', function (Calendar) {
        var datepicker = new Calendar.DatePicker({
            trigger: '.calendar',
            autoRender: true
        });
        datepicker.on('selectedchange', function (ev) {
            var selectedDate = $(".calendar").val();
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '${basepath}/rest/manage/sell/toValid',
                data: {
                    createTime: selectedDate
                },
                success: function (data) {
                    var rsData = data.data;
                    if (rsData.length > 0) {
                        $(".calendar").val('');
                        BUI.Message.Alert('该日期报表已经存在，请选择其他报表日期！', 'error');
                    }
                }
            })
        });
    });
</script>
</@page.pageBase>
