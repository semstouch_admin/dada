<#macro menu menus=[]  currentMenu="首页" >
<!-- /.navbar-top-links -->
<ul  id="side-menu">
</ul>
<!-- /.sidebar-collapse -->
<!-- /.navbar-static-side -->
<script type="text/javascript">
    var Menu = BUI.Menu;
    var data =[
        <#list menus as menu>
            {
                text: '${menu.name}',
                <#assign collapsed="true">
                <#if menu.children?? && menu.children?size gt 0>
                    items: [
                        <#list menu.children as menu>
                            {
                                text: '${menu.name}',
                                href: '${rootPath}${menu.url}',
                                <#if menu.name==currentMenu>
                                    selected:true
                                    <#assign collapsed="false">
                                </#if>
                            },
                        </#list>
                    ],
                    collapsed:${collapsed},
                <#else>
                    collapsed:${collapsed},
                    href:'${menu.url}'
                </#if>
            },
        </#list>
    ];
    var i=0;
    var sideMenu = new Menu.SideMenu({
        render:'#side-menu',
        width:200,
        prefixCls:'xm-',
        itemTpl:'</a><a class="xm-menu-title " href="{href}" data-id=""  data-status="0" ><div class="inb">{text}</div><i class="iconfont icon icon-more" ></i></a>',
        collapsedCls:'xm-menu-title',
        subMenuItemTpl : '<a class="link" href="{href}">{text}</a>',
        items :data
    });

    sideMenu.render();
    sideMenu.on('itemclick', function(e){
        var menuHref =e.item.get('href');
        if(typeof(menuHref)!="undefined") {
            window.location.href = menuHref;
        }

    });
    $(function(){
      var act=$('.active').text();
        $("a:contains('"+act+"')").attr('data-status','1').parents('.xm-menu-item').addClass('act').find(".icon").addClass('icon-more1');
        $('.menu-second').click(function(){
          var ico= $(this).find(".icon");
           if($(this).find(".menu-leaf").length)
           {$(this).addClass('act').siblings().removeClass('act').find('.icon').removeClass('icon-more1');
            ico.hasClass("icon-more1") ? ico.removeClass('icon-more1'):ico.addClass('icon-more1');
           }
        });
    });
    $('.xm-menu-title').each(function(i,e){
        var a=i+1;
        var cal="icon-"+a;
        $(this).prepend('<i class="iconfont  ico '+cal+'" ></i>')
    });
    $('.menu-leaf').each(function(i,e){
        var a=i+1;
        var cal="icon-a"+a;
        $(this).prepend('<i class="iconfont  ico '+cal+'" ></i>')
    })
</script>
</#macro>