<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="活动管理">
<style>
    .div-s{
        font-size: 16px;
        font-weight: 500;
        float: left;
    }
    .img-controls {
        margin-left: 50px;
        float: left;
    }
    .img-controls p {
        margin: 0 !important;

    }
    .img-controls div {
        width: 100%;
        height: 120px;
    }

    .img-controls img {
        float: left;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a onclick="javascript:history.go(-1);">返回上一级</a>
            </div>
            <div class="panel-body">
                <div class="div-s">活动照片：</div>
                <div class="img-controls">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getQueryString(name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }
    var id = getQueryString("id");
    var cityID = getQueryString("cityID");
    var activityID = getQueryString("activityID");
    $.ajax({
        type: 'get',
        url: '/rest/manage/activitypicture/selectAllList',
        data: {
            storeID: id,
            cityID: cityID,
            activityID: activityID
        },
        dataType: "json",
        success: function (data) {
            var list = data.data;
            var listHtml = "";
            $.each(list, function (i, o) {
                listHtml +='<p>'+ o.createTime+'</p><div>';
                var imgArr= o.picture.split(',');
                for(var j=0;j < imgArr.length; j++){
                    listHtml +='<img src="/'+imgArr[j]+'"/>';
                }
                listHtml +='</div><p>'+ o.location+'</p>';

            });
            $(".img-controls").html(listHtml);

        }
    })
</script>

</@page.pageBase>