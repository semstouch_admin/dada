<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="键值对管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary, .button-success, .button-danger {
        margin-left: 5px;
    }

    .form-horizontal .time-label {
        width: 26px;
        line-height: 40px;
    }

    .form-horizontal .value-label {
        width: 20px;
        line-height: 40px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }

    [class*="span"] {
        margin-left: 5px;
    }
</style>
<form id="searchForm" class="form-panel">
    <ul class="panel-content">
        <li>
            <div class="control-group span6">
                <label class="time-label control-label">键：</label>

                <div class="time-controls controls">
                    <input type="text" name="key1" id="key1" value="${e.key1!""}">
                </div>
            </div>

            <label class="value-label control-label">值：</label>

            <div class="time-controls controls">
                <input type="text" name="value" id="value" value="${e.value!""}">
            </div>

            <div class="form-actions span8">
                <button type="submit" class="button button-primary">查询
                </button>
                <a href="javascript:add()" class="button button-success">
                    添加</a>
                <button class="button button-danger" onclick="return delFunction()">删除
                </button>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<div id="addContent" style="display:none;">
    <form id="addForm" class="form-horizontal" action="${basepath}/rest/manage/keyvalue/insertJson" method="post">
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">键：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="key1" data-rules="{required : true}">
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">值：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="value" data-rules="{required : true}">
                </div>
            </div>
        </div>


    </form>
</div>

<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/keyvalue/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">键：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="key1" data-rules="{required : true}">
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label">值：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="value" data-rules="{required : true}">
                </div>
            </div>
        </div>

    </form>
</div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '编号', dataIndex: 'id', width:'10%',elCls: 'center'},
                {title: '键', dataIndex: 'key1', width:'40%',elCls: 'center'},
                {title: '值', dataIndex: 'value', width:'40%',elCls: 'center'},
                {
                    title: '操作', dataIndex: 'id', width:'10%',elCls: 'center', renderer: function (value) {
                    return '<a href="javascript:edit(' + value + ')">编辑</a>';
                }
                }
            ];

    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    key1: $("#key1").val(),
                    value: $("#value").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });

    grid.render();

    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });

    //删除选中的记录
    function delFunction() {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString()
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/keyvalue/deletesJson",
            dataType: "json",
            data: {
                ids: ids
            },
            success: function (data) {
                var obj = form.serializeToObject();
                obj.start = 0; //返回第一页
                store.load(obj);
            }
        });

    }


    /*----------------------------------------------新增表单处理 begin------------------------------------*/
    var addForm = new BUI.Form.Form({
        srcNode: '#addForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            addDialog.close();
        }
    }).render();

    var addDialog = new BUI.Overlay.Dialog({
        title: '新增键值对',
        width: 500,
        height: 250,
        contentId: 'addContent',
        success: function () {
            if (addForm.isValid()) {
                addForm.ajaxSubmit();
            } else {
                addForm.valid();
            }
        }
    });
    //添加按钮事件
    function add() {
        addDialog.show();
    }
    /*----------------------------------------------新增表单处理 end------------------------------------*/

    /*----------------------------------------------编辑表单处理 begin------------------------------------*/
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            editDialog.close();
        }
    });
    editForm.render();

    var editDialog = new BUI.Overlay.Dialog({
        title: '编辑键值对',
        width: 500,
        height: 250,
        contentId: 'editContent',
        success: function () {
            editForm.ajaxSubmit();
        }
    });
    //编辑按钮事件
    function edit(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "${basepath}/rest/manage/keyvalue/toEditJson",
            data: {id: id},
            success: function (data) {
                var form = $("#editForm")[0];
                BUI.FormHelper.setFields(form, data.data);
                editDialog.show();
            }
        });
    }
    /*----------------------------------------------编辑表单处理 end------------------------------------*/

</script>

</@page.pageBase>