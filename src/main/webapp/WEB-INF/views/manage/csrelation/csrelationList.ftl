<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="门店录入">
<style>
    .form-horizontal .controls {
           line-height: 40px;
           height: 40px;
       }

       .span16 {
           width: 880px;
       }

       .form-horizontal .time-label {
           width: 60px;
           line-height: 40px;
           margin-left: 20px;
       }

       .button-primary {
           margin-left: 5px;
       }

       .form-panel .time-input {
           width: 120px;
       }

       .form-panel select {
           margin-left: 10px;
       }

       .form-horizontal .time-controls {
           margin-left: 5px;
       }
</style>
<!--suppress ALL -->
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/csrelation/loadData">
    <ul class="panel-content">
        <li>
            <div>
                <div class="control-group span5">
                    <div class="search-controls  controls">
                        <input type="text" name="nickname" id="title" value="" placeholder="搜索类型、名称">
                    </div>
                </div>
                <div class="form-actions span2">
                    <button type="submit" class="button  button-primary">搜索</button>
                </div>
            </div>
            <div class="form-actions">
                <a href="${basepath}/rest/manage/csrelation/toAdd" class="button button-success">新增
                </a>
                <a class="button button-danger" href="javascript:delFunction()">
                    批量删除
                </a>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '10%'},
                {title: '合伙人名称', dataIndex: 'nickname', elCls: 'center', width: '20%'},
                {title: '所在地区', dataIndex: 'address', elCls: 'center', width: '30%'},
                {title: '门店数量', dataIndex: 'number', elCls: 'center', width: '20%'},
                {
                    title: '操作', dataIndex: 'id', elCls: 'center', width: '20%', renderer: function (value) {
                    return '<a href="${basepath}/rest/manage/csrelation/toEdit?id=' + value + '">编辑</a><a style="margin-left:10px;"  href="javascript: delFunction();">删除</a>';
                }
                }
            ];
    var store = new Store({
                url: '${basepath}/rest/manage/csrelation/loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: '1'
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });

    grid.render();
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });

    //删除选中的记录
    function delFunction() {
        BUI.Message.Alert('确定要删除该信息?', function () {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString()
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/csrelation/deletesJson",
            dataType: "json",
            data: {
                ids: ids
            },
            success: function (data) {
                var obj = new Object();
                obj.start = 0; //返回第一页
                store.load(obj);
            }
        });
        }, 'info');
    }
</script>

</@page.pageBase>
