<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="门店录入">
<link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/dpl.css" rel="stylesheet">
<style>
    .button-large {
        padding: 10px 30px;
        font-size: 16px;
    }

    hr {
        border-color: #009688;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a href="${basepath}/rest/manage/csrelation/toList" class="saveBtn">返回上一级</a>
            </div>
            <div class="panel-body">
                <form id="J_Form" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            选择合伙人：
                        </label>

                        <div class="controls" id="brandSelect">
                            <input type="hidden" id="brand" name="cityIDs" data-rules="{required:true}">
                        </div>
                    </div>
                    <h3 class="offset2">门店信息
                        <a class="button button-primary pull-right" href="javascript:add()" style="height:20px">新增</a>
                    </h3>
                    <hr/>
                    <input type="hidden" class="control-text span-width " name="ids">

                    <div id="grid"></div>
                  <br />
                    <div class="centered">
                        <a class="button  button-large  button-success saveBtn">保存</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<#-------------------------------------------------------新增弹出框 begin-------------------------------------------------------->
<div id="addContent" style="display:none;">
    <form id="addCityForm" class="form-horizontal" action="${basepath}/rest/manage/csrelation/insertJson" method="post">
        <div class="row">
            <div class="control-group span16">
                <label class="control-label"> <s>*</s>门店号：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="storeID" id="storeID"
                           data-rules="{regexp:/^[a-zA-Z0-9]+$/,maxlength:64}" data-messages="{regexp:'请输入英文字母或数字'}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label"> <s>*</s>门店名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="storeName"
                           data-rules="{required : true}" id="storeName"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label"> <s>*</s>门店地址：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="storeAddress"
                           data-rules="{required : true}" id="storeAddress"/>
                </div>
            </div>
        </div>
    </form>
</div>
<#-------------------------------------------------------新增弹出框 end-------------------------------------------------------->
<#-------------------------------------------------------编辑弹出框 begin-------------------------------------------------------->
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/csrelation/updateJson"
          method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label"> <s>*</s>门店号：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="storeID" id="editStoreID"
                           data-rules="{regexp:/^[a-zA-Z0-9]+$/,maxlength:64}" data-messages="{regexp:'请输入英文字母或数字'}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label"> <s>*</s>门店名称：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="storeName"
                           data-rules="{required : true}"/>
                </div>
            </div>
            <div class="control-group span16">
                <label class="control-label"> <s>*</s>门店地址：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="storeAddress"
                           data-rules="{required : true}"/>
                </div>
            </div>
        </div>
    </form>
</div>
<#-------------------------------------------------------编辑弹出框 end-------------------------------------------------------->
<script type="text/javascript">
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Overlay = BUI.Overlay;
    var Select = BUI.Select, Data = BUI.Data;
    BUI.use('bui/form', function (Form) {
        new Form.Form({
            srcNode: '#J_Form'
        }).render();
    });
    /*--------------------------选择合伙人下拉选择框列表 begin --------------------*/
    var brandStore = new Data.Store({
        url: '/rest/manage/account/selectAllCity',
        autoLoad: true,
        root: 'data'
    });
    var brandSelect = new Select.Select({
        render: '#brandSelect',
        valueField: '#brand',
        list: {
            itemTpl: '<li>{nickname}</li>',
            idField: 'id',
            elCls: 'bui-select-list'
        },
        store: brandStore
    });
    brandSelect.render();
    /*--------------------------选择合伙人下拉选择框列表  end--------------------*/
    /*----------------------------------------------新增表单处理 begin------------------------------------*/
    //添加按钮事件
    function add() {
        addCityForm.reset();  //表单重置
        addCityForm.clearErrors();
        $("#storeID").next().remove();
        if ($("#brand").val() == '') {
            BUI.Message.Alert('请选择城市合伙人', function () {
            }, 'info');
            return;
        } else {
            addDialog.show();
        }

    }
    var addCityForm = new BUI.Form.Form({
        srcNode: '#addCityForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            addDialog.close();
        }
    }).render();
    addCityForm.setInternal('initRecord',addCityForm.serializeToObject());

    //表单验证,判断门店号是否已存在
    $("#storeID").keyup(function(){
        if($("#storeID").val()==null||$("#storeID").val()==''){
            $("#storeID").next().remove();
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/csrelation/toValid",
            data: {
                storeID:$("#storeID").val()
            },
            dataType: "json",
            success: function (data) {
                if (data.data == null) {
                    $("#storeID").next().remove();
                } else {
                    $("#storeID").next().remove();
                    $("#storeID").after('<span class="x-field-error"><span class="x-icon x-icon-mini x-icon-error">!</span><label class="x-field-error-text">门店号已存在！</label></span>')
                }
            }
        })
    });

    var addDialog = new BUI.Overlay.Dialog({
        title: '新增',
        width: 455,
        height: 250,
        contentId: 'addContent',
        success: function () {
           /* if (addCityForm.isValid()) {
                addAjax();
            } else {
                addCityForm.valid();
            }*/
            addCityForm.submit();

        }
    });

    /*----------------------------------------------新增表单处理 end------------------------------------*/
    var ids = new Array();
    var sids = '';
    var columns = [
        {
            title: '门店号', dataIndex: 'storeID', width: '20%', elCls: 'center'
        },
        {
            title: '门店名称', dataIndex: 'storeName', width: '30%', elCls: 'center'
        },
        {
            title: '门店地址', dataIndex: 'storeAddress', width: '30%', elCls: 'center'
        },
        {
            title: '操作', dataIndex: 'id', width: '20%', elCls: 'center', renderer: function (data) {
            sids += data + " ";
            return '<a  href="javascript:edit(' + data + ')">编辑</a><a style="margin-left:10px;"  href="javascript: delFunction(' + data + ');">删除</a>';
        }
        }
    ];
    var store = new Store({
        url: '${basepath}/rest/manage/csrelation/selectWithoutCtiyID',
        autoLoad: true,
        root: 'data'
    });
    var grid = new Grid.Grid({
        render: '#grid',
        width: '100%',//如果表格使用百分比，这个属性一定要设置
        columns: columns,
        idField: 'title',
        store: store,
        plugins: [Grid.Plugins.CheckSelection]// 插件形式引入多选表格
    });
    grid.render();
    //删除选中的记录
    function delFunction(id) {
        BUI.Message.Confirm('确定要删除该门店', function () {
            $.ajax({
                type: "POST",
                url: "${basepath}/rest/manage/csrelation/deleteJson",
                dataType: "json",
                data: {
                    id: id
                },
                success: function (data) {
                    store.load();
                }
            })
        }, 'info');
    }
    /*----------------------------------------------编辑表单处理 begin------------------------------------*/
    //编辑按钮事件
    function edit(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "${basepath}/rest/manage/csrelation/toEditJson",//弹窗编辑
            data: {id: id},
            success: function (data) {
                var form = $("#editForm")[0];
                BUI.FormHelper.setFields(form, data.data);
                editDialog.show();

            }
        });
    }
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            editDialog.close();
        }
    });
    editForm.render();
    var editDialog = new BUI.Overlay.Dialog({
        title: '编辑',
        width: 455,
        height: 250,
        contentId: 'editContent',
        success: function () {
            editForm.submit();
        }
    });
    //表单验证,判断门店号是否已存在
    $("#editStoreID").keyup(function(){
        if($("#editStoreID").val()==null||$("#editStoreID").val()==''){
            $("#editStoreID").next().remove();
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/csrelation/toValid",
            data: {
                storeID:$("#editStoreID").val()
            },
            dataType: "json",
            success: function (data) {
                if (data.data == null) {
                    $("#editStoreID").next().remove();
                } else {
                    $("#editStoreID").next().remove();
                    $("#editStoreID").after('<span class="x-field-error"><span class="x-icon x-icon-mini x-icon-error">!</span><label class="x-field-error-text">门店号已存在！</label></span>')
                }
            }
        })
    });
    //添加所有的信息
    $(".saveBtn").click(function () {
        //获取ids
        ids = sids.replace(/(\s*$)/g, "").split(" ");
        if ($("#brand").val() == '' || $("#CityAddress").val() == '' || ids == '') {
            BUI.Message.Alert('信息填写不完整', function () {
            }, 'info');
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/csrelation/updateAllCityID",
            data: {
                cityIDs: $("#brand").val(),
                CityAddress: $("#CityAddress").val(),
                ids: ids
            },
            success: function (data) {
                window.location.href = "/rest/manage/csrelation/toList";
            }
        })

    });
    /*----------------------------------------------编辑表单处理 end------------------------------------*/

</script>
</@page.pageBase>