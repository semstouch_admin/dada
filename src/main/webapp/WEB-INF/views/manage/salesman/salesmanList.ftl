<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="业务员管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary {
        margin-left: 5px;
    }
</style>
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/sign/loadSignData">
    <ul class="panel-content">
        <li>
            <div>
                <div class="control-group span5">
                    <div class="search-controls  controls">
                        <input type="text"  name="activityName" id="title" value="" placeholder="搜索关键字">
                    </div>
                </div>
                <div class="form-actions span2">
                    <button type="submit" class="button  button-primary">
                        搜索
                    </button>
                </div>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<script>
    /*-------------------------------------------------------- 表格数据渲染start-----------------------------------*/
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '5%'},
                {title: '活动名称', dataIndex: 'activityName', elCls: 'center', width: '25%'},
                {title: '业务员名称', dataIndex: 'cityName', elCls: 'center', width: '15%'},
                {title: '门店名称', dataIndex: 'storeName', elCls: 'center', width: '15%'},
                {title: '打卡时间', dataIndex: 'createTime', elCls: 'center', width: '15%'},
                {title: '打卡地点', dataIndex: 'location', elCls: 'center', width: '25%'}
            ];
    var store = new Store({
                url: '${basepath}/rest/manage/sign/loadSignData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status:1
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                width: '100%',//如果表格使用百分比，这个属性一定要设置
                columns: columns,
                idField: 'id',
                store: store,
                // 底部工具栏
                bbar: {
                    // pagingBar:表明包含分页栏
                    pagingBar: true
                }
            });


    grid.render();
    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    /*-------------------------------------------------------- 表格数据渲染end-----------------------------------*/

</script>
</@page.pageBase>