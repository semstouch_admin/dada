<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <style>
        #allChecked{width: 26px;height:46px;margin-right:4px;border:0px solid #ff5c00;line-height: 46px;color: #fff;background-color: #ff5c00; }
        .zero-tip { text-align: center;  margin-top: 10px;  font-size: 14px;  }
                .data-bg {width: 240px!important;  height: 180px!important;  left: 50%;  margin: 10px auto;  display: block;  }
                .img-div {  height: 90%;  display: none;}
    </style>
</head>
<body ontouchstart>
<div class="page">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="weui-tab__panel">
                <div class="area-header page__hd header">
                    <a class="pull-left iconfont icon-zuobian" onclick="javascript:history.go(-1);"></a>
                    <h1 class="title">
                        选择合伙人
                    </h1>
                    <a class="pull-right" data-url="${basepath}/rest/front/activity/toChooseMan">编辑</a>
                </div>
                <div class="choose-content content">
                    <div class="weui-cells weui-cells_checkbox">
                        <!--缺省页面!-->
                        <div class="img-div">
                            <img src="${staticpath}/front/dist/images/data.png" alt="门店" class="data-bg"/>

                            <p class="zero-tip">亲,你还没有合伙人哦！</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="z-index:88888888;float:left;bottom:70px;position:fixed;left:80%;height:46px;" href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="${staticpath}/front/dist/js/activity/chooseManEdit.js"></script>
</body>
</html>
