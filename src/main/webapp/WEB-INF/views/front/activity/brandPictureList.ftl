<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <!--   <link rel="stylesheet" href="css/example.css"> -->
    <style>
        .weui-uploader__file img {
            height: 100%;
            width: 100%;
        }

        .zero-tip {
            text-align: center;
            margin-top: 10px;
            font-size: 14px;
        }

        .data-bg {
            width: 240px !important;
            height: 180px !important;
            left: 50%;
            margin: 30px auto 0;
            display: block;
        }

        .img-div,.kaImg-div {
            height: 90%;
            display: none;
        }

        .weui-gallery {
            z-index: 8000000;
        }
    </style>
</head>
<body ontouchstart>
<div class="page">
    <div class="area-header page__hd header">
        <a class="pull-left iconfont icon-zuobian" onclick="javascript:history.go(-1);"></a>
        <h1 class="title">
            活动照片
        </h1>
    </div>
    <div class="addPicture-content content">
        <div class="weui-cells weui-cells_form  weui-cells_formList">
            <!--缺省页面!-->
            <div class="kaImg-div">
                <img src="${staticpath}/front/dist/images/data.png" alt="门店" class="data-bg"/>

                <p class="zero-tip">亲,对方还没有上传图片信息！</p>
            </div>
        </div>
    </div>
</div>
<div class="weui-gallery" id="gallery">
    <span class="weui-gallery__img" id="galleryImg"></span>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:86%;bottom:0;position:absolute;left:84%;"
     href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script src="${staticpath}/front/dist/js/activity/brandPictureList.js"></script>
<script>

</script>

</body>
</html>
