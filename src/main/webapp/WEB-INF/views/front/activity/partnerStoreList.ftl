<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <style>
        .data-content .weui-cell_access:last-child .weui-cell__ft:after {
            border-color: #999999;
        }

        .zero-tip {
            text-align: center;
            margin-top: 10px;
            font-size: 14px;
        }

        .data-bg {
            width: 180px;
            height: 150px;
            left: 50%;
            margin: 10px auto;
            display: block;
        }

        .img-div {
            height: 90%;
            display: none;
        }
    </style>
</head>
<body ontouchstart>
<div class="page">
    <div class="area-header page__hd header">
        <a class="pull-left iconfont icon-zuobian" onclick="javascript:history.go(-1);"></a>

        <h1 class="title">
            选择门店
        </h1>
        <a class="pull-right addBtn">提交</a>
    </div>
    <div class="data-content content">
        <div class="information-weui-cells weui-cells" data-url="${basepath}/rest/front/activity/toActivityChargeAdd">
            <!--缺省页面!-->
            <div class="img-div">
                <img src="${staticpath}/front/dist/images/data.png" alt="门店" class="data-bg"/>

                <p class="zero-tip">亲,你还没有所属门店哦！</p>
            </div>
        </div>
    </div>
</div>
<!--发布成功提示-->
<div id="toast" style="display: none;">
    <div class="weui-mask_transparent"></div>
    <div class="weui-toast">
        <i class="weui-icon-success-no-circle weui-icon_toast"></i>

        <p class="weui-toast__content">提交成功</p>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:86%;bottom:0;position:fixed;left:84%;"
   href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="${staticpath}/front/dist/js/activity/partnerStoreList.js"></script>
</body>
</html>
