<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/idangerous.swiper.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <style>
        .swiper-wrapper {
            max-height: 250px;
            height: auto;
        }

        .swiper-slide img {
            width: 100%;
            height: 100%;
            max-height: 250px;
        }

        .area-header, .sendOffline-content, .weui-tabbar {
            display: block;
        }

        .area-header {
            width: 100%;
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            height: 46px;
        }

        .sendOffline-content {
            margin-top: 46px;
            margin-bottom: 46px;
        }

        .bar {
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .weui-textarea {
            margin-bottom: 20px;
            font-family: "SimHei";
            font-weight: 500;
            font-size: 14px;
        }

        #img-div, #img-div img {
            width: 100%;
            height: 90px;
        }
    </style>
</head>
<body>
<div class="page">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="area-header page__hd header">
                <a class="pull-left iconfont icon-zuobian"
                   href="${basepath}/rest/front/activity/toActivityList"></a>

                <h1 class="title">
                    活动详情
                </h1>
                <a class="pull-right" id="KAProgress" style="display: none;"
                   data-url="${basepath}/rest/front/activity/toActivityEdit">编辑</a> <!--KA经理在活动详情页面进行中的活动-->
            </div>
            <div class="weui-tab__panel">
                <div class="sendOffline-content content">
                    <!-- 幻灯片Slider -->
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                    <!--活动信息内容-->
                    <div class="weui-cells weui-cells_form">
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">活动名称</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-name" type="text" value="" readonly="readonly"/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">活动预算</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-budge" type="number" value="" readonly="readonly"/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="textarea-header weui-cell__hd"><label class="weui-label">活动要求</label></div>
                            <div class="weui-cell__bd">
                                <textarea class="weui-textarea offline-demand" rows="3" readonly="readonly"></textarea>
                                <!-- <div class="weui-textarea-counter"><span>0</span>/200</div> -->
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">开始日期</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-starTime" type="text" value="" readonly="readonly"/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">结束日期</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-endTime" type="text" value="" readonly="readonly"/>
                            </div>
                        </div>
                        <div class="product-weui-cell">

                        </div>
                    </div>
                </div>
            </div>
            <!--品牌商-->
            <div class="weui-tabbar" id="beginDetail" style="display: none;"
                 data-url="${basepath}/rest/front/activity/toAreaList"><a href="#" class="bar bar-tab see-activity"
                                                                          id="detailBrand"></a></div>
            <!--KA 经理 筹备活动编辑活动时-->
            <div class="weui-tabbar" id="KAEdit" style="display: none;"><a href="#"
                                                                           class="bar bar-tab see-activity">编辑</a></div>
            <!--KA 经理 活动进行中时查看详情-->  <!--城市合伙人 活动进行中时查看活动详情-->
            <div class="weui-tabbar" style="display: none;" id="kaDetail"
                 data-url="${basepath}/rest/front/activity/toAreaList" data-><a href="#"
                                                                                class="bar bar-tab see-activity">查看详情</a>
            </div>
            <!--城市合伙人 活动筹备中已经加入活动后查看活动-->
            <div class="weui-tabbar" style="display: none;" id="partnerDetail"
                 data-url="${basepath}/rest/front/activity/toCityStoreList"
                 data-kaUrl="${basepath}/rest/front/activity/toKaStoreList"><a href="#" class="bar bar-tab see-activity"
                                                                               id="partnerBtn"></a></div>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:80%;bottom:0;position:absolute;left:80%;height:46px;"
   href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="${staticpath}/front/dist/js/lib/idangerous.swiper.min.js"></script>
<script src="${staticpath}/front/dist/js/activity/activityDetail.js"></script>
</body>
</html>
