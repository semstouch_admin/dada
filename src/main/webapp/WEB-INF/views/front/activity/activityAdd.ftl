<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/mend.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <style>
        .area-header, .sendOffline-content, .weui-tabbar {
            display: block;
        }

        .area-header {
            width: 100%;
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            height: 46px;
        }

        .sendOffline-content {
            margin-top: 46px;
            margin-bottom: 46px;
        }

        .bar {
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .weui-textarea {
            margin-bottom: 20px;
            font-family: "SimHei";
            font-weight: 500;
            font-size: 14px;
        }

        .img-div, .img-div img {
            width: 100%;
            height: 90px;
        }
    </style>
</head>
<body ontouchstart>
<div class="page">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="area-header page__hd header">
                <a class="pull-left iconfont icon-zuobian" href="/rest/front/activity/toLoginChoose"
                   style="position: fixed;"></a>

                <h1 class="title">
                    发布活动
                </h1>
            <#--<a class="pull-right">发布</a>-->
            </div>
            <div class="weui-tab__panel">
                <div class="sendOffline-content content">
                    <div class="weui-cells weui-cells_form">
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">活动名称</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-name" type="text" value="" placeholder="请输入名称"/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">活动预算</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-budge" type="text" value="" placeholder="输入金额"
                                       onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="textarea-header weui-cell__hd"><label class="weui-label">活动要求</label></div>
                            <div class="weui-cell__bd">
                                <textarea class="weui-textarea offline-demand" rows="3" placeholder="输入活动要求"></textarea>
                            </div>
                        </div>
                        <div class="product-weui-cell">
                            <div class="weui-cell">
                                <div class="weui-cell__hd"><label class="weui-label">商品名称</label></div>
                                <div class="weui-cell__bd">
                                    <input class="weui-input product-name" type="text" value="" placeholder="输入商品名称"
                                           style="width: 58%;height: 2.2em;float: left;" name="personCost"/>
                                    <img class="delBtn" src="${staticpath}/front/dist/images/reduceBtn.png" width="30"
                                         style="float: left;">
                                    <img class="addBtn" src="${staticpath}/front/dist/images/jiaBtn.png" width="30"
                                         style="float: right">
                                </div>
                            </div>
                            <div class="weui-cell">
                                <div class="weui-cell__bd" style="margin:0;">
                                    <div class="weui-uploader">
                                        <div class="weui-uploader__hd">
                                            <p class="weui-uploader__title">商品条码
                                            </p>
                                        </div>
                                        <div class="weui-uploader__bd">
                                            <ul class="card-content-inner fileCode">
                                                <li class="img-div"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">开始日期</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-starTime" type="date" value=""/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">结束日期</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-endTime" type="date" value=""/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__bd" style="margin:0;">
                                <div class="weui-uploader">
                                    <div class="weui-uploader__hd">
                                        <p class="weui-uploader__title">图片上传<span
                                                style="font-size:14px!important;color: #999999;margin-left: 4px;">(最多上传三张)</span>
                                        </p>
                                    </div>
                                    <div class="weui-uploader__bd">
                                        <ul class="card-content-inner imgList">
                                            <li id="filePicker"><img src="/resource/front/dist/images/a11.jpg"></li>
                                        </ul>
                                    </div>
                                    <input type="hidden" id="productImgs" name="imgs" class="picture"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="weui-tabbar" id="addBtn" data-url="/rest/front/activity/toActivityList">
                <a href="#" class="bar bar-tab see-activity">发布活动</a>
            </div>
        </div>
    </div>
</div>
<!--发布成功提示-->
<div id="toast" style="display: none;">
    <div class="weui-mask_transparent"></div>
    <div class="weui-toast">
        <i class="weui-icon-success-no-circle weui-icon_toast"></i>

        <p class="weui-toast__content">发布成功</p>
    </div>
</div>
<!--发布失败提示-->
<div id="dialogUnSuccessful" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">发布活动需要提前一天</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--信息填写不完整-->
<div id="dialogInformation" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">信息填写不完整</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--结束日期必须比开始日期晚或者相等不完整-->
<div id="dialogTime" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">结束日期要大于开始日期或等于</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--选择时间段不正确-->
<div id="dialogSTime" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">选择日期不正确</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--选择开始时间需要提前一天-->
<div id="dialogStTime" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">开始时间需要提前一天</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--图片上传超过三张做提示-->
<div id="dialogPicture" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">图片最多上传三张</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="z-index:88888888;float:left;bottom:70px;position:fixed;left:80%;height:46px;"
   href="${basepath}/rest/front/activity/toLoginChoose" class="icon-img"><img
        src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type='text/javascript' src='${staticpath}/webUpload/webuploader.min.js' charset='utf-8'></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>

<script type="text/javascript">

    var imgArr = new Array();//声明全局数组
    jQuery(function ($) {//使用jquery$
        //图片上传初始化
        var uploader = WebUploader.create({
            auto: true, // 选完文件后，是否自动上传。
            swf: '${staticpath}/webUploader/Uploader.swf',
            server: '${basepath}/rest/manage/ued/config?action=uploadimage',
            // 选择文件的按钮,可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#filePicker',
            // 只允许选择图片文件。
            fileNumLimit: 3,
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });
        //上传成功后
        uploader.on('uploadSuccess', function (file, response) {
            uploader.removeFile(file);//需要移除掉文件队列缓存，避免第二次上传同样文件，无法上传的问题
            imgArr.push(response.url);//将URL插入全局数组//数组最后面添加一个或多个元素
            //判断是不是超过六张图片，如果是超过则把#filePicker隐藏
            if (jQuery(".imgList").find("li").length <= 3) {
                jQuery(".imgList").prepend("<li><img src='/" + response.url + "'><div class='del-div'><i class='del-img' data-url='" + response.url + "'></i></div></li>");
                jQuery("#productImgs").val(imgArr.join(","));  //数组转字符串赋值给input
                //在url插入上传成功的图片
                //绑定删除按钮事件
                jQuery(".del-div").click(function () {
                    //更新数组
                    var data = $(".del-img").attr("data-url");
                    //splice() 方法向/从数组中添加/删除项目，然后返回被删除的项目。
                    $.each(imgArr, function (index, item) {
                        if (item == data) {
                            imgArr.splice(index, 1);
                        }
                    });
                    //更新input值
                    jQuery("#productImgs").val(imgArr.join(","));
                    //移除图片显示
                    $(this).parent().remove();
                });
            } else {
                jQuery("#dialogPicture").css("display", "block");
                jQuery(".weui-dialog__btn").click(function () {
                    jQuery("#dialogPicture").css("display", "none");
                });
                setTimeout(function () {
                    jQuery("#filePicker").prop();
                }, 1000);
            }

        });
    });


    addPicture();
    $(".addBtn").click(function () {
        $(".product-weui-cell:last").after('<div class="product-weui-cell ab"><div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">商品名称</label></div><div class="weui-cell__bd"><input class="weui-input product-name" type="number" value="" placeholder="输入商品条码" name="" /></div></div><div class="weui-cell"> <div class="weui-cell__bd" style="margin:0;"> <div class="weui-uploader"> <div class="weui-uploader__hd"> <p class="weui-uploader__title">商品条码 </p> </div><div class="weui-uploader__bd"><ul class="card-content-inner fileCode"> <li class="img-div"></li> </ul> </div> </div> </div> </div></div>');
        addPicture();
    });
    $(".delBtn").click(function () {
        $(".ab:last").remove();
    });
    var headImg = '';
    function addPicture() {
        var uploader = WebUploader.create({
            auto: true,
            swf: '${staticpath}/webUploader/Uploader.swf',
            server: '${basepath}/rest/manage/ued/config?action=uploadimage',
            pick: '.fileCode',
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
            }
        });

        uploader.on('uploadSuccess', function (file, response) {
            var uploaderId = '#rt_' + file.source.ruid;
            uploader.removeFile(file);
            headImg = response.url;
            var imgHtml = '<img src="/' + headImg + '">';
            $(uploaderId).prev().find(".img-div").empty();
            $(uploaderId).prev().find(".img-div").append(imgHtml);
        });
    }

    $(".weui-input").focus(function () {
        $(".icon-img").hide();
    });
    $(".weui-input").blur(function () {
        $(".icon-img").show();
    });
    $(".weui-textarea").focus(function () {
        $(".icon-img").hide();
    });
    $(".weui-textarea").blur(function () {
        $(".icon-img").show();
    });
</script>
<script src="${staticpath}/front/dist/js/activity/activityAdd.js"></script>
</body>
</html>
