<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/mend.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <!--   <link rel="stylesheet" href="css/example.css"> -->
</head>
<body ontouchstart>
<div class="page">
    <div class="area-header page__hd header">
        <a class="pull-left iconfont icon-zuobian" onclick="javascript:history.go(-1);"></a>

        <h1 class="title">
            活动照片
        </h1>
        <a class="pull-right" id="saveBtn" data-pictureUrl="${basepath}/rest/front/activity/toPictureList">保存</a>
    </div>
    <div class="addPicture-content content">
        <div class="weui-cells weui-cells_form">
            <div class="weui-cell">
                <div class="weui-cell__bd">
                    <div class="weui-uploader">
                        <div class="weui-uploader__hd">
                        <#--<p class="time-p weui-uploader__title">2017-08-29 12:12</p>-->
                            <!-- <div class="weui-uploader__info">0/2</div> -->
                        </div>
                        <div class="weui-cell__bd" style="margin:0;">
                            <div class="weui-uploader">
                                <div class="weui-uploader__bd">
                                    <ul class="card-content-inner imgList">
                                        <li id="filePicker"><img src="/resource/front/dist/images/a11.jpg"></li>
                                    </ul>
                                </div>
                                <input type="hidden" id="productImgs" name="imgs" class="picture"/>
                            </div>
                        </div>
                        <div class="weui-uploader__hd">
                            <p class="weui-uploader__title" id="address">正在获取位置</p>
                            <!-- <div class="weui-uploader__info">0/2</div> -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--图片上传超过三张做提示-->
<div id="dialogPicture" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">图片最多上传三张</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:80%;bottom:0;position:absolute;left:80%;" href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type='text/javascript' src='${staticpath}/webUpload/webuploader.min.js' charset='utf-8'></script>
<#--引入微信js文件-->
<script type='text/javascript' src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js" charset="utf-8"></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script src="${staticpath}/front/dist/js/activity/activityAdd.js"></script>
<script type="text/javascript">
    var imgArr = new Array();//声明全局数组
    jQuery(function ($) {//使用jquery$
        //图片上传初始化
        var uploader = WebUploader.create({
            auto: true, // 选完文件后，是否自动上传。
            swf: '${staticpath}/webUploader/Uploader.swf',
            server: '${basepath}/rest/manage/ued/config?action=uploadimage',
            // 选择文件的按钮,可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#filePicker',
            // 只允许选择图片文件。
            fileNumLimit: 3,
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });
        //上传成功后
        uploader.on('uploadSuccess', function (file, response) {
            uploader.removeFile(file);//需要移除掉文件队列缓存，避免第二次上传同样文件，无法上传的问题
            imgArr.push(response.url);//将URL插入全局数组//数组最后面添加一个或多个元素
            //判断是不是超过三张图片，如果是超过则把#filePicker隐藏
            if (jQuery(".imgList").find("li").length <= 3) {
                jQuery(".imgList").prepend("<li><img src='/" + response.url + "'><div class='del-div'><i class='del-img' data-url='" + response.url + "'></i></div></li>");
                jQuery("#productImgs").val(imgArr.join(","));  //数组转字符串赋值给input
                //在url插入上传成功的图片
                //绑定删除按钮事件
                jQuery(".del-div").click(function () {
                    //更新数组
                    var data = $(".del-img").attr("data-url");
                    //splice() 方法向/从数组中添加/删除项目，然后返回被删除的项目。
                    $.each(imgArr, function (index, item) {
                        if (item == data) {
                            imgArr.splice(index, 1);
                        }
                    });
                    //更新input值
                    jQuery("#productImgs").val(imgArr.join(","));
                    //移除图片显示
                    $(this).parent().remove();
                });
            } else {
                jQuery("#dialogPicture").css("display", "block");
                jQuery(".weui-dialog__btn").click(function () {
                    jQuery("#dialogPicture").css("display", "none");
                });
                setTimeout(function () {
                    jQuery("#filePicker").prop();
                }, 1000);
            }

        });
    });
    function getQueryString(name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }
    var activityID = getQueryString("id");
    var cityID = getQueryString("cityId");
    var storeID = getQueryString("storeId");
    var insertStatus = getQueryString("insertStatus");
    //点击保存，上传图片与地址
    $("#saveBtn").click(function () {
        if ($(".picture").val() == '' || $("#address").text() == '') {
            weui.alert("信息填写不完整");
        } else {
            $.ajax({
                type: 'post',
                url: '/rest/front/activitypicture/insertActivitypicture',
                dataType: 'json',
                data: {
                    activityID: activityID,
                    cityID: cityID,
                    storeID: storeID,
                    picture: $(".picture").val(),
                    location: $("#address").text()
                },
                success: function (data) {
                    weui.alert("成功");
                    setTimeout(function () {
                        var id=activityID;
                        var city=cityID;
                        var url = $(".pull-right").attr('data-pictureUrl')+ "?id=" + id + "&city=" + city + "&storeID=" + storeID + "&insertStatus=" + insertStatus;
                        window.location.href = url;
                    }, 1000);
                }
            })
        }

    });
</script>
</body>
</html>
