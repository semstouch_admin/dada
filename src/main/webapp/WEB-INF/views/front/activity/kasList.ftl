<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_yb5s5cminzdgf1or.css">
</head>
<body ontouchstart>
<div class="page">
    <div class="area-header page__hd header">
        <a class="pull-left iconfont icon-zuobian" data-kaUrl="${basepath}/rest/front/activity/toCityManLis" onclick="javascript:history.go(-1);"></a>
        <h1 class="title">
        </h1>
    </div>
    <div class="data-content content">
        <div class="data-title weui-cells">
            <div class="weui-cell">
                <div class="weui-cell__bd">
                    <p class="totalCost"></p>
                </div>
                <div class="weui-cell__ft totalGoods"></div>
            </div>
        </div>
        <div class="kaStoreList" data-pictureUrl="${basepath}/rest/front/activity/toKapList" data-chargeUrl="${basepath}/rest/front/activity/toActivityCharge" data-signUrl="${basepath}/rest/front/activity/toSignList">
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:86%;bottom:0;position:absolute;left:84%;" href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="${staticpath}/front/dist/js/activity/kasList.js"></script>
</body>
</html>
