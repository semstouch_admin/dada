<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <style>
        .sendOffline-content .weui-label {
            width: 105px !important;
        }

        .sendOffline-content .weui-cells {
            margin-top: 46px !important;
        }

    </style>
</head>
<body ontouchstart>
<div class="page">
    <div class="area-header page__hd header">
        <a class="pull-left iconfont icon-zuobian" onclick="javascript:history.go(-1);"></a>

        <h1 class="title">
            活动信息
        </h1>
        <!-- <a  class="pull-right">编辑</a>KA经理在活动详情页面多个编辑按钮-->
    </div>
    <div class="sendOffline-content content">
        <!--活动信息内容-->
        <div class="weui-cells weui-cells_form">
            <div class="weui-cell">
                <div class="weui-cell__hd"><label class="weui-label">场地费用</label></div>
                <div class="weui-cell__bd">
                    <input class="weui-input fieldBtn" type="number" value=""/>
                </div>
            </div>
            <div class="weui-cell">
                <div class="weui-cell__hd"><label class="weui-label">堆头费用</label></div>
                <div class="weui-cell__bd">
                    <input class="weui-input stackBtn" type="number" value=""/>
                </div>
            </div>
            <div class="weui-cell">
                <div class="weui-cell__hd"><label class="weui-label">补货数量</label></div>
                <div class="weui-cell__bd">
                    <input class="weui-input goodsBtn" type="number" value=""/>
                </div>
            </div>
            <div class="personList">
            </div>
        </div>
        <div class="weui-tabbar" id="beginDetail"><a href="#" class="bar bar-tab see-activity" id="editBtn">保存</a></div>
    </div>
</div>
<!--!--信息填写不完整-->
<div id="dialogInformation" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">信息填写不完整</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:80%;bottom:0;position:absolute;left:80%;"
   href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script src="${staticpath}/front/dist/js/activity/activityCharge.js"></script>
</body>
</html>
