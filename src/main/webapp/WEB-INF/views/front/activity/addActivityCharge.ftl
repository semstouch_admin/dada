<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <style type="text/css">
        .weui-btn + .weui-btn {
            margin-top: 0;
        }

        .weui-btn_mini {
            padding: 0 0.8em;
        }

        .sendOffline-content .weui-label {
            width: 105px !important;
        }

        .sendOffline-content {
            margin-top: 46px;

        }
    </style>

</head>
<body ontouchstart>
<div class="page">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="weui-tab__panel">
                <div class="area-header page__hd header">
                    <a class="pull-left iconfont icon-zuobian" onclick="javascript:history.go(-1);"></a>

                    <h1 class="title">

                    </h1>
                    <!-- <a  class="pull-right">编辑</a>KA经理在活动详情页面多个编辑按钮-->
                </div>
                <div class="sendOffline-content content">
                    <!--活动信息内容-->
                    <div class="weui-cells weui-cells_form">
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">场地费用</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input fieldBtn" type="text" value="" placeholder="输入场地费用"
                                       onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">堆头费用</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input duitouBtn" type="text" value="" placeholder="输入堆头费用"
                                       onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">补货数量</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input goodsBtn" type="text" value="" placeholder="输入补货数量"
                                       onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                            </div>
                        </div>
                        <div class="personList2"></div>
                        <#--<div class="weui-cell personnelList" style="display: none;">-->
                        <div class="weui-cell personnelList">
                            <div class="weui-cell__hd"><label class="weui-label">促销员</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input personnelBtn" type="text" value="" placeholder="输入工资"
                                       style="width: 58%;height: 2.2em;float: left;" name="personCost"
                                       onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                                <img class="delBtn" src="${staticpath}/front/dist/images/reduceBtn.png" width="30"
                                     style="float: left;">
                                <img class="addBtn" src="${staticpath}/front/dist/images/jiaBtn.png" width="30"
                                     style="float: right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="weui-tabbar saveBtn"><a href="#" class="bar bar-tab see-activity">保存</a>
            </div>
        </div>
    </div>
</div>
<!--!--信息填写不完整-->
<div id="dialogInformation" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">信息填写不完整</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="z-index:88888888;float:left;bottom:70px;position:fixed;left:80%;height:46px;" href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script type="text/javascript">
    $(".addBtn").click(function () {
        $(".weui-cells_form").append('<div class="weui-cell sales-weui-cell ab"><div class="weui-cell__hd"><label class="weui-label">促销员</label></div><div class="weui-cell__bd"><input class="weui-input personnelBtn" type="number" value="" placeholder="输入工资" name="personCost" onkeyup="clearNoNum(this)"/></div></div>');
    });

    $(".delBtn").click(function () {
        $(".ab:last").remove();
    });
    //只能输入数字
    function clearNoNum(obj) {
        obj.value = obj.value.replace(/[^\d.]/g, "");  //清除“数字”和“.”以外的字符
        if (obj.value.indexOf(".") < 0 && obj.value != "") {//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            obj.value = parseFloat(obj.value);
        }
    }
    $(".weui-input").focus(function(){
                   $(".icon-img").hide();
              });
          $(".weui-input").blur(function(){
              $(".icon-img").show();
                 });

</script>
<script src="${staticpath}/front/dist/js/activity/addActivityCharge.js"></script>
</body>
</html>
