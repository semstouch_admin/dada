<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <!--   <link rel="stylesheet" href="css/example.css"> -->
</head>
<body ontouchstart>
<div class="page">
    <div class="area-header page__hd header">
        <a class="pull-left iconfont icon-zuobian" onclick="javascript:history.go(-1);"></a>

        <h1 class="title">
            店铺列表
        </h1>
    </div>
    <div class="area-content content">
        <div class="area-list weui-cells" data-url="${basepath}/rest/front/activity/toBrandPictureList">
            <#--<a class="weui-cell weui-cell_access" href="javascript:;">-->
                <#--<div class="weui-cell__bd">-->
                    <#--<p>湖里万达沃尔玛</p>-->
                <#--</div>-->
                <div class="weui-cell__ft">
                </div>
            <#--</a>-->
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:86%;bottom:0;position:absolute;left:84%;" href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="${staticpath}/front/dist/js/activity/storeList.js"></script>
</body>
</html>
