<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/mend.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <style>
        .area-header, .sendOffline-content, .weui-tabbar {
            display: block;
        }

        .area-header {
            width: 100%;
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            height: 46px;
        }

        .sendOffline-content {
            margin-top: 46px;
            margin-bottom: 46px;
        }

        .bar {
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .weui-textarea {
            margin-bottom: 20px;
            font-family: "SimHei";
            font-weight: 500;
            font-size: 14px;
        }

        .img-div, .img-div img {
            width: 100%;
            height: 90px;
        }

        #allChecked {
            width: 26px;
            height: 46px;
            margin-right: 4px;
            border: 0px solid #ff5c00;
            line-height: 46px;
            color: #fff;
            background-color: #ff5c00;
        }

        .zero-tip {
            text-align: center;
            margin-top: 10px;
            font-size: 14px;
        }

        .data-bg {
            width: 240px !important;
            height: 180px !important;
            left: 50%;
            margin: 10px auto;
            display: block;
        }

        .img1-div {
            height: 90%;
            display: none;
        }
    </style>
</head>
<body ontouchstart>
<div class="page" id="pageOne">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="area-header page__hd header">
                <a class="pull-left iconfont icon-zuobian"
                   data-url="${basepath}/rest/front/activity/toActivityDetail" id="pull-left-one"></a>

                <h1 class="title">
                    编辑活动
                </h1>
                <!-- <a  class="pull-right">编辑</a>KA经理在活动详情页面多个编辑按钮-->
            </div>
            <div class="weui-tab__panel">
                <div class="sendOffline-content content">
                    <!--活动信息内容-->
                    <div class="weui-cells weui-cells_form">
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">活动名称</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-name" type="text" value=""/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">活动预算</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-budge" type="tel" value=""
                                       onkeyup="value=value.replace(/[^\d]/g,'')"
                                       onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="textarea-header weui-cell__hd"><label class="weui-label">活动要求</label></div>
                            <div class="weui-cell__bd">
                                <textarea class="weui-textarea offline-demand" value="" rows="3"></textarea>
                            <#--<!-- <div class="weui-textarea-counter"><span>0</span>/200</div> &ndash;&gt;-->
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">开始时间</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-starTime" type="date" value=""/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__hd"><label class="weui-label">结束时间</label></div>
                            <div class="weui-cell__bd">
                                <input class="weui-input offline-endTime" type="date" value=""/>
                            </div>
                        </div>
                        <div class="weui-cell">
                            <div class="weui-cell__bd" style="margin:0;">
                                <div class="weui-uploader">
                                    <div class="weui-uploader__hd">
                                        <p class="weui-uploader__title">图片上传<span
                                                style="font-size:14px!important;color: #999999;margin-left: 4px;">(最多上传三张)</span>
                                        </p>
                                    </div>
                                    <div class="weui-uploader__bd">
                                        <ul class="card-content-inner imgList">
                                            <li id="filePicker"><img src="/resource/front/dist/images/a11.jpg"></li>
                                        </ul>
                                    </div>
                                    <input type="hidden" id="productImgs" name="imgs" class="picture"/>
                                </div>
                            </div>
                        </div>
                        <div class="productList">
                        </div>
                    </div>
                    <div class="area-list weui-cells">
                        <a class="weui-cell weui-cell_access" data-url="${basepath}/rest/front/activity/toChooseMan"
                           data-urlEdit="${basepath}/rest/front/activity/toChooseManEdit">
                            <div class="weui-cell__bd" style="margin-left:0;">
                                <p>选择合伙人</p>
                            </div>
                            <div class="weui-cell__ft addPartner">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!--KA 经理编辑后重新发布品牌商的活动-->
            <div class="weui-tabbar" id="addBtn" data-url="${basepath}/rest/front/activity/toActivityList"><a href="#"
                                                                                                              class="bar bar-tab see-activity">发布活动</a>
            </div>
        </div>
    </div>
</div>
<div class="page" id="pageTwo" style="display: none">
    <div class="page__bd">
        <div class="weui-tab">
            <div class="weui-tab__panel">
                <div class="area-header page__hd header">
                    <h1 class="title">
                        选择合伙人
                    </h1>
                    <a class="pull-right allChecked">全选</a>

                </div>
                <div class="choose-content content" style="margin-top:46px;">
                    <div class="weui-cells weui-cells_checkbox" id="add-weui-cells_checkbox">
                        <!--缺省页面!-->
                        <div class="img1-div">
                            <img src="${staticpath}/front/dist/images/data.png" alt="门店" class="data-bg"/>

                            <p class="zero-tip">亲,你还没有合伙人哦！</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="weui-tabbar" id="saveBtn" data-url="${basepath}/rest/front/activity/toActivityEdit"><a href="#"
                                                                                                               class="bar bar-tab see-activity">保存</a>
            </div>
        </div>
    </div>
</div>
<div class="page" id="pageThree" style="display:none;">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="weui-tab__panel">
                <div class="area-header page__hd header">
                    <a class="pull-left iconfont icon-zuobian" id="edit-pull-left"></a>

                    <h1 class="title">
                        选择合伙人
                    </h1>
                    <a class="pull-right" href="#pageTwo" id="editBtn">编辑</a>
                </div>
                <div class="choose-content content" style="margin-top:46px;">
                    <div class="weui-cells weui-cells_checkbox" id="addThree-weui-cells_checkbox">
                        <!--缺省页面!-->
                        <div class="img-div">
                            <img src="${staticpath}/front/dist/images/data.png" alt="门店" class="data-bg"/>

                            <p class="zero-tip">亲,你还没有合伙人哦！</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--发布成功提示-->
<div id="toast" style="display: none;">
    <div class="weui-mask_transparent"></div>
    <div class="weui-toast">
        <i class="weui-icon-success-no-circle weui-icon_toast"></i>

        <p class="weui-toast__content">发布成功</p>
    </div>
</div>
<!--!--信息填写不完整-->
<div id="dialogInformation" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">信息填写不完整</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--图片上传超过三张做提示-->
<div id="dialogPicture" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">图片最多上传三张</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--结束日期必须比开始日期晚或者相等不完整-->
<div id="dialogTime" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">结束日期要大于开始日期或等于</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--结束日期必须比开始日期晚或者相等不完整-->
<div id="dialogChange" style="display: none;">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__bd">请选择合伙人</div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="z-index:88888888;float:left;bottom:70px;position:fixed;left:80%;height:46px;"
   href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="https://res.wx.qq.com/open/libs/weuijs/1.1.1/weui.min.js"></script>
<script type='text/javascript' src='${staticpath}/webUpload/webuploader.min.js' charset='utf-8'></script>
<script src="http://code.changer.hk/jquery/plugins/jquery.cookie.js"></script>
<script>
    function getQueryString(name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return unescape(r[2]);
        }
        return null;
    }
    /**KA经理活动编辑时先查看页面**/
    var imgArr = new Array();       //声明全局数组
    var id = getQueryString("id");
    var insertStatus = getQueryString("insertStatus");
    var updateStatus = getQueryString("updateStatus");
    var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
</script>

<script type="text/javascript">
    jQuery(function ($) {           //使用jquery$
        //图片上传初始化
        var uploader = WebUploader.create({
            auto: true, // 选完文件后，是否自动上传。
            swf: '${staticpath}/webUploader/Uploader.swf',
            server: '${basepath}/rest/manage/ued/config?action=uploadimage',
            // 选择文件的按钮,可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#filePicker',
            // 只允许选择图片文件。
            fileNumLimit: 3,
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });

        //上传成功后
        uploader.on('uploadSuccess', function (file, response) {
            uploader.removeFile(file);//需要移除掉文件队列缓存，避免第二次上传同样文件，无法上传的问题
            imgArr.push(response.url);//将URL插入全局数组//数组最后面添加一个或多个元素
            //判断是不是超过三张图片，如果是超过则把#filePicker隐藏
            if (imgArr.length <= 3) {
                jQuery(".imgList").prepend("<li><img src='/" + response.url + "'><div class='del-div'><i class='del-img' data-url='" + response.url + "'></i></div></li>");
                jQuery("#productImgs").val(imgArr.join(","));  //数组转字符串赋值给input
                //在url插入上传成功的图片
                //绑定删除按钮事件
                jQuery(".del-div").click(function () {
                    //更新数组
                    var data = $(this).children().attr("data-url");
                    //splice() 方法向/从数组中添加/删除项目，然后返回被删除的项目。
                    $.each(imgArr, function (index, item) {
                        if (item == data) {
                            imgArr.splice(index, 1);
                        }
                    });
                    //更新input值
                    jQuery("#productImgs").val(imgArr.join(","));
                    //移除图片显示
                    $(this).parent().remove();
                });
            } else {
                jQuery("#dialogPicture").css("display", "block");
                jQuery(".weui-dialog__btn").click(function () {
                    jQuery("#dialogPicture").css("display", "none");
                });
                setTimeout(function () {
                    jQuery("#filePicker").prop();
                }, 1000);
            }
        });
    });
    //初始化查询活动编辑页面
    function query() {
        $.ajax({
            type: "GET",
            url: "/rest/front/activity/selectActivityDetail",
            data: {
                id: id//传活动id
            },
            dataType: "json",
            success: function (data) {
                var offline = data.data;
                /**首页的详情页面内容部分**/
                $(".offline-name").val(offline.kaName);
                $(".offline-demand").val(offline.kaDemand);
                $(".offline-starTime").val(offline.kaStarTime);
                $(".offline-endTime").val(offline.kaEndTime);
                $(".offline-budge").val(offline.kaBudge);
                var product = data.data.kaProductName.split(",");
                var imgArr1 = data.data.kaProductCode.split(",");
                var productHtml = "";//声明商品信息列表
                for (var j = 0; j < product.length; j++) {
                    if (j == 0) {//当只有一条信息时，要出现加好与减号按钮，多条信息时，第一条出现加减按钮即可
                        productHtml += '<div class="product-weui-cell"><div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">商品名称</label></div>' +
                                '<div class="weui-cell__bd"><input class="weui-input personnelBtn product-name" type="text" value="' + product[j] + '" style="width: 58%;height: 2.2em;float: left;"> <img class="delBtn" src="${staticpath}/front/dist/images/reduceBtn.png" width="30" style="float: left;"><img class="addBtn" src="${staticpath}/front/dist/images/jiaBtn.png" width="30" style="float: right">' +
                                '</div></div>';
                        productHtml += ' <div class="weui-cell"><div class="weui-cell__bd" style="margin:0;"> <div class="weui-uploader"> <div class="weui-uploader__hd"> <p class="weui-uploader__title">商品条码 </p> </div> <div class="weui-uploader__bd"> <ul class="card-content-inner fileCode"> <li class="img-div"><img src="/' + imgArr1[j] + '"></li> </ul> </div> </div> </div> </div></div>'
                    } else {
                        productHtml += '<div class="product-weui-cell ab"><div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">商品名称</label></div>' +
                                '<div class="weui-cell__bd"><input class="weui-input product-name" type="text" value="' + product[j] + '">' +
                                '</div></div>';
                        productHtml += ' <div class="weui-cell"><div class="weui-cell__bd" style="margin:0;"> <div class="weui-uploader"> <div class="weui-uploader__hd"> <p class="weui-uploader__title">商品条码 </p> </div> <div class="weui-uploader__bd"> <ul class="card-content-inner fileCode"> <li class="img-div"><img src="/' + imgArr1[j] + '"></li> </ul> </div> </div> </div> </div></div>'
                    }
                }
                $(".productList").html(productHtml);
                addPicture();
                $(".addBtn").click(function () {
                    $(".product-weui-cell:last").after('<div class="product-weui-cell ab"><div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">商品名称</label></div><div class="weui-cell__bd"><input class="weui-input product-name" type="number" value="" placeholder="输入商品名称" name="personCost" /></div></div><div class="weui-cell"> <div class="weui-cell__bd" style="margin:0;"> <div class="weui-uploader"> <div class="weui-uploader__hd"> <p class="weui-uploader__title">商品条码 </p> </div><div class="weui-uploader__bd"><ul class="card-content-inner fileCode"> <li class="img-div"></li> </ul> </div> </div> </div> </div></div>');
                    addPicture();
                });
                $(".delBtn").click(function () {
                    $(".ab:last").remove();
                });
                var headImg = '';

                function addPicture() {//添加商品信息条形码时调用添加图片接口
                    var uploader = WebUploader.create({
                        auto: true,
                        swf: '${staticpath}/webUploader/Uploader.swf',
                        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
                        pick: '.fileCode',
                        accept: {
                            title: 'Images',
                            extensions: 'gif,jpg,jpeg,bmp,png',
                            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
                        }
                    });

                    uploader.on('uploadSuccess', function (file, response) {
                        var uploaderId = '#rt_' + file.source.ruid;
                        uploader.removeFile(file);
                        headImg = "/" + response.url;
                        var imgHtml = '<img src="' + headImg + '">';//声明条形码图片放置位置
                        $(uploaderId).prev().find(".img-div").empty();//在更换图片时，先清空已经选择的图片
                        $(uploaderId).prev().find(".img-div").append(imgHtml);//找到每个li,并放置图片
                    });
                }

                var pictureList = offline.kaPicture;
                imgArr = pictureList.split(',');
                for (var i = 0; i < imgArr.length; i++) {
                    $(".imgList").prepend("<li><img src='/" + imgArr[i] + "'><div class='del-div'><i class='del-img' data-url='" + imgArr[i] + "'></i></div></li>");
                }
                jQuery("#productImgs").val(pictureList);
                //绑定删除按钮事件
                jQuery(".del-div").click(function () {
                    //更新数组
                    var data = $(this).children().attr("data-url");
                    //splice() 方法向/从数组中添加/删除项目，然后返回被删除的项目。
                    $.each(imgArr, function (index, item) {
                        if (item == data) {
                            imgArr.splice(index, 1);
                        }
                    });
                    //更新input值
                    jQuery("#productImgs").val(imgArr.join(","));
                    //移除图片显示
                    $(this).parent().remove();
                });
                if (offline.cityStatus == 0 || offline.cityStatus == 1) {
                    $(".weui-cell_access").attr("href", "#pageThree");
                    $(".weui-cell_access").click(function () {
                        $("#pageThree").show();
                        $("#pageOne").hide();
                    });
                } else {
                    $(".weui-cell_access").attr("href", "#pageTwo");
                    $(".weui-cell_access").click(function () {
                        $("#pageTwo").show();
                        $("#pageOne").hide();
                    });

                }
                if (insertStatus == "2" && sessionStorage.roleType == 3) {
                    $(".offline-starTime").attr('readonly', 'readonly');
                }
            }
        });
    }
    query();
    //点击返回跳转页面
    $("#pull-left-one").click(function () {
        var url = $(this).attr('data-url');
        url = url + "?id=" + id + "&status=" + insertStatus + "&updateStatus=" + updateStatus;
        window.location.href = url;
    });
    $(".weui-input").focus(function () {
        $(".icon-img").hide();
    });
    $(".weui-input").blur(function () {
        $(".icon-img").show();
    });
    $(".weui-textarea").focus(function () {
        $(".icon-img").hide();
    });
    $(".weui-textarea").blur(function () {
        $(".icon-img").show();
    });
</script>
<script>
    var selections = '';
    var cityIDs = new Array();
    var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
    //未选择时，查看全部城市合伙人列表以及在本页面复选框选中城市合伙人
    $.ajax({
        type: "GET",
        url: "/rest/front/kcrelation/selectCityList",
        dataType: "json",
        success: function (data) {
            var manList = data.data;
            if (manList == null || manList == '') {//判断是否有值，没有值的情况下，出现缺省页面
                $('.img-div').show();
            } else {
                var manHtml = "";
                var selectID = '';
                $.each(manList, function (i, o) {
                    i++;
                    manHtml += '<label class="weui-cell weui-check__label" for="' + i + '" ><div class="weui-cell__hd"><input type="checkbox" class="weui-check checkboxTwo" name="choose" id="' + i + '" data-id="' + o.cityID + '"  data-text="' + o.cityName + '"/> <i class="weui-icon-checked"></i> </div>' +
                            '<div class="weui-cell__bd"> <p class="cityName" >' + o.cityName + '</p> </div> </label>';
                    selectID += o.cityID + " ";//遍历选中每个cityID的值
                });

                $("#add-weui-cells_checkbox").html(manHtml);
            }
            //点击全选时塞值
            $(".allChecked").click(function () {
                selections = selectID.replace(/(\s*$)/g, "");//去除字符串空格右边
                cityIDs = selections.split(" ");
            });
            //checkbox 全选/取消全选
            var isCheckAll = false;
            $(".allChecked").click(function () {
                if (isCheckAll) {
                    $("input[type='checkbox']").each(function () {
                        this.checked = false;
                    });
                    isCheckAll = false;
                } else {
                    $("input[type='checkbox']").each(function () {
                        this.checked = true;
                    });
                    isCheckAll = true;
                }
            });
            var texts = [];
            //多选input的值
            $('.weui-check').change(function () {
                texts = [];
                $(".weui-check:checked").each(function () {
                    texts.push($(this).closest("input").attr("data-id"));
                });
                cityIDs = texts;
            });

        }
    });
    $("#edit-pull-left").click(function () {
        $("#pageThree").hide();
        $("#pageOne").show();
    });
    //编辑状态
    //已经选择时，查看被选择城市合伙人列表以及去编辑城市合伙人
    $.ajax({
        type: "GET",
        url: "/rest/front/activityjoin/selectCityChoose",
        dataType: "json",
        data: {
            activityID: id,
            kaID: obj.id
        },
        success: function (data) {
            var manList = data.data;
            if (manList == null || manList == '') {
                $('.img-div').show();
            } else {
                var manHtml = "";
                $.each(manList, function (i, o) {
                    i++;
                    manHtml += '<label class="weui-cell weui-check__label" for="' + i + '" ><div class="weui-cell__hd"><input type="checkbox" class="weui-check" name="choose" id="' + i + '" data-id="' + o.cityID + '" checked/> <i class="weui-icon-checked"></i> </div>' +
                            '<div class="weui-cell__bd"> <p>' + o.cityName + '</p> </div> </label>';
                });
            }
            $("#addThree-weui-cells_checkbox").html(manHtml);
        }
    });
    $("#editBtn").click(function () {
        $("#pageTwo").show();
        $("#pageThree").hide();
        $("#pageOne").hide();
    });
    $("#saveBtn").click(function () {
        var chk_value = [];
        var text = [];
        $('.checkboxTwo:checked').each(function () {
            chk_value.push($(this).attr('data-id'));
            text.push($(this).attr('data-text'));
        });

        cityIDs = chk_value;
        var areaText = '';
        for (var i = 0; i < text.length; i++) {
            areaText = areaText + '<label class="weui-cell weui-check__label" for="' + i + '" ><div class="weui-cell__hd"><input type="checkbox" class="weui-check" name="choose" id="' + i + '" data-id="' + chk_value[i] + '" checked/> <i class="weui-icon-checked"></i> </div>' +
                    '<div class="weui-cell__bd"> <p>' + text[i] + '</p> </div> </label>';
        }
        $("#addThree-weui-cells_checkbox").empty();
        $("#addThree-weui-cells_checkbox").html(areaText);
        if (cityIDs == '' || cityIDs == null) {
            $("#pageTwo").hide();
            $("#pageOne").show();
        } else {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/rest/front/activityjoin/updateCityList',
                data: {
                    cityIDs: cityIDs,
                    activityID: id
                },
                success: function (data) {
                    $("#pageTwo").hide();
                    $("#pageOne").show();
                }
            })
        }
    });
</script>
<script>
    $("#addBtn").click(function () {
        var kaName = $(".offline-name").val(),
                kaDemand = $(".offline-demand").val(),
                kaStarTime = $(".offline-starTime").val(),
                kaEndTime = $(".offline-endTime").val(),
                kaBudge = $(".offline-budge").val(),
                kaPicture = $('#productImgs').val();
        var kaProductName = '';
        var kaProductCode = '';
        $(".product-name").each(function () {
            kaProductName += $(this).val() + ",";
        });
        kaProductName = kaProductName.substr(0, kaProductName.length - 1);
        $(".img-div").children('img').each(function () {
            kaProductCode += ($(this).attr('src').substring(1)) + ",";
        });
        kaProductCode = kaProductCode.substr(0, kaProductCode.length - 1);
        if (kaName == '' || kaDemand == '' || kaStarTime == '' || kaEndTime == '' || kaBudge == '' || kaPicture == '' || kaProductName == '' || kaProductCode == '') {
            //信息填写不完整提示
            $("#dialogInformation").css("display", "block");
            $(".weui-dialog__btn").click(function () {
                $("#dialogInformation").css("display", "none");
            })
        } else if (cityIDs == '') {
            //没有选择合伙人提示
            $("#dialogChange").css("display", "block");
            $(".weui-dialog__btn").click(function () {
                $("#dialogChange").css("display", "none");
            })
        } else if (kaStarTime > kaEndTime) {
            //结束日期要大于开始日期
            $("#dialogTime").css("display", "block");
            $(".weui-dialog__btn").click(function () {
                $("#dialogTime").css("display", "none");
            })
        } else {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/rest/front/activity/updateActivity',
                data: {
                    id: id,
                    kaName: kaName,
                    kaDemand: kaDemand,
                    kaStarTime: kaStarTime,
                    kaEndTime: kaEndTime,
                    kaBudge: kaBudge,
                    kaPicture: kaPicture,
                    cityIDs: cityIDs,
                    kaProductName: kaProductName,
                    kaProductCode: kaProductCode
                },
                success: function (data) {
                    if (data.success) {
                        var $toast = $('#toast');
                        $toast.fadeIn();
                        setTimeout(function () {
                            $toast.fadeOut();
                            window.location.href = $('#addBtn').attr('data-url');
                        }, 1000);
                    } else {
                        weui.alert(data.message);
                    }
                }
            });
        }
    });
</script>

</body>
</html>
