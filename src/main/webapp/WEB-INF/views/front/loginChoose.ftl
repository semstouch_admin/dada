<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1,user-scalable=0" name="viewport">
    <title>
        嗒达通
    </title>
    <link href="${staticpath}/front/dist/css/weui.css" rel="stylesheet" type="text/css">
    <link href="${staticpath}/front/dist/css/base.css" rel="stylesheet" type="text/css">
    <link href="${staticpath}/front/dist/css/module.css" rel="stylesheet" type="text/css">
</head>
<style>
    .area-header {
        background-color: #1b1b20 !important;
        background-image: none !important;
        top: 0;
    }

    .page {
        background-color: #fff !important;
    }
</style>
<body ontouchstart="">
<div class="page">
    <div class="workbench">
        <div class="box-title">
            <h1>嗒达通工作台</h1>

            <h2>选择使用对应功能</h2>
        </div>
    </div>

    <div class="work-content">
        <div style="height:10px;width: 100%;margin-bottom: 10px;"></div>
        <div style="background-color: #fff;">
            <div class="box-item flex-box">
                <div class="left">
                    <h3>活动管理</h3>

                    <p>查看发布活动</p>
                </div>
                <div class="verline"></div>
                <div class="right cell-main">
                    <a href="/rest/front/activity/toActivityAdd" class="btn" id="addBtn" style="display: none;">添加活动</a>
                    <a href="/rest/front/activity/toActivityList" class="btn" id="detailBtn">查看活动</a>
                </div>
            </div>
        </div>
        <img src="${staticpath}/front/dist/images/shadow.png" alt="" class="pic">

        <div style="background-color: #fff;">
            <div class="box-item flex-box">
                <div class="left">
                    <h3>报表管理</h3>

                    <p>随时随地看报表</p>
                </div>
                <div class="verline"></div>
                <div class="right cell-main">
                    <a href="/rest/front/sell/toAreaList" class="btn" id="checkBtn">查看报表</a>
                </div>
            </div>
        </div>
        <img src="${staticpath}/front/dist/images/shadow.png" alt="" class="pic" style="display: none;" id="imgBtn">

        <div style="background-color: #fff;">
            <div class="box-item flex-box" id="signBtn" style="display: none;">
                <div class="left">
                    <h3>签到打卡</h3>

                    <p>按时打卡不落下</p>
                </div>
                <div class="verline"></div>
                <div class="right cell-main">
                    <a href="/rest/front/sign/toSignActivityList" class="btn">去打卡</a>
                </div>
            </div>
        </div>
        <img src="${staticpath}/front/dist/images/shadow.png" alt="" class="pic">

        <div style="background-color: #fff;">
            <div class="box-item flex-box">
                <div class="left">
                    <h3>工作路线</h3>

                    <p>正在开发中</p>
                </div>
                <div class="verline"></div>
                <div class="right cell-main">
                    <a href="/rest/front/activity/toWorkRoute" class="btn">我的路线</a>
                </div>
            </div>
        </div>
        <img src="${staticpath}/front/dist/images/shadow.png" alt="" class="pic">

        <div style="background-color: #fff;">
            <div class="box-item flex-box">
                <div class="left">
                    <h3>活动费用</h3>

                    <p>正在开发中</p>
                </div>
                <div class="verline"></div>
                <div class="right cell-main">
                    <a href="/rest/front/activity/toActivityCost" class="btn">我的费用</a>
                </div>
            </div>
        </div>
        <img src="${staticpath}/front/dist/images/shadow.png" alt="" class="pic">
    </div>
</div>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="https://res.wx.qq.com/open/libs/weuijs/1.1.1/weui.min.js"></script>
<script src="${staticpath}/front/dist/js/loginChoose.js"></script>
</body>
</html>
