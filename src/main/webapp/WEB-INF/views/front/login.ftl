<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <style type="text/css">
        .weui-input {  width: 80%;  }
        .weui-icon_toast{margin:0;}
        .weui-toast{ z-index: 5000; min-height: 6.6em;}
        .weui-cells_form{ z-index: 100;}
        .weui-input{font-size:14px;}
        .weui-cell{padding:15px 15px;}
    </style>
</head>
<body ontouchstart>
<div class="page">
    <div class="dadaLogin-content content">
        <div class="logo">
            <div class="logo-main">
                <img src="${staticpath}/front/dist/images/logo1.png" alt="头像" width="75">
                <img src="${staticpath}/front/dist/images/logo2.png" alt="标题" width="145">
            </div>
        </div>
        <!-- 输入手机号与密码-->
        <form id="signupForm">
            <div class="weui-cells weui-cells_form">
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label class="weui-label"><img
                            src="${staticpath}/front/dist/images/account.png" alt="手机" width="14"></label></div>
                    <div class="weui-cell__bd" class="phone-weui-cell__bd">
                        <input class="weui-input" type="text" placeholder="请输入帐号" id="numberInput" name="username"
                               autocomplete="off"/>
                    </div>
                    <img src="${staticpath}/front/dist/images/fork.png" alt="打叉" width="16" id="fork-img">
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label class="weui-label"><img
                            src="${staticpath}/front/dist/images/key.png" alt="密码" width="16"></label></div>
                    <div class="weui-cell__bd" class="key-weui-cell__bd">
                        <input class="weui-input" type="password" placeholder="请输入密码" id="passwordInput"
                               name="password" onfocus="this.type='password'"/>
                    </div>
                    <!-- <span id="passwordinfo" style="float: left;"></span> -->
                    <img src="${staticpath}/front/dist/images/eye.png" alt="眼睛" width="18" id="eye-img">
                </div>
            </div>
            <div class="button-sp-area">
                <a href="javascript:;" class="weui-btn weui-btn_plain-default">登录</a>
            </div>
        </form>
        <!--信息填写不完整-->
        <div id="dialogInformation" style="display: none;">
            <div class="weui-mask"></div>
            <div class="weui-dialog">
                <div class="weui-dialog__bd">登录信息不完整</div>
                <div class="weui-dialog__ft">
                    <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">确定</a>
                </div>
            </div>
        </div>
        <div id="toast" style="display: none;">
            <div class="weui-mask_transparent"></div>
            <div class="weui-toast">
                <i class="weui-icon-success-no-circle weui-icon_toast"></i>
                <p class="weui-toast__content">已完成</p>
            </div>
        </div>
    </div>
</div>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="https://res.wx.qq.com/open/libs/weuijs/1.1.1/weui.min.js"></script>
<script type="text/javascript">
    $(function () {
        var $numberInput = $('#numberInput'),//输入手机号input
                $inputCancel = $('#fork-img'),//打叉的图片
                $passwordInput = $("#passwordInput"),//输入密码input
                $inputShow = $("#eye-img");//眼睛的图片
        //对手机input做点击显示打叉
        $numberInput.on('click', function () {
            $inputCancel.show();
        });
        //点击打叉图片，清除手机input里面值
        $inputCancel.on('click', function () {
            $numberInput.val('');
        });
        //点击密码input框显示按钮
        $passwordInput.on('click', function () {
            $inputShow.show();
        });
        //点击眼睛图片，显示密码与隐藏密码
        var status = 0;
        $inputShow.on('click', function () {
            if (status == 0) {
                status = 1;
                $passwordInput.attr("type", "text");
            } else {
                status = 0;
                $passwordInput.attr("type", "password");
            }
        });
        //点击登陆按钮
        $(".weui-btn").click(function () {
            //手机号码的正则表达式
            var account = $("input[name='username']").val();
            var password1 = $("input[name='password']").val();
            if (account == '' || password1 == '') {
                //信息填写不完整提示
                $("#dialogInformation").css("display", "block");
                $(".weui-dialog__btn").click(function () {
                    $("#dialogInformation").css("display", "none");
                })
            } else {
                $.ajax({
                    type: 'POST',
                    url: '/rest/front/activity/login',
                    dataType: 'json',
                    data: {
                        username: account,
                        password: password1
                    },
                    success: function (data) {
                        if (data.success) {
                            var $toast = $('#toast');
                                $toast.fadeIn();
                                setTimeout(function () {
                                    $toast.fadeOut();
                                    window.location.href = '/rest/front/activity/toLoginChoose';
                                }, 1000);
                        } else {
                            weui.alert(data.message);
                        }
                    }
                })
            }
        })
    });
</script>
</body>
</html>
