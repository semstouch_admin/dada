<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_yb5s5cminzdgf1or.css">
    <style>
        .zero-tip {
            text-align: center;
            margin-top: 10px;
            font-size: 14px;
        }

        .data-bg {
            width: 180px;
            height: 150px;
            left: 50%;
            margin: 10px auto;
            display: block;
        }

        .img-div {
            height: 60%;
            display: none;
        }

        .weui-mask {
            z-index: 3000;
        }

        #searchBtn {
            width: 200px;
            height: 40px;
            line-height: 40px;
            border-radius: 6px;
            text-align: center;
            background-color: #ff7e00;
            color: #fff;
            margin: 50px auto;
        }

        .udLine {
            content: " ";
            left: 0;
            height: 1px;
            top: 0;
            right: 0;
            border-top: 1px solid #e5e5e5;
            color: #e5e5e5;
            -webkit-transform-origin: 0 0;
            transform-origin: 0 0;
            -webkit-transform: scaleY(0.5);
            transform: scaleY(0.5);
        }

        .time-content .weui-cell:before {
            height: 0 !important;
            border-top: 0px;
        }

        #showDatePicker, #goodsBtn {
            width: 24px;
            height: 24px;
            margin-top: 11px;
        }

        .newTitle {
            width: 150px !important;
            text-align: center;
            margin: 0 auto;
            display: block;
            word-break: keep-all;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>
</head>
<body ontouchstart>
<div class="page" id="pageOne">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="weui-tab__panel" style="padding-bottom: 0px">
                <div class="area-header page__hd header">
                    <a class="pull-left iconfont icon-zuobian" id="ni" onclick="javascript:history.go(-1);"></a>

                    <h1 class="title">
                        <span class="newTitle"></span>
                    </h1>
                    <img src="${staticpath}/front/dist/images/calendarBtn.png" class="pull-right-rili pull-right"
                         id="showDatePicker">
                    <img src="${staticpath}/front/dist/images/goods.png" class="pull-right-rili pull-right"
                         id="goodsBtn">
                </div>
                <div class="personal-content content">
                    <div class="main-money">
                        <div class="search-box">
                            <input type="text" class="key-word" placeholder="请搜索合伙人"/>
                        </div>
                        <h3 class="monthH3" data-status="0">当月流水金额</h3>
                        <h1 class="salesTotal"></h1>
                    <#--<h3 class="salesNumber"><span>已售产品：0件</span></h3>-->
                        <h3 class="salesNumber"></h3>
                    </div>
                    <div class="weui-cells-title  weui-cells">
                        <div class="weui-cell">
                            <div class="weui-cell__bd">
                                <p class="salesAreaTotal"></p>
                            </div>
                            <div class="weui-cell__ft salesAreaNumber"></div>
                        </div>
                    </div>
                    <!--缺省页面!-->
                    <div class="img-div">
                        <img src="${staticpath}/front/dist/images/data.png" alt="门店" class="data-bg"/>

                        <p class="zero-tip">亲,你还没有上传数据哦！</p>
                    </div>
                    <div class="weui-cells-information weui-cells" data-url="${basepath}/rest/front/sell/toStoreList">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page" id="pageTwo" style="display: none">
    <div class="page__bd">
        <div class="weui-tab">
            <div class="weui-tab__panel">
                <div class="area-header page__hd header" style="opacity:none;">
                    <a class="pull-left iconfont icon-zuobian" id="timeLeft"></a>

                    <h1 class="title">
                        选择时间
                    </h1>
                </div>
                <div class="time-content personal-content content"
                     style="margin-top:46px;background: #fff;height:100%;">
                    <div class="weui-cell">
                        <div class="weui-cell__hd"><label class="weui-label">开始日期</label></div>
                        <div class="weui-cell__bd">
                            <input class="weui-input offline-starTime" type="date" value="" placeholder="输入开始日期"/>
                        </div>
                    </div>
                    <div class="udLine">
                    </div>
                    <div class="weui-cell">
                        <div class="weui-cell__hd"><label class="weui-label">结束日期</label></div>
                        <div class="weui-cell__bd">
                            <input class="weui-input offline-endTime" type="date" value="" placeholder="输入结束日期"/>
                        </div>
                    </div>
                    <div class="udLine">
                    </div>
                    <div id="searchBtn">搜索</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:86%;bottom:0;position:absolute;left:84%;"
   href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script src="${staticpath}/front/dist/js/sell/cityList.js"></script>
<script>
    //搜索框模糊查询
    $('.key-word').keyup(function () {
        var key = $(this).val();
        $('.weui-cell_access').hide();
        if (key == '') {
            $('.weui-cell_access').show();
        }
        $('.weui-cell_access:contains("' + key + '")').show();
    });
    $('.key-word').blur(function () {
        var key = $(this).val();
        $('.weui-cell_access').hide();
        if (key == '') {
            $('.weui-cell_access').show();
        }
        $('.weui-cell_access:contains("' + key + '")').show();
    });
    $('.key-word').change(function () {
        var key = $(this).val();
        $('.weui-cell_access').hide();
        if (key == '') {
            $('.weui-cell_access').show();
        }
        $('.weui-cell_access:contains("' + key + '")').show();
    })
</script>
</body>
</html>
