<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_yb5s5cminzdgf1or.css">
    <style>
        .zero-tip {
            text-align: center;
            margin-top: 10px;
            font-size: 14px;
        }

        .data-bg {
            width: 180px;
            height: 150px;
            left: 50%;
            margin: 10px auto;
            display: block;
        }

        .img-div {
            height: 60%;
            display: none;
        }

        .weui-mask {
            z-index: 3000;
        }

        #searchBtn {
            width: 200px;
            height: 40px;
            line-height: 40px;
            border-radius: 6px;
            text-align: center;
            background-color: #ff7e00;
            color: #fff;
            margin: 50px auto;
        }

        .udLine {
            content: " ";
            left: 0;
            height: 1px;
            top: 0;
            right: 0;
            border-top: 1px solid #e5e5e5;
            color: #e5e5e5;
            -webkit-transform-origin: 0 0;
            transform-origin: 0 0;
            -webkit-transform: scaleY(0.5);
            transform: scaleY(0.5);
        }

        .time-content .weui-cell:before {
            height: 0 !important;
            border-top: 0px;
        }

        .newTitle {
            width: 150px !important;
            text-align: center;
            margin: 0 auto;
            display: block;
            word-break: keep-all;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>
</head>
<body ontouchstart>
<div class="page" id="pageOne">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="weui-tab__panel" style="padding-bottom: 0px">
                <div class="area-header page__hd header">
                    <a class="pull-left iconfont icon-zuobian" id="ni" onclick="javascript:history.go(-1);"></a>

                    <h1 class="title">
                        <span class="newTitle"></span>
                    </h1>
                    <a class="pull-right-rili pull-right iconfont icon-calendar" style="padding-right: 18px;"
                       id="showDatePicker"></a>
                </div>
                <div class="personal-content content">
                    <div class="main-money">
                        <div class="search-box">
                            <input type="text" class="key-word" placeholder="请搜索合伙人"/>
                        </div>
                        <h3 class="monthH3" data-status="0">当月流水金额</h3>

                        <h1 class="salesTotal"></h1>
                    <#--<h3 class="salesNumber"><span>已售产品：0件</span></h3>-->
                        <h3 class="salesNumber"></h3>
                    </div>
                    <div class="weui-cells-title  weui-cells">
                        <div class="weui-cell">
                            <div class="weui-cell__bd">
                                <p class="salesAreaTotal"></p>
                            </div>
                            <div class="weui-cell__ft salesAreaNumber"></div>
                        </div>
                    </div>
                    <!--缺省页面!-->
                    <div class="img-div">
                        <img src="${staticpath}/front/dist/images/data.png" alt="门店" class="data-bg"/>

                        <p class="zero-tip">亲,你还没有上传数据哦！</p>
                    </div>
                    <div class="weui-cells-information weui-cells" data-url="${basepath}/rest/front/sell/toStoreList">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page" id="pageTwo" style="display: none">
    <div class="page__bd">
        <div class="weui-tab">
            <div class="weui-tab__panel">
                <div class="area-header page__hd header" style="opacity:none;">
                    <a class="pull-left iconfont icon-zuobian" id="timeLeft"></a>

                    <h1 class="title">
                        选择时间
                    </h1>
                </div>
                <div class="time-content personal-content content"
                     style="margin-top:46px;background: #fff;height:100%;">
                    <div class="weui-cell">
                        <div class="weui-cell__hd"><label class="weui-label">开始日期</label></div>
                        <div class="weui-cell__bd">
                            <input class="weui-input offline-starTime" type="date" value="" placeholder="输入开始日期"/>
                        </div>
                    </div>
                    <div class="udLine">
                    </div>
                    <div class="weui-cell">
                        <div class="weui-cell__hd"><label class="weui-label">结束日期</label></div>
                        <div class="weui-cell__bd">
                            <input class="weui-input offline-endTime" type="date" value="" placeholder="输入结束日期"/>
                        </div>
                    </div>
                    <div class="udLine">
                    </div>
                    <div id="searchBtn">搜索</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:86%;bottom:0;position:absolute;left:84%;"
   href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png" width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<#--<script src="${staticpath}/front/dist/js/sell/productCityList.js"></script>-->
<script>
    //使用decodeURI()解码时
    function GetRequest() {
        var url = decodeURI(decodeURI(location.search)); //获取url中"?"符后的字串，使用了两次decodeRUI解码
        var theRequest = new Object();
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
            }
            return theRequest;
        }
    }
    var postData = GetRequest();
    var area = postData.area;//获取该活动的地区选择
    var product = postData.product;//获取该活动的全部商品的商品名称
    var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
    //查询标题
    $(".newTitle").text(area);
    //点击时间的选择又一个页面
    $("#showDatePicker").click(function () {
        $("#pageTwo").show();
        $("#pageOne").hide();
    });
    $("#timeLeft").click(function () {
        $("#pageTwo").hide();
        $("#pageOne").show();
    });
    var startDate = "",
            endDate = "";
    $("#searchBtn").click(function () {
        startDate = $(".offline-starTime").val();
        endDate = $(".offline-endTime").val();
        if (startDate == '' || endDate == '') {
            weui.alert("开始时间或结束时间未选择")
        } else {
            $("#pageTwo").hide();
            $("#pageOne").show();
            $(".monthH3").attr('data-status', 1);
            queryTotal();
            queryMonthTotal();
            queryProductList();
        }
    });
    //搜索框模糊查询
    var keyName = "";
    $('.key-word').keyup(function () {
        keyName = $(this).val();
        queryTotal();
        queryMonthTotal();
    });
    $('.key-word').blur(function () {
        keyName = $(this).val();
        queryTotal();
        queryMonthTotal();
    });
    //查询总金额与销量
    function queryTotal() {
        var params = "";
        if (sessionStorage.roleType == 2) {//品牌商角色
            params = {
                brandID: obj.id,
                area: area,
                product: product,
                cityNameLike: keyName
            };//缓存取到该账号的id};
        } else if (sessionStorage.roleType == 3) {//KA经理角色
            params = {
                kaID: obj.id,
                area: area,
                product: product,
                cityNameLike: keyName
            };
        }
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/rest/front/sell/selectSalesAmount',
            data: params,
            success: function (data) {
                //查询总流水金额
                var salesAreaTotalHtml = "";
                salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '</span>';
                $(".salesAreaTotal").html(salesAreaTotalHtml);
                //查询总销量产品
                var salesAreaNumberHtml = "";
                salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
                $(".salesAreaNumber").html(salesAreaNumberHtml);
            }
        })
    }
    queryTotal();

    function queryMonthTotal() {
        var params = "";
        if (sessionStorage.roleType == 2) {//品牌商角色
            if (startDate != "") {
                params = {
                    brandID: obj.id,
                    area: area,
                    product: product,
                    startDate: startDate,
                    endDate: endDate,
                    cityNameLike: keyName
                };//缓存取到该账号的id};
            } else if (startDate1 == "" || startDate1 == null) {
                params = {
                    brandID: obj.id,
                    area: area,
                    product: product,
                    startDate: startDate,
                    endDate: endDate,
                    cityNameLike: keyName
                };//缓存取到该账号的id};
            } else {
                params = {
                    brandID: obj.id,
                    area: area,
                    product: product,
                    startDate: startDate1,
                    endDate: endDate1,
                    cityNameLike: keyName
                };//缓存取到该账号的id};
            }
        } else if (sessionStorage.roleType == 3) {//KA经理角色
            if (startDate != "") {
                params = {
                    kaID: obj.id,
                    area: area,
                    product: product,
                    startDate: startDate,
                    endDate: endDate,
                    cityNameLike: keyName
                };
            } else if (startDate1 == "" || startDate1 == null) {
                params = {
                    kaID: obj.id,
                    area: area,
                    product: product,
                    startDate: startDate,
                    endDate: endDate,
                    cityNameLike: keyName
                };
            } else {
                params = {
                    kaID: obj.id,
                    area: area,
                    product: product,
                    startDate: startDate1,
                    endDate: endDate1,
                    cityNameLike: keyName
                };
            }

        }
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/rest/front/sell/selectMonthSalesAmount',
            data: params,
            success: function (data) {
                if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                    $(".monthH3").text("流水金额");
                } else {
                    $(".monthH3").text("当月流水金额");
                }
                //查询总流水金额
                var salesTotalHtml = "";
                salesTotalHtml += '<span>' + data.data.salesAmount + '</span>';
                $(".salesTotal").html(salesTotalHtml);
                //查询已售产品
                var salesNumberHtml = "";
                if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                    salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
                } else {
                    salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
                }
                $(".salesNumber").html(salesNumberHtml);
            }
        })
    }
    queryMonthTotal();
    function queryProductList() {
        var params = "";
        if (sessionStorage.roleType == 2) {//品牌商角色
            if (startDate != "") {
                params = {brandID: obj.id, product: product, area: area, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
            } else if (startDate1 == "" || startDate1 == null) {
                params = {brandID: obj.id, product: product, area: area, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
            } else {
                params = {brandID: obj.id, product: product, area: area, startDate: startDate1, endDate: endDate1};//缓存取到该账号的id};
            }
        } else if (sessionStorage.roleType == 3) {//KA经理角色
            if (startDate != "") {
                params = {kaID: obj.id, product: product, area: area, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
            } else if (startDate1 == "" || startDate1 == null) {
                params = {kaID: obj.id, product: product, area: area, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
            } else {
                params = {kaID: obj.id, product: product, area: area, startDate: startDate1, endDate: endDate1};//缓存取到该账号的id};
            }
        }
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/rest/front/sell/selectCityList',
            data: params,
            success: function (data) {
                var areaList = data.data;
                if (areaList == null || areaList == '') {
                    $('.img-div').show();
                } else {
                    $('.img-div').hide();
                    var areaHtml = "";
                    $.each(areaList, function (i, o) {
                        areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-cityID="' + o.cityID + '" data-name="' + o.cityName + '"><div class="weui-cell__bd">' +
                                '<p>' + o.cityName + '</p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                                '</div><div class="weui-cell__ft"></div></a>';
                    });

                    $(".weui-cells-information").html(areaHtml);
                    $(".weui-cell_access").click(function () {
                        var cityName = $(this).attr('data-name');
                        var cityID = $(this).attr('data-cityID');
                        if (startDate != "" ) {
                            window.location.href = '/rest/front/sell/toProductStoreList' + '?product=' + product + '&area=' + area + '&cityName=' + cityName + '&cityID=' + cityID + "&startDate=" + startDate + "&endDate=" + endDate;
                        } else if (startDate1 == "" || startDate1 == null) {
                            window.location.href = '/rest/front/sell/toProductStoreList' + '?product=' + product + '&area=' + area + '&cityName=' + cityName + '&cityID=' + cityID + "&startDate=" + startDate + "&endDate=" + endDate;
                        } else {
                            window.location.href = '/rest/front/sell/toProductStoreList' + '?product=' + product + '&area=' + area + '&cityName=' + cityName + '&cityID=' + cityID + "&startDate=" + startDate1 + "&endDate=" + endDate1;
                        }
                    })
                }
            }
        })
    }
    queryProductList();
</script>
<script>
    //搜索框模糊查询
    $('.key-word').keyup(function () {
        var key = $(this).val();
        $('.weui-cell_access').hide();
        if (key == '') {
            $('.weui-cell_access').show();
        }
        $('.weui-cell_access:contains("' + key + '")').show();
    });
    $('.key-word').blur(function () {
        var key = $(this).val();
        $('.weui-cell_access').hide();
        if (key == '') {
            $('.weui-cell_access').show();
        }
        $('.weui-cell_access:contains("' + key + '")').show();
    });
    $('.key-word').change(function () {
        var key = $(this).val();
        $('.weui-cell_access').hide();
        if (key == '') {
            $('.weui-cell_access').show();
        }
        $('.weui-cell_access:contains("' + key + '")').show();
    })
</script>
</body>
</html>
