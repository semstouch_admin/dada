<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <!--   <link rel="stylesheet" href="css/example.css"> -->
    <style>
        .weui-cell .weui-cell__bd p:first-child {
            font-size: 16px;
            color: #353535;
        }

        .weui-cell .weui-cell__bd {
            font-size: 14px;
            color: #999999;
        }

        .zero-tip {
            text-align: center;
            margin-top: 10px;
            font-size: 14px;
        }

        .data-bg {
            width: 240px !important;
            height: 180px !important;
            left: 50%;
            margin: 60px auto 0;
            display: block;
        }

        .img-div {
            height: 90%;
            display: none;
        }

        .weui-icon_toast {
            margin: 0;
        }

        .weui-toast {
            z-index: 5000;
            min-height: 6.6em;
        }
        .area-header:after{
            height:0;
        }
    </style>
</head>
<body ontouchstart>
<div class="page">
    <div class="area-header page__hd header">
        <a class="pull-left iconfont icon-zuobian" id="back"></a>

        <h1 class="title">
            签到记录
        </h1>
        <a class="pull-right">签到</a>
        <input id="addressLocation" type="hidden" value=""/>
    </div>
    <div class="data-content content">
        <div class="information-weui-cells weui-cells" style="z-index: 100;">
            <!--缺省页面!-->
            <div class="img-div">
                <img src="${staticpath}/front/dist/images/activity.png" alt="门店" class="data-bg"/>

                <p class="zero-tip">亲,你还没有任何签到列表哦！</p>
            </div>
        </div>
        <div id="toast" style="display: none;">
            <div class="weui-mask_transparent"></div>
            <div class="weui-toast">
                <i class="weui-icon-success-no-circle weui-icon_toast"></i>

                <p class="weui-toast__content">签到成功</p>
            </div>
        </div>
    </div>
</div>
<!--返回首页的按钮-->
<a style="display: block;z-index: 800000;float:left;top:86%;bottom:0;position:absolute;left:84%;"
   href="${basepath}/rest/front/activity/toLoginChoose"><img src="${staticpath}/front/dist/images/img-1.png"
                                                              width="46"></a>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.changer.hk/jquery/plugins/jquery.cookie.js"></script>
<#--引入微信js文件-->
<script type='text/javascript' src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js" charset="utf-8"></script>
<script src="https://res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script src="${staticpath}/front/dist/js/sign/citySignList.js"></script>
</body>
</html>
