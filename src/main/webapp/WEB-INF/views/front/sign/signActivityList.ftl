<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title>
        嗒达通
    </title>
    <link rel="stylesheet" href="${staticpath}/front/dist/css/weui.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/base.css">
    <link rel="stylesheet" type="text/css" href="${staticpath}/front/dist/css/master.css">
    <link rel="stylesheet" type="text/css" href="//at.alicdn.com/t/font_397517_apq9zsvphrr4quxr.css">
    <style>
        .zero-tip {
            text-align: center;
            margin-top: 10px;
            font-size: 14px;
        }

        .data-bg {
            width: 240px !important;
            height: 180px !important;
            left: 50%;
            margin: 60px auto 0;
            display: block;
        }

        .img-div {
            height: 90%;
            display: none;
        }

        /*.area-header,.sendOffline-content,.weui-tabbar{*/
        /*display: block;*/
        /*}*/
        .area-header, offline-header {
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            height: 46px;
        }

        .offline-content {
            width: 100%;
            position: absolute;
            margin-top: 46px;
            top: 0;
            bottom: 0;
            overflow-y: scroll;
        }

        .area-header {
            background-color: #ff7e00 !important;
            background-image: none !important;
            top: 0;
        }

    </style>
</head>
<body ontouchstart>
<div class="page">
    <div class="page__bd" style="height: 100%;">
        <div class="weui-tab">
            <div class="weui-tab__panel" style="padding-bottom: 0px">
                <!-- 品牌商活动列表 -->
                <div class="area-header page__hd header">
                    <a class="pull-left iconfont icon-zuobian" href="/rest/front/activity/toLoginChoose"
                       style="position: fixed;"></a>
                    <h1 class="title">
                    </h1>
                </div>
                <div class="offline-content content">
                    <div class="offline-month">
                        <!--缺省页面!-->
                        <div class="img-div">
                            <img src="${staticpath}/front/dist/images/activity.png" alt="门店" class="data-bg"/>
                            <p class="zero-tip">亲,你还没有任何活动哦！</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script type='text/javascript' src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js" charset="utf-8"></script>
<script src="${staticpath}/front/dist/js/sign/signActivityList.js"></script>
</body>
</html>
