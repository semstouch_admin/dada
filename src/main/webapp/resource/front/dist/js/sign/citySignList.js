function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var activityID = getQueryString("id");
var cityID = getQueryString("cityID");
var storeID = getQueryString("storeID");
//查询签到列表
function querySign() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sign/selectSignList',
        data: {
            activityID: activityID,
            cityID: cityID,
            storeID: storeID
        },
        success: function (data) {
            var list = data.data;
            if (list == null || list == '') {
                $('.img-div').show();
            } else {
                var listHtml = "";
                $.each(list, function (i, o) {
                    listHtml += '<a class="weui-cell" href="javascript:;"><div class="weui-cell__bd"><p>' + o.createTime + '</p> <p>' + o.location + '</p> </div> <div class="weui-cell__ft">' + o.cityName + '</div></a>';
                });
                $(".information-weui-cells").html(listHtml);
            }
        }
    })
}

querySign();
//点击签到列表，刷新本页面
var n = 0;
$(".pull-right").click(function () {
    //微信js sdk接口获取gps坐标
    /*$(".pull-right").remove();*/
    if (n == 0) {
        n = 1;
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/rest/front/activity/toScan',
            data: {
                pageUrl: location.href.split('#')[0]
            },
            success: function (res) {
                if (res.success) {
                    wx.config({
                        debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                        appId: res.data.appId, // 必填，公众号的唯一标识
                        timestamp: res.data.timestamp, // 必填，生成签名的时间戳
                        nonceStr: res.data.noncestr, // 必填，生成签名的随机串
                        signature: res.data.signature,// 必填，签名，见附录1
                        jsApiList: ['getLocation'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                    });
                    wx.ready(function () {
                        wx.getLocation({
                            type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
                            success: function (res) {
                                var lat = res.latitude; // 纬度，浮点数，范围为90 ~ -90
                                var lng = res.longitude; // 经度，浮点数，范围为180 ~ -180。
                                getLocationAddress(lat, lng);
                            }
                        });
                    });
                } else {
                    alert(res.message);
                }
            }
        });
    }
});
/**
 * 调用后台将gps坐标转化成物理地址
 * @param lat
 * @param lng
 */
function getLocationAddress(lat, lng) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/activity/getLocationAddress',
        data: {
            lat: lat,
            lng: lng
        },
        success: function (res) {
            if (res.success) {
                $("#addressLocation").val(res.data);
                getAddress();
            }
        }
    });
}


function getAddress() {

    if ($("#addressLocation").val() != '') {
        $.ajax({
            type: 'post',
            url: '/rest/front/sign/insertJson',
            dataType: 'json',
            data: {
                activityID: activityID,
                cityID: cityID,
                storeID: storeID,
                location: $("#addressLocation").val()
            },
            success: function (data) {
                var $toast = $('#toast');
                $toast.fadeIn();
                setTimeout(function () {
                    $toast.fadeOut();
                    window.location.href = "/rest/front/activity/toSignList?id=" + activityID + "&city=" + cityID + "&storeID=" + storeID;
                }, 1000);
            }
        })
    } else {
        weui.alert("定位失败");
    }
}
//返回
$("#back").click(function () {
    window.location.href = "/rest/front/sign/toSignStoreList?id=" + activityID;
});