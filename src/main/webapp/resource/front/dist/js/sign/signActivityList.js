//查询名称
$.ajax({
    type: 'GET',
    dataType: 'json',
    url: '/rest/front/activity/selectUser',
    success: function (data) {
        $(".title").text(data.data.nickname);
    }
});
//查询城市合伙人的打卡活动列表
$.ajax({
    type: 'GET',
    dataType: 'json',
    url: '/rest/front/activity/selectCityActivityList',
    data: {
        insertStatus: "2"
    },
    success: function (data) {
        var activityList = data.data,
            activityHtml = '';
        if (activityList == null || activityList == '') {
            $('.img-div').show();
        } else {
            var month = '';
            var year = '';
            $.each(activityList, function (i, o) {
                if (o.acStatus == "1") {
                    var year1 = o.starTime.substr(0, 4);//截取年份
                    var mon = o.starTime.slice(5, 7);//截取月份
                    if ((mon != "" && month != mon) || (year1 != "" && year != year1)) {
                        month = mon;
                        year = year1;
                        var monIndividual = o.starTime.slice(5, 6);//截取月份的个位数
                        var monTen = o.starTime.slice(6, 7);//截取月份的十位数
                        if (monIndividual == 0) {
                            activityHtml += ' <div class="month-div"><span><div></div><p>' + monTen + '月</p></span></div>';
                        } else {
                            activityHtml += ' <div class="month-div"><span><div></div><p>' + mon + '月</p></span></div>';
                        }
                    }
                    activityHtml += '<div class="month-weui-cells weui-cells" data-id="' + o.id + '" data-status="' + o.insertStatus + '">' +
                        '<h5>' + o.name + '</h5>';
                    var imgArr = o.picture.split(',');
                    activityHtml += '<img src="/' + imgArr[0] + '" alt="活动图片">';
                    activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/progress.png" alt="进行中"></div>';
                    activityHtml += '<a class="weui-cell weui-cell_access" href="/rest/front/sign/toSignStoreList?id=' + o.id + '&status=' + o.insertStatus + '">';
                    if (o.storeMember == "0") {
                        activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div>';
                    } else {
                        activityHtml += '<div class="weui-cell__bd"><p>' + o.storeMember + '家门店正在参与活动</p></div>';
                    }
                    activityHtml += '<div class="weui-cell__ft detailBtn">签到打卡</div></a></div>';
                }
            });
            $(".offline-month").html(activityHtml);
        }
    }
});
