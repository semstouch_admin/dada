/**
 * Created by Administrator on 2017/9/1.
 */
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var activityID = getQueryString("id");
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
//城市合伙人进行中签到页面
$.ajax({
    type: "GET",
    url: "/rest/front/activitycharge/selectCsdList",
    data: {
        activityID: activityID,
        cityID: obj.id
    },
    dataType: "json",
    success: function (data) {
        var list = data.data;
        var listHtml = "";
        $.each(list, function (i, o) {
            listHtml += '<div class="information-weui-cells weui-cells"> <a class="weui-cell weui-cell_access" href="javascript:;" data-id="' + o.storeID + '"><div class="weui-cell__bd">' +
                '<p>' + o.storeName + '</p>' +
                '<p>地址：' + o.storeAddress + '</p>' +
                '</div><div class="weui-cell__ft"></div></a>' +
                '<div class="images-weui-cell  weui-cell"><div class="weui-cell__bd">' +
                '<p>已打卡' + o.signAmount + '次</p></div>' +
                '<div class="weui-cell__ft"><p class="signBtn" data-storeID="' + o.storeID + '">签到打卡</p></div>' +
                '</div></div>';
        });
        $(".kaStoreList").html(listHtml);
        //跳转到签到详情页面
        $(".signBtn").click(function () {
            var id = activityID;
            var cityID= obj.id;
            var storeID = $(this).attr('data-storeID');
            var signUrl = $(".kaStoreList").attr('data-signUrl') + "?id=" + id + "&cityID=" + cityID + "&storeID=" + storeID;
            window.location.href = signUrl;
        });
    }
});
