//获取登陆用户缓存的值，存入
$.ajax({
    type: 'GET',
    dataType: 'json',
    url: '/rest/front/activity/getSession',
    success: function (data) {
        sessionStorage.id = data.data.user.id;
        var str = JSON.stringify(data.data.user);
        sessionStorage.obj = str;
        sessionStorage.roleType = data.data.user.rid;
        query();
    }
});
//判断角色显示不同东西
function query() {
    if (sessionStorage.roleType == 2) {   //品牌商
        $("#addBtn").show();//显示添加活动
    } else if (sessionStorage.roleType == 3) {//Ka经理

    } else if (sessionStorage.roleType == 4) {//城市合伙人
        $("#imgBtn").show();//显示
        $("#signBtn").show();//显示签到打卡
        $("#checkBtn").attr("href","/rest/front/sell/toStoreList");
    }

}

