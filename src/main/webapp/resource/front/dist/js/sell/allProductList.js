/**
 * Created by Administrator on 2017/9/2.
 */
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
//点击时间的选择又一个页面
$("#showDatePicker").click(function () {
    $("#pageTwo").show();
    $("#pageOne").hide();
});
$("#timeLeft").click(function () {
    $("#pageTwo").hide();
    $("#pageOne").show();
});
var startDate = "",
    endDate = "";
$("#searchBtn").click(function () {
    startDate = $(".offline-starTime").val();
    endDate = $(".offline-endTime").val();
    if (startDate == '' || endDate == '') {
        weui.alert("开始时间或结束时间未选择")
    } else {
        $("#pageTwo").hide();
        $("#pageOne").show();
        queryTotal();
        $(".monthH3").attr('data-status', 1);
        queryProductList();
        queryMonthTotal();
    }
});
//搜索框模糊查询
var keyName = "";
$('.key-word').keyup(function () {
    keyName = $(this).val();
    queryTotal();
    queryMonthTotal();

});
$('.key-word').blur(function () {
    keyName = $(this).val();
    queryTotal();
    queryMonthTotal();
});

//查询总金额与销量
function queryTotal() {
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        params = {brandID: obj.id, productLike: keyName};//缓存取到该账号的id};
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        params = {kaID: obj.id, productLike: keyName};
    } else if (sessionStorage.roleType == 4) {//城市合伙人
        params = {cityID: obj.id,productLike: keyName};
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectSalesAmount',
        data: params,
        success: function (data) {
            //查询总流水金额
            var salesAreaTotalHtml = "";
            salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '</span>';
            $(".salesAreaTotal").html(salesAreaTotalHtml);
            //查询总销量产品
            var salesAreaNumberHtml = "";
            salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
            $(".salesAreaNumber").html(salesAreaNumberHtml);
        }
    })
}
queryTotal();
//查询当月金额与销量
function queryMonthTotal() {
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        params = {brandID: obj.id, startDate: startDate, endDate: endDate, productLike: keyName};//缓存取到该账号的id};
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        params = {kaID: obj.id, startDate: startDate, endDate: endDate, productLike: keyName};
    } else if (sessionStorage.roleType == 4) {//城市合伙人
        params = {cityID: obj.id, startDate: startDate, endDate: endDate, productLike: keyName};
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectMonthSalesAmount',
        data: params,
        success: function (data) {
            if ($(".monthH3").attr('data-status') == 1) {
                $(".monthH3").text("流水金额");
            }else{
                $(".monthH3").text("当月流水金额");
            }
            //查询总流水金额
            var salesTotalHtml = "";
            salesTotalHtml += '<span class="h1-span">' + data.data.salesAmount + '</span>';
            $(".salesTotal").html(salesTotalHtml);
            //查询已售产品
            var salesNumberHtml = "";
            if ($(".monthH3").attr('data-status') == 1) {
                salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            } else {
                salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            }
            $(".salesNumber").html(salesNumberHtml);
        }
    })
}
queryMonthTotal();
function queryProductList() {
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        params = {brandID: obj.id, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        params = {kaID: obj.id, startDate: startDate, endDate: endDate};
    } else if (sessionStorage.roleType == 4) {//城市合伙人
        params = {cityID: obj.id, startDate: startDate, endDate: endDate};
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectProductList',
        data: params,
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-name="' + o.product + '"><div class="weui-cell__bd">' +
                        '<p>' + o.product + '<span style="margin-left: 6px;color: #999;font-size: 14px;">(条形码：' + o.barCode + ')</span></p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div><div class="weui-cell__ft"></div></a>';
                });
                $(".weui-cells-information").html(areaHtml);
                $(".weui-cell_access").click(function () {
                    var product = $(this).attr('data-name');
                    if (sessionStorage.roleType == 4) {//城市合伙人跳转到店铺列表
                        window.location.href = '/rest/front/sell/toProductStoreList' + '?product=' + product+"&startDate="+startDate+"&endDate="+endDate;
                    } else {//KA经理与品牌商跳转到地区列表
                        window.location.href = '/rest/front/sell/toProductAreaList' + '?product=' + product+"&startDate="+startDate+"&endDate="+endDate;
                    }
                })
            }
        }
    })
}
queryProductList();
