/**
 * Created by Administrator on 2017/9/2.
 */
//使用decodeURI()解码时
function GetRequest() {
    var url = decodeURI(decodeURI(location.search)); //获取url中"?"符后的字串，使用了两次decodeRUI解码
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
        return theRequest;
    }
}
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
$(function () {
    //判断角色
    if (sessionStorage.roleType == 2) {
        var postData = GetRequest();
        var cityName = postData.cityName;//获取该活动的上一个选择地区的值
        var startDate1 = postData.startDate;//获取该活动的开始日期
        var endDate1 = postData.endDate;//获取该活动的结束日期
        $(".newTitle").text(cityName);
        //查询品牌商总金额与销量
        queryTotal();
        queryMonthTotal();
        //查询品牌商门店列表
        queryBrand();

    } else if (sessionStorage.roleType == 3) {
        var postData = GetRequest();
        var cityName = postData.cityName;//获取该活动的上一个选择地区的值
        var startDate1 = postData.startDate;//获取该活动的开始日期
        var endDate1 = postData.endDate;//获取该活动的结束日期
        $(".newTitle").text(cityName);
        //查询KA经理总金额与销量
        queryTotal();
        queryMonthTotal();
        //查询KA经理门店列表
        queryKA();

    } else if (sessionStorage.roleType == 4) {
        var postData = GetRequest();
        var product = postData.product;//获取该活动的全部商品的商品名称
        var startDate1 = postData.startDate;//获取该活动的开始日期
        var endDate1 = postData.endDate;//获取该活动的结束日期
        $(".newTitle").text(product);
        //查询城市合伙人总金额与销量
        queryPartnerTotal();
        queryPartnerMonthTotal();
        //查询城市合伙人门店列表
        queryPartner();
    }
});
//点击时间的选择又一个页面
$("#showDatePicker").click(function () {
    $("#pageTwo").show();
    $("#pageOne").hide();
});
$("#timeLeft").click(function () {
    $("#pageTwo").hide();
    $("#pageOne").show();
});
var startDate = "",
    endDate = "";
$("#searchBtn").click(function () {
    startDate = $(".offline-starTime").val();
    endDate = $(".offline-endTime").val();
    if (startDate == '' || endDate == '') {
        weui.alert("开始时间或结束时间未选择")
    } else {
        $("#pageTwo").hide();
        $("#pageOne").show();
        $(".monthH3").attr('data-status', 1);
        if (sessionStorage.roleType == 2) {
            queryBrand();
            queryTotal();
            queryMonthTotal();
        } else if (sessionStorage.roleType == 3) {
            queryKA();
            queryTotal();
            queryMonthTotal();
        } else {
            queryPartner();
            queryPartnerTotal();
            queryPartnerMonthTotal();
        }
    }
});
//搜索框模糊查询
var keyName = "";
$('.key-word').keyup(function () {
    keyName = $(this).val();
    if (sessionStorage.roleType == 2 || sessionStorage.roleType == 3) {
        queryTotal();
        queryMonthTotal();
    } else {
        queryPartnerTotal();
        queryPartnerMonthTotal();
    }
});
$('.key-word').blur(function () {
    keyName = $(this).val();
    if (sessionStorage.roleType == 2 || sessionStorage.roleType == 3) {
        queryTotal();
        queryMonthTotal();
    } else {
        queryPartnerTotal();
        queryPartnerMonthTotal();
    }
});

//查询品牌商总金额与销量
function queryTotal() {
    var postData = GetRequest();
    var cityID = postData.cityID;//获取该活动的地区ID
    var area = postData.areaName;//获取该活动的地区选择
    var product = postData.product;//获取该活动的全部商品的商品名称
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        params = {
            brandID: obj.id,
            area: area,
            cityID: cityID,
            product: product,
            storeNameLike: keyName
        };//缓存取到该账号的id};
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        params = {
            kaID: obj.id,
            area: area,
            cityID: cityID,
            product: product,
            storeNameLike: keyName
        };
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectSalesAmount',
        data: params,
        success: function (data) {
            //查询总流水金额
            var salesAreaTotalHtml = "";
            salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '</span>';
            $(".salesAreaTotal").html(salesAreaTotalHtml);
            //查询总销量产品
            var salesAreaNumberHtml = "";
            salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
            $(".salesAreaNumber").html(salesAreaNumberHtml);
        }
    })
}
function queryMonthTotal() {
    var postData = GetRequest();
    var cityID = postData.cityID;//获取该活动的地区ID
    var area = postData.areaName;//获取该活动的地区选择
    var product = postData.product;//获取该活动的全部商品的商品名称
    var startDate1 = postData.startDate;//获取该活动的开始日期
    var endDate1 = postData.endDate;//获取该活动的结束日期
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        if (startDate != "") {
            params = {
                brandID: obj.id,
                area: area,
                cityID: cityID,
                product: product,
                startDate: startDate,
                endDate: endDate,
                storeNameLike: keyName
            };//缓存取到该账号的id};
        } else if (startDate1 == "" || startDate1 == null) {
            params = {
                brandID: obj.id,
                area: area,
                cityID: cityID,
                product: product,
                startDate: startDate,
                endDate: endDate,
                storeNameLike: keyName
            };//缓存取到该账号的id};
        } else {
            params = {
                brandID: obj.id,
                area: area,
                cityID: cityID,
                product: product,
                startDate: startDate1,
                endDate: endDate1,
                storeNameLike: keyName
            };//缓存取到该账号的id};
        }
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        if (startDate != "") {
            params = {
                kaID: obj.id,
                area: area,
                cityID: cityID,
                product: product,
                startDate: startDate,
                endDate: endDate,
                storeNameLike: keyName
            };//缓存取到该账号的id};
        } else if (startDate1 == "" || startDate1 == null) {
            params = {
                kaID: obj.id,
                area: area,
                cityID: cityID,
                product: product,
                startDate: startDate,
                endDate: endDate,
                storeNameLike: keyName
            };//缓存取到该账号的id};
        }
        else {
            params = {
                kaID: obj.id,
                area: area,
                cityID: cityID,
                product: product,
                startDate: startDate1,
                endDate: endDate1,
                storeNameLike: keyName
            };//缓存取到该账号的id};
        }

    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectMonthSalesAmount',
        data: params,
        success: function (data) {
            if ($(".monthH3").attr('data-status') == 1) {
                $(".monthH3").text("流水金额");
            } else {
                $(".monthH3").text("当月流水金额");
            }
            //查询总流水金额
            var salesTotalHtml = "";
            salesTotalHtml += '<span>' + data.data.salesAmount + '</span>';
            $(".salesTotal").html(salesTotalHtml);
            //查询已售产品
            var salesNumberHtml = "";
            if ($(".monthH3").attr('data-status') == 1) {
                salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            } else {
                salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            }
            $(".salesNumber").html(salesNumberHtml);
        }
    })
}
//查询城市合伙人总金额与销量
function queryPartnerTotal() {
    var postData = GetRequest();
    var product = postData.product;//获取该活动的全部商品的商品名称
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectSalesAmount',
        data: {
            cityID: obj.id,//缓存取到该账号的id,
            product: product,
            oreName: keyName
        },
        success: function (data) {
            //查询总流水金额
            var salesAreaTotalHtml = "";
            salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '</span>';
            $(".salesAreaTotal").html(salesAreaTotalHtml);
            //查询总销量产品
            var salesAreaNumberHtml = "";
            salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
            $(".salesAreaNumber").html(salesAreaNumberHtml);
        }
    })
}
function queryPartnerMonthTotal() {
    var postData = GetRequest();
    var product = postData.product;//获取该活动的全部商品的商品名称
    var startDate1 = postData.startDate;//获取该活动的开始日期
    var endDate1 = postData.endDate;//获取该活动的结束日期
    if (startDate != "") {
        params = {
            cityID: obj.id,//缓存取到该账号的id,
            product: product,
            startDate: startDate,
            endDate: endDate,
            oreName: keyName
        };//缓存取
    } else if (startDate1 == "" || startDate1 == null) {
        params = {
            cityID: obj.id,//缓存取到该账号的id,
            product: product,
            startDate: startDate,
            endDate: endDate,
            oreName: keyName
        };//缓存取
    } else {
        params = {
            cityID: obj.id,//缓存取到该账号的id,
            product: product,
            startDate: startDate1,
            endDate: endDate1,
            oreName: keyName
        };//缓存取
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectMonthSalesAmount',
        data: params,
        success: function (data) {
            if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                $(".monthH3").text("流水金额");
            } else {
                $(".monthH3").text("当月流水金额");
            }
            //查询总流水金额
            var salesTotalHtml = "";
            salesTotalHtml += '<span>' + data.data.salesAmount + '</span>';
            $(".salesTotal").html(salesTotalHtml);
            //查询已售产品
            var salesNumberHtml = "";
            if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            } else {
                salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            }
            $(".salesNumber").html(salesNumberHtml);
        }
    })
}
function queryBrand() {
    var postData = GetRequest();
    var cityID = postData.cityID;//获取该活动的地区ID
    var area = postData.areaName;//获取该活动的地区选择
    var product = postData.product;//获取该活动的全部商品的商品名称
    var startDate1 = postData.startDate;//获取该活动的开始日期
    var endDate1 = postData.endDate;//获取该活动的结束日期
    if (startDate != "") {
        params = {
            brandID: obj.id,//缓存取到该账号的id
            area: area,
            cityID: cityID,
            product: product,
            startDate: startDate,
            endDate: endDate
        };//缓存取
    } else if (startDate1 == "" || startDate1 == null) {
        params = {
            brandID: obj.id,//缓存取到该账号的id
            area: area,
            cityID: cityID,
            product: product,
            startDate: startDate,
            endDate: endDate
        };//缓存取
    } else {
        params = {
            brandID: obj.id,//缓存取到该账号的id
            area: area,
            cityID: cityID,
            product: product,
            startDate: startDate1,
            endDate: endDate1
        };//缓存取
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectStoreList',
        data: params,
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {

                    areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-storeID="' + o.storeID + '" data-name="' + o.storeName + '"><div class="weui-cell__bd">' +
                        '<p>' + o.storeName + '</p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div></a>';

                });
                $(".weui-cells-information").html(areaHtml);
            }
        }
    })
}
function queryKA() {
    var postData = GetRequest();
    var cityID = postData.cityID;//获取该活动的地区ID
    var area = postData.areaName;//获取该活动的地区选择
    var product = postData.product;//获取该活动的全部商品的商品名称
    var startDate1 = postData.startDate;//获取该活动的开始日期
    var endDate1 = postData.endDate;//获取该活动的结束日期
    if (startDate != "") {
        params = {
            kaID: obj.id,//缓存取到该账号的id
            area: area,
            cityID: cityID,
            product: product,
            startDate: startDate,
            endDate: endDate
        };//缓存取
    } else if (startDate1 == "" || startDate1 == null) {
        params = {
            kaID: obj.id,//缓存取到该账号的id
            area: area,
            cityID: cityID,
            product: product,
            startDate: startDate,
            endDate: endDate
        };//缓存取
    } else {
        params = {
            kaID: obj.id,//缓存取到该账号的id
            area: area,
            cityID: cityID,
            product: product,
            startDate: startDate1,
            endDate: endDate1
        };//缓存取
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectStoreList',
        data: params,
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-storeID="' + o.storeID + '" data-name="' + o.storeName + '"><div class="weui-cell__bd">' +
                        '<p>' + o.storeName + '</p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div></a>';

                });
                $(".weui-cells-information").html(areaHtml);
            }

        }
    })
}
function queryPartner() {
    var postData = GetRequest();
    var product = postData.product;//获取该活动的全部商品的商品名称
    var startDate1 = postData.startDate;//获取该活动的开始日期
    var endDate1 = postData.endDate;//获取该活动的结束日期
    if (startDate != "") {
        params = {
            cityID: obj.id,
            product: product,//缓存取到该账号的id,
            startDate: startDate,
            endDate: endDate
        };//缓存取
    } else if (startDate1 == "" || startDate1 == null) {
        params = {
            cityID: obj.id,
            product: product,//缓存取到该账号的id,
            startDate: startDate,
            endDate: endDate
        };//缓存取
    } else {
        params = {
            cityID: obj.id,
            product: product,//缓存取到该账号的id,
            startDate: startDate1,
            endDate: endDate1
        };//缓存取
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectStoreList',
        data: params,
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-storeID="' + o.storeID + '" data-name="' + o.storeName + '"><div class="weui-cell__bd">' +
                        '<p>' + o.storeName + '</p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div></a>';
                });
                $(".weui-cells-information").html(areaHtml);
            }
        }
    })
}

