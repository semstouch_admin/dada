//使用decodeURI()解码时
function GetRequest() {
    var url = decodeURI(decodeURI(location.search)); //获取url中"?"符后的字串，使用了两次decodeRUI解码
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
        return theRequest;
    }
}
var postData = GetRequest();
var product = postData.product;//获取该活动的地区选择
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
var startDate1 = postData.startDate;//获取该活动的开始日期
var endDate1 = postData.endDate;//获取该活动的结束日期
//查询标题
$(".newTitle").text(product);
//点击时间的选择又一个页面
$("#showDatePicker").click(function () {
    $("#pageTwo").show();
    $("#pageOne").hide();
});
$("#timeLeft").click(function () {
    $("#pageTwo").hide();
    $("#pageOne").show();
});
var startDate = "",
    endDate = "";
$("#searchBtn").click(function () {
    startDate = $(".offline-starTime").val();
    endDate = $(".offline-endTime").val();
    if (startDate == '' || endDate == '') {
        weui.alert("开始时间或结束时间未选择")
    } else {
        $("#pageTwo").hide();
        $("#pageOne").show();
        $(".monthH3").attr('data-status', 1);
        queryTotal();
        queryMonthTotal();
        queryProductList();
    }
});
//搜索框模糊查询
var keyName = "";
$('.key-word').keyup(function () {
    keyName = $(this).val();
    queryTotal();
    queryMonthTotal();
});
$('.key-word').blur(function () {
    keyName = $(this).val();
    queryTotal();
    queryMonthTotal();
});

//查询总金额与销量
function queryTotal() {
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        params = {brandID: obj.id, product: product, areaLike: keyName};//缓存取到该账号的id};
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        params = {kaID: obj.id, product: product, areaLike: keyName};
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectSalesAmount',
        data: params,
        success: function (data) {
            //查询总流水金额
            var salesAreaTotalHtml = "";
            salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '</span>';
            $(".salesAreaTotal").html(salesAreaTotalHtml);
            //查询总销量产品
            var salesAreaNumberHtml = "";
            salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
            $(".salesAreaNumber").html(salesAreaNumberHtml);
        }
    })
}
queryTotal();
//查询当月金额与销量
function queryMonthTotal() {
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        if (startDate != "") {
            params = {brandID: obj.id, product: product, startDate: startDate, endDate: endDate, areaLike: keyName};//缓存取到该账号的id};
        }
        else if (startDate1 == "" || startDate1 == null) {
            params = {brandID: obj.id, product: product, startDate: startDate, endDate: endDate, areaLike: keyName};//缓存取到该账号的id};
        } else {
            params = {brandID: obj.id, product: product, startDate: startDate1, endDate: endDate1, areaLike: keyName};//缓存取到该账号的id};
        }
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        if (startDate != "") {
            params = {kaID: obj.id, product: product, startDate: startDate, endDate: endDate, areaLike: keyName};
        }
        else if (startDate1 == "" || startDate1 == null) {
            params = {kaID: obj.id, product: product, startDate: startDate, endDate: endDate, areaLike: keyName};
        } else {
            params = {kaID: obj.id, product: product, startDate: startDate1, endDate: endDate1, areaLike: keyName};
        }
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectMonthSalesAmount',
        data: params,
        success: function (data) {
            if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                $(".monthH3").text("流水金额");
            } else {
                $(".monthH3").text("当月流水金额");
            }
            //查询总流水金额
            var salesTotalHtml = "";
            salesTotalHtml += '<span>' + data.data.salesAmount + '</span>';
            $(".salesTotal").html(salesTotalHtml);
            //查询已售产品
            var salesNumberHtml = "";
            if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            } else {
                salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            }
            $(".salesNumber").html(salesNumberHtml);
        }
    })
}
queryMonthTotal();
//城市列表
function queryProductList() {
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        if (startDate != "") {
            params = {brandID: obj.id, product: product, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
        }
        else if (startDate1 == "" || startDate1 == null) {
            params = {brandID: obj.id, product: product, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
        }
        else {
            params = {brandID: obj.id, product: product, startDate: startDate1, endDate: endDate1};//缓存取到该账号的id};
        }

    } else if (sessionStorage.roleType == 3) {//KA经理角色
        if (startDate != "") {
            params = {kaID: obj.id, product: product, startDate: startDate, endDate: endDate};
        } else if (startDate1 == "" || startDate1 == null) {
            params = {kaID: obj.id, product: product, startDate: startDate, endDate: endDate};
        } else {
            params = {kaID: obj.id, product: product, startDate: startDate1, endDate: endDate1};
        }
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectAreaList',
        data: params,
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-area="' + o.area + '"><div class="weui-cell__bd">' +
                        '<p>' + o.area + '</p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div><div class="weui-cell__ft"></div></a>';

                });
                $(".weui-cells-information").html(areaHtml);
                $(".weui-cell_access").click(function () {
                    var area = $(this).attr('data-area');
                    if (startDate1 == "" || startDate1 == null) {
                        window.location.href = '/rest/front/sell/toProductCityList' + '?product=' + product + '&area=' + area + "&startDate=" + startDate + "&endDate=" + endDate;
                    } else {
                        window.location.href = '/rest/front/sell/toProductCityList' + '?product=' + product + '&area=' + area + "&startDate=" + startDate1 + "&endDate=" + endDate1;
                    }
                })
            }
        }
    });
}
queryProductList();
