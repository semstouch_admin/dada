/**
 * Created by Administrator on 2017/9/2.
 */
//使用decodeURI()解码时
function GetRequest() {
    var url = decodeURI(decodeURI(location.search)); //获取url中"?"符后的字串，使用了两次decodeRUI解码
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
        return theRequest;
    }
}
var postData = GetRequest();
var storeName = postData.storeName;
var cityID = postData.cityId;//获取该活动的城市合伙人id
var storeID = postData.storeID;//获取该活动的门店id
var area = postData.areaName;//获取该活动的地区名称
var startDate1 = postData.startDate;//获取该活动的开始日期
var endDate1 = postData.endDate;//获取该活动的结束日期
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
$(".newTitle").text(storeName);//标题
$(function () {
    //判断角色
    if (sessionStorage.roleType == 2) {
        //查询品牌商总金额与销量
        queryBrandTotal();
        queryBrandMonthTotal();
        //查询品牌商商品列表
        queryBrand();

    } else if (sessionStorage.roleType == 3) {
        //查询KA经理总金额与销量
        queryBrandTotal();
        queryBrandMonthTotal();
        //查询KA经理商品列表
        queryKA();

    } else {
        //查询城市合伙人总金额与销量
        queryPartnerTotal();
        queryPartnerMonthTotal();
        //查询城市合伙人商品列表
        queryPartner();
    }
});
//点击时间的选择又一个页面
$("#showDatePicker").click(function () {
    $("#pageTwo").show();
    $("#pageOne").hide();
});
$("#timeLeft").click(function () {
    $("#pageTwo").hide();
    $("#pageOne").show();
});
var startDate = "",
    endDate = "";
$("#searchBtn").click(function () {
    startDate = $(".offline-starTime").val();
    endDate = $(".offline-endTime").val();
    if (startDate == '' || endDate == '') {
        weui.alert("开始时间或结束时间未选择")
    } else {
        $("#pageTwo").hide();
        $("#pageOne").show();
        $(".monthH3").attr('data-status', 1);
        if (sessionStorage.roleType == 2) {
            queryBrand();
            queryBrandTotal();
            queryBrandMonthTotal();
        } else if (sessionStorage.roleType == 3) {
            queryKA();
            queryBrandTotal();
            queryBrandMonthTotal();
        } else {
            queryPartner();
            queryPartnerTotal();
            queryPartnerMonthTotal();
        }
    }
});
//搜索框模糊查询
var keyName = "";
$('.key-word').keyup(function () {
    keyName = $(this).val();
    if (sessionStorage.roleType == 2 || sessionStorage.roleType == 3) {
        queryBrandTotal();
        queryBrandMonthTotal();
    } else {
        queryPartnerTotal();
        queryPartnerMonthTotal();
    }
});
$('.key-word').blur(function () {
    keyName = $(this).val();
    if (sessionStorage.roleType == 2 || sessionStorage.roleType == 3) {
        queryBrandTotal();
        queryBrandMonthTotal();
    } else {
        queryPartnerTotal();
        queryPartnerMonthTotal();
    }
});

//查询品牌商总金额与销量
function queryBrandTotal() {
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        params = {
            brandID: obj.id,
            area: area,
            storeID: storeID,
            cityID: cityID,
            productLike: keyName
        };//缓存取到该账号的id};
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        params = {
            kaID: obj.id,
            area: area,
            storeID: storeID,
            cityID: cityID,
            productLike: keyName
        };
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectSalesAmount',
        data: params,
        success: function (data) {
            //查询总流水金额
            var salesAreaTotalHtml = "";
            salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '</span>';
            $(".salesAreaTotal").html(salesAreaTotalHtml);
            //查询总销量产品
            var salesAreaNumberHtml = "";
            salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
            $(".salesAreaNumber").html(salesAreaNumberHtml);
        }
    })
}
//查询品牌商总金额与销量
function queryBrandMonthTotal() {
    var params = "";
    if (sessionStorage.roleType == 2) {//品牌商角色
        if (startDate != "") {
            params = {
                brandID: obj.id,
                area: area,
                storeID: storeID,
                cityID: cityID,
                startDate: startDate,
                endDate: endDate,
                productLike: keyName
            };//缓存取到该账号的id};
        } else if (startDate1 == "" || startDate1 == null) {//品牌商角色
            params = {
                brandID: obj.id,
                area: area,
                storeID: storeID,
                cityID: cityID,
                startDate: startDate,
                endDate: endDate,
                productLike: keyName
            };//缓存取到该账号的id};
        } else {
            params = {
                brandID: obj.id,
                area: area,
                storeID: storeID,
                cityID: cityID,
                startDate: startDate1,
                endDate: endDate1,
                productLike: keyName
            };//缓存取到该账号的id};
        }
    } else if (sessionStorage.roleType == 3) {//KA经理角色
        if (startDate != "") {
            params = {
                kaID: obj.id,
                area: area,
                storeID: storeID,
                cityID: cityID,
                startDate: startDate,
                endDate: endDate,
                productLike: keyName
            };
        } else if (startDate1 == "" || startDate1 == null) {//品牌商角色
            params = {
                kaID: obj.id,
                area: area,
                storeID: storeID,
                cityID: cityID,
                startDate: startDate,
                endDate: endDate,
                productLike: keyName
            };
        } else {
            params = {
                kaID: obj.id,
                area: area,
                storeID: storeID,
                cityID: cityID,
                startDate: startDate1,
                endDate: endDate1,
                productLike: keyName
            };
        }
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectMonthSalesAmount',
        data: params,
        success: function (data) {
            if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                $(".monthH3").text("流水金额");
            } else {
                $(".monthH3").text("当月流水金额");
            }
            //查询总流水金额
            var salesTotalHtml = "";
            salesTotalHtml += '<span>' + data.data.salesAmount + '</span>';
            $(".salesTotal").html(salesTotalHtml);
            //查询已售产品
            var salesNumberHtml = "";
            if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            } else {
                salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            }
            $(".salesNumber").html(salesNumberHtml);
        }
    })
}

function queryBrand() {
    if (startDate != "") {
        params = {
            brandID: obj.id,
            area: area,
            storeID: storeID,
            cityID: cityID,
            startDate: startDate,
            endDate: endDate
        };//缓存取到该账号的id};
    } else if (startDate1 == "" || startDate1 == null) {//品牌商角色
        params = {
            brandID: obj.id,
            area: area,
            storeID: storeID,
            cityID: cityID,
            startDate: startDate,
            endDate: endDate
        };//缓存取到该账号的id};
    } else {
        params = {
            brandID: obj.id,
            area: area,
            storeID: storeID,
            cityID: cityID,
            startDate: startDate1,
            endDate: endDate1
        };//缓存取到该账号的id};
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectProductList',
        data: params,
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {

                    areaHtml += '<a class="weui-cell weui-cell_access" href="#"><div class="weui-cell__bd">' +
                        '<p>' + o.product + '<span style="margin-left: 6px;color: #999;font-size: 14px;">(条形码：' + o.barCode + ')</span></p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div></a>';

                });
                $(".weui-cells-information").html(areaHtml);
            }
        }
    })
}
function queryKA() {
    if (startDate != "") {
        params = {kaID: obj.id, area: area, storeID: storeID, cityID: cityID, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
    } else if (startDate1 == "" || startDate1 == null) {//品牌商角色
        params = {kaID: obj.id, area: area, storeID: storeID, cityID: cityID, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
    } else {
        params = {kaID: obj.id, area: area, storeID: storeID, cityID: cityID, startDate: startDate1, endDate: endDate1};//缓存取到该账号的id};
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectProductList',
        data: params,

        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#"><div class="weui-cell__bd">' +
                        '<p>' + o.product + '<span style="margin-left: 6px;color: #999;font-size: 14px;">(条形码：' + o.barCode + ')</span></p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div></a>';

                });
                $(".weui-cells-information").html(areaHtml);
            }
        }
    })
}
//查询城市合伙人总金额与销量
function queryPartnerTotal() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectSalesAmount',
        data: {
            storeID: storeID,
            cityID: obj.id,//缓存取到该账号的id
            productLike: keyName
        },
        success: function (data) {
            //查询总流水金额
            var salesAreaTotalHtml = "";
            salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '</span>';
            $(".salesAreaTotal").html(salesAreaTotalHtml);
            //查询总销量产品
            var salesAreaNumberHtml = "";
            salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
            $(".salesAreaNumber").html(salesAreaNumberHtml);
        }
    })
}
//查询城市合伙人总金额与销量
function queryPartnerMonthTotal() {
    if (startDate != "") {
        params = {cityID: obj.id, productLike: keyName, storeID: storeID, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
    } else if (startDate1 == "" || startDate1 == null) {//品牌商角色
        params = {cityID: obj.id, productLike: keyName, storeID: storeID, startDate: startDate, endDate: endDate};//缓存取到该账号的id};
    } else {
        params = {cityID: obj.id, productLike: keyName, storeID: storeID, startDate: startDate1, endDate: endDate1};//缓存取到该账号的id};
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectMonthSalesAmount',
        data: params,
        success: function (data) {
            if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                $(".monthH3").text("流水金额");
            } else {
                $(".monthH3").text("当月流水金额");
            }
            //查询总流水金额
            var salesTotalHtml = "";
            salesTotalHtml += '<span>' + data.data.salesAmount + '</span>';
            $(".salesTotal").html(salesTotalHtml);
            //查询已售产品
            var salesNumberHtml = "";
            if ($(".monthH3").attr('data-status') == 1 || startDate1 != "" || startDate1 != null) {
                salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            } else {
                salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            }
            $(".salesNumber").html(salesNumberHtml);
        }
    })
}
function queryPartner() {
    if (startDate != "") {
        params = {cityID: obj.id, storeID: storeID, startDate: startDate, endDate:endDate};//缓存取到该账号的id};
    } else if (startDate1 == "" || startDate1 == null) {//品牌商角色
        params = {cityID: obj.id, storeID: storeID, startDate: startDate, endDate:endDate};//缓存取到该账号的id};
    } else {
        params = {cityID: obj.id, storeID: storeID, startDate: startDate1, endDate: endDate1};//缓存取到该账号的id};
    }
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectProductList',
        data: params,
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#"><div class="weui-cell__bd">' +
                        '<p>' + o.product + '<span style="margin-left: 6px;color: #999;font-size: 14px;">(条形码：' + o.barCode + ')</span></p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div></a>';

                });
                $(".weui-cells-information").html(areaHtml);
            }
        }
    })
}
//点击我的商品按钮到全部商品页面
$("#goodsBtn").click(function () {
    window.location.href = "/rest/front/sell/toAllProductList";
});
