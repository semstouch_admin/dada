////当天的日期
//Date.prototype.format = function (format) {
//    var args = {
//        "M+": this.getMonth() + 1,
//        "d+": this.getDate(),
//        "h+": this.getHours(),
//        "m+": this.getMinutes(),
//        "s+": this.getSeconds(),
//        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
//        "S": this.getMilliseconds()
//    };
//    if (/(y+)/.test(format))
//        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
//    for (var i in args) {
//        var n = args[i];
//        if (new RegExp("(" + i + ")").test(format))
//            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length));
//    }
//    return format;
//};
//var nowDate = new Date().format("yyyy-MM-dd hh:mm");

var obj = "";
//点击时间的选择又一个页面
$("#showDatePicker").click(function () {
    $("#pageTwo").show();
    $("#pageOne").hide();
});
$("#timeLeft").click(function () {
    $("#pageTwo").hide();
    $("#pageOne").show();
});
var startDate = "",
    endDate = "";
var keyName = "";
$.ajax({
    type: 'GET',
    dataType: 'json',
    url: '/rest/front/activity/getSession',
    success: function (data) {
        sessionStorage.id = data.data.user.id;
        var str = JSON.stringify(data.data.user);
        sessionStorage.obj = str;
        sessionStorage.roleType = data.data.user.rid;
        obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
        //判断角色
        if (sessionStorage.roleType == 2) {
            //查询品牌商总金额与销量
            queryBrandTotal();
            queryBrandMonthTotal();
            //带日期条件查询品牌商地区列表
            queryBrand()
        } else {
            //查询KA经理总金额与销量
            queryKATotal();
            queryKAMonthTotal();
            //带日期条件查询KA经理地区列
            queryKA();
        }
        $("#searchBtn").click(function () {
            startDate = $(".offline-starTime").val();
            endDate = $(".offline-endTime").val();
            if (startDate == '' || endDate == '') {
                weui.alert("开始时间或结束时间未选择")
            } else {
                $("#pageTwo").hide();
                $("#pageOne").show();
                if (sessionStorage.roleType == 2) {
                    queryBrand();
                    $(".monthH3").attr('data-status', 1);
                    queryBrandMonthTotal();
                    queryBrandTotal();
                } else {
                    queryKA();
                    $(".monthH3").attr('data-status', 1);
                    queryKAMonthTotal();
                    queryKATotal();
                }
            }
        });
        //搜索框模糊查询
        $('.key-word').keyup(function () {
            keyName = $(this).val();
            if (sessionStorage.roleType == 2) {
                queryBrandTotal();
                queryBrandMonthTotal();
            } else {
                queryKATotal();
                queryKAMonthTotal();
            }
        });
        $('.key-word').blur(function () {
            keyName = $(this).val();
            if (sessionStorage.roleType == 2) {
                queryBrandTotal();
                queryBrandMonthTotal();
            } else {
                queryKATotal();
                queryKAMonthTotal();
            }
        });

    }
});


//查询品牌商总金额与销量
function queryBrandTotal() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectSalesAmount',
        data: {
            brandID: obj.id,//缓存取到该账号的id
            //startDate: startDate,
            //endDate: endDate,
            areaLike: keyName
        },
        success: function (data) {
            //查询总流水金额
            var salesAreaTotalHtml = "";
            salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '元</span>';
            $(".salesAreaTotal").html(salesAreaTotalHtml);
            //查询总销量产品
            var salesAreaNumberHtml = "";
            salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
            $(".salesAreaNumber").html(salesAreaNumberHtml);
        }
    })
}
//查询品牌商总金额与销量
function queryBrandMonthTotal() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectMonthSalesAmount',
        data: {
            brandID: obj.id,//缓存取到该账号的id
            startDate: startDate,
            endDate: endDate,
            areaLike: keyName
        },
        success: function (data) {
            //查询总流水金额
            if ($(".monthH3").attr('data-status') == 1) {
                $(".monthH3").text("流水金额");
            } else {
                $(".monthH3").text("当月流水金额");
            }
            var salesTotalHtml = "";
            salesTotalHtml += '<span>' + data.data.salesAmount + '</span>';
            $(".salesTotal").html(salesTotalHtml);
            //查询已售产品
            var salesNumberHtml = "";
            if ($(".monthH3").attr('data-status') == 1) {
                salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            } else {
                salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            }
            $(".salesNumber").html(salesNumberHtml);
        }
    })
}
//查询KA经理总金额与销量
function queryKATotal() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectSalesAmount',
        data: {
            kaID: obj.id,//缓存取到该账号的id
            //startDate: startDate,
            //endDate: endDate,
            areaLike: keyName
        },
        success: function (data) {
            //查询总流水金额
            var salesAreaTotalHtml = "";
            salesAreaTotalHtml += '<span>总流水金额：' + data.data.salesAmount + '元</span>';
            $(".salesAreaTotal").html(salesAreaTotalHtml);
            //查询总销量产品
            var salesAreaNumberHtml = "";
            salesAreaNumberHtml += '<span>总销售量：' + data.data.salesVolume + '件</span>';
            $(".salesAreaNumber").html(salesAreaNumberHtml);
        }
    })
}
function queryKAMonthTotal() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectMonthSalesAmount',
        data: {
            kaID: obj.id,//缓存取到该账号的id
            startDate: startDate,
            endDate: endDate,
            areaLike: keyName
        },
        success: function (data) {
            //查询总流水金额
            if ($(".monthH3").attr('data-status') == 1) {
                $(".monthH3").text("流水金额");
            } else {
                $(".monthH3").text("当月流水金额");
            }
            //查询总流水金额
            var salesTotalHtml = "";
            salesTotalHtml += '<span>' + data.data.salesAmount + '</span>';
            $(".salesTotal").html(salesTotalHtml);
            //查询已售产品
            var salesNumberHtml = "";
            if ($(".monthH3").attr('data-status') == 1) {
                salesNumberHtml += '销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            } else {
                salesNumberHtml += '当月销售量：<span class="num-span">' + data.data.salesVolume + '</span>件';
            }
            $(".salesNumber").html(salesNumberHtml);
        }
    })
}
//查询品牌商地区列表
function queryBrand() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectAreaList',
        data: {
            brandID: obj.id,//缓存取到该账号的id
            startDate: startDate,
            endDate: endDate
        },
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-area="' + o.area + '"><div class="weui-cell__bd">' +
                        '<p>' + o.area + '</p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div><div class="weui-cell__ft"></div></a>';
                });
                $(".weui-cells-information").html(areaHtml);
                $(".weui-cell_access").click(function () {
                    var area = $(this).attr('data-area');
                    var url = encodeURI($(".weui-cells-information").attr('data-url') + "?area=" + area+"&startDate="+startDate+"&endDate="+endDate);
                    var enurl = encodeURI(url);//使用了两次encodeRUI进行编码
                    window.location.href = enurl;
                })
            }
        }
    });
}


//查询KA经理地区列表
function queryKA() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/sell/selectAreaList',
        data: {
            kaID: obj.id,//缓存取到该账号的id
            startDate: startDate,
            endDate: endDate
        },
        success: function (data) {
            var areaList = data.data;
            if (areaList == null || areaList == '') {
                $('.img-div').show();
            } else {
                $('.img-div').hide();
                var areaHtml = "";
                $.each(areaList, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-area="' + o.area + '"><div class="weui-cell__bd">' +
                        '<p>' + o.area + '</p><p>销售金额：' + o.salesAmount + ' &nbsp;&nbsp;销售数量：' + o.salesVolume + '件</p>' +
                        '</div><div class="weui-cell__ft"></div></a>';
                });
                $(".weui-cells-information").html(areaHtml);
                $(".weui-cell_access").click(function () {
                    var area = $(this).attr('data-area');
                    var url = encodeURI($(".weui-cells-information").attr('data-url') + "?area=" + area+"&startDate="+startDate+"&endDate="+endDate);
                    var enurl = encodeURI(url);//使用了两次encodeRUI进行编码
                    window.location.href = enurl;
                })
            }
        }
    })
}
//点击我的商品按钮到全部商品页面
$("#goodsBtn").click(function () {
    window.location.href = "/rest/front/sell/toAllProductList";
});