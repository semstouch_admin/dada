/**
 * Created by Administrator on 2017/9/1.
 */
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var activityID = getQueryString("id");
var cityID = getQueryString("cityID");
var status = getQueryString("status");
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
//品牌商
    if (status == 1) {
        //未开始的活动活动查询店铺列表
        queryBrandStore1();
    } else {
        //正在进行中和已经结束的活动查询店铺列表
        queryBrandStore2();
    }


function queryBrandStore1() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectStoreList",
        data: {
            activityID: activityID,
            cityID: cityID
        },
        dataType: "json",
        success: function (data) {
            var area = data.data;
            var areaHtml = '';
            $.each(area, function (i, o) {
                areaHtml += '<a class="weui-cell weui-cell_access" href="#"><div class="weui-cell__bd">' +
                    '<p>' + o.storeName+ '</p></div></a>';
            });
            $(".area-list").html(areaHtml);
        }
    })
}
function queryBrandStore2() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectStoreList",
        data: {
            activityID: activityID,
            cityID: cityID
        },
        dataType: "json",
        success: function (data) {
            var area = data.data;
            var areaHtml = '';
            $.each(area, function (i, o) {
                areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-id="' + o.storeID+ '"><div class="weui-cell__bd">' +
                    '<p>' + o.storeName + '</p></div> <div class="weui-cell__ft"></div></a>';
            });
            $(".area-list").html(areaHtml);
            //点击列表跳转到查看活动图片页面
            $(".weui-cell_access").click(function () {
            var id = activityID;
            var storeID = $(this).attr('data-id');
            var url =$(".area-list").attr('data-url') + "?id=" + id + "&cityID=" + cityID+"&storeID="+storeID+ "&status=" + status;
            window.location.href = url;
            })
        }
    })
}
