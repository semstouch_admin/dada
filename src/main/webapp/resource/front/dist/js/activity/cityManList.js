/**
 * Created by Administrator on 2017/8/31.
 */
//使用decodeURI()解码时
function GetRequest() {
    var url = decodeURI(decodeURI(location.search)); //获取url中"?"符后的字串，使用了两次decodeRUI解码
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
        return theRequest;
    }
}
var postData = GetRequest();
var activityID = postData.id;//获取该活动的id
var area = postData.area;//获取该活动的地区选择
var insertStatus = postData.insertStatus;//获取该活动的地区选择
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
$(function () {
    //判断角色
    if (sessionStorage.roleType == 2) {
        queryBrandMan();
    } else if (sessionStorage.roleType == 3) {
        queryKaMan();
    }

});
function queryBrandMan() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectCityList",
        data: {
            activityID: activityID,
            area: area
        },
        dataType: "json",
        success: function (data) {
            var area = data.data;
            if (area == null || area == '') {
                $('.img-div').show();
            } else {
                var areaHtml = '';
                $.each(area, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" data-id="' + o.cityID + '"><div class="weui-cell__bd">' +
                        '<p>' + o.cityName + '</p></div><div class="weui-cell__ft">' + o.cityNumber + '家</div></a>';
                });
                $(".area-list").html(areaHtml);
                $(".weui-cell_access").click(function () {
                    var id = activityID;
                    var cityID = $(".weui-cell_access").attr('data-id');
                    var url = $(".area-list").attr('data-url') + "?id=" + id + "&cityID=" + cityID + "&insertStatus=" + insertStatus;
                    window.location.href = url;
                })

            }

        }
    })
}
function queryKaMan() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectCityList",
        data: {
            activityID: activityID,
            area: area
        },
        dataType: "json",
        success: function (data) {
            var area = data.data;
            if (area == null || area == '') {
                $('.img-div').show();
            } else {
                var areaHtml = '';
                $.each(area, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" data-id="' + o.cityID + '"><div class="weui-cell__bd">' +
                        '<p>' + o.cityName + '</p></div><div class="weui-cell__ft">' + o.cityNumber + '家</div></a>';
                });
                $(".area-list").html(areaHtml);
                $(".weui-cell_access").click(function () {
                    var id = activityID;
                    var status = insertStatus;
                    var cityID = $(".weui-cell_access").attr('data-id');
                    var url = $(".area-list").attr('data-kaUrl') + "?id=" + id + "&cityID=" + cityID + "&insertStatus=" + insertStatus;
                    window.location.href = url;
                })
            }
            }
        })
}