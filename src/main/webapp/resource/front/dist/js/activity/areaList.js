function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var activityID = getQueryString("id");//获取该活动的id
var insertStatus = getQueryString("insertStatus");//获取该活动的id
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
$(function () {
    //判断角色
    queryBrandArea();

});
function queryBrandArea() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectAreaList",
        data: {
            activityID: activityID
        },
        dataType: "json",
        success: function (data) {
            var area = data.data;
            if (area == null || area == '') {
                if(sessionStorage.roleType==4){
                    $('.img-div').show();
                }else{
                    $('.KaImg-div').show();
                }
            } else {
                var areaHtml = '';
                $.each(area, function (i, o) {
                    areaHtml += '<a class="weui-cell weui-cell_access" href="#" data-area="' + o.area + '"><div class="weui-cell__bd">' +
                        '<p>' + o.area + '</p></div><div class="weui-cell__ft">' + o.areaNumber + '家</div></a>';
                });

                $(".area-list").html(areaHtml);
            }
            $(".weui-cell_access").click(function () {
                var id = activityID;
                var area = $(this).attr('data-area');
                var url = encodeURI($(".area-list").attr('data-url') + "?id=" + id + "&area=" + area + "&insertStatus=" + insertStatus);
                var enurl = encodeURI(url);//使用了两次encodeRUI进行编码
                window.location.href = enurl;
            })


        }
    })
}