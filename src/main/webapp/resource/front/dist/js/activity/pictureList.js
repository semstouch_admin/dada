/**
 * Created by Administrator on 2017/9/1.
 */
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var activityID = getQueryString("id");
var cityID = getQueryString("city");
var storeID = getQueryString("storeID");
var insertStatus = getQueryString("insertStatus");
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
//if (insertStatus == "2" && sessionStorage.roleType == 4 || insertStatus == "1" && sessionStorage.roleType == 4) {
    $(".pull-right").show();
    $(".pull-left").click(function () {
        var id = activityID;
        var url = $(this).attr('data-url') + "?id=" + id + "&insertStatus=" + insertStatus;
        window.location.href = url;
    });
    $(".pull-right").click(function () {
        var id = activityID;
        var cityId = cityID;
        var storeId = storeID;
        var url = $(".pull-right").attr('data-url') + "?id=" + id + "&cityId=" + cityId + "&storeId=" + storeId + "&insertStatus=" + insertStatus;
        window.location.href = url;
    });
//}
function queryBrandPicture() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitypicture/selectPictureList",
        data: {
            activityID: activityID,
            cityID: cityID,
            storeID: storeID
        },
        dataType: "json",
        success: function (data) {
            var picture = data.data,
                pictureHtml = "";
            if (picture == null || picture == '') {
                if(sessionStorage.roleType == 4){
                    $('.img-div').show();
                }else if(sessionStorage.roleType == 3){
                    $('.kaImg-div').show();
                }

            } else {
                $.each(picture, function (i, o) {
                    pictureHtml += ' <div class="weui-cell"><div class="weui-cell__bd"><div class="weui-uploader"><div class="weui-uploader__hd">' +
                        '<p class="time-p weui-uploader__title">' + o.createTime + '</p></div>';
                    if (o.picture != '' && o.picture != null) {
                        var imgArr = o.picture.split(",");
                        pictureHtml += '<div class="weui-uploader__bd"><ul class="weui-uploader__files ni" id="uploaderFiles">';
                        for (var j = 0; j < imgArr.length; j++) {
                            pictureHtml += '<li class="weui-uploader__file" style="background-image:url(/' + imgArr[j] + ')"></li>';
                        }
                        pictureHtml += '</ul></div>';
                    } else {
                        pictureHtml += '';
                    }
                    pictureHtml += '<div class="weui-uploader__hd">' +
                        '<p class="weui-uploader__title">' + o.location + '</p></div></div></div></div>';
                });
                $(".weui-cells_formList").html(pictureHtml);
                var $uploaderFiles = $(".ni");
                var $gallery = $("#gallery");
                var $galleryImg = $("#galleryImg");
                $uploaderFiles.on("click", "li", function () {
                    $galleryImg.attr("style", this.getAttribute("style"));
                    $gallery.fadeIn(100);
                });
                $gallery.on("click", function () {
                    $gallery.fadeOut(100);
                });
            }
        }
    })
}
queryBrandPicture();
