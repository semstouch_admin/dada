/**
 * Created by Administrator on 2017/8/31.
 */
$("#addBtn").click(function () {
    var name = $(".offline-name").val(),
        kaName = $(".offline-name").val(),
        demand = $(".offline-demand").val(),
        kaDemand = $(".offline-demand").val(),
        starTime = $(".offline-starTime").val(),
        kaStarTime = $(".offline-starTime").val(),
        endTime = $(".offline-endTime").val(),
        kaEndTime = $(".offline-endTime").val(),
        budge = $(".offline-budge").val(),
        kaBudge = $(".offline-budge").val(),
        picture = $(".picture").val(),
        kaPicture = $(".picture").val();
    var productName = '';
    var kaProductName = '';
    var productCode = '';
    var kaProductCode = '';
    $(".product-name").each(function () {
        productName += $(this).val() + ",";
        kaProductName += $(this).val() + ",";
    });
    productName = productName.substr(0, productName.length - 1);
    kaProductName = kaProductName.substr(0, kaProductName.length - 1);
    $(".img-div").children('img').each(function () {
        productCode += ($(this).attr('src').substring(1)) + ",";
        kaProductCode += ($(this).attr('src').substring(1)) + ",";

    });
    productCode = productCode.substr(0, productCode.length - 1);
    kaProductCode = kaProductCode.substr(0, kaProductCode.length - 1);
    var time=Date.parse(new Date());
    var time1 = Date.parse(new Date(starTime));
    var time2 = Date.parse(new Date(endTime));
    if (name == '' || kaName == '' || demand == '' || kaDemand == '' || starTime == '' || kaStarTime == '' || endTime == '' || kaEndTime == '' || budge == '' || kaBudge == '' || picture == '' || kaPicture == '' || productName == '' || kaProductName == '' || productCode == '' || kaProductCode == '' || productCode == '' || kaProductCode == '') {
        //信息填写不完整提示
        $("#dialogInformation").css("display", "block");
        $(".weui-dialog__btn").click(function () {
            $("#dialogInformation").css("display", "none");
        })
    } else if (time1 <= time || time2 <= time ) {
        //选择日期不正确
        $("#dialogSTime").css("display", "block");
        $(".weui-dialog__btn").click(function () {
            $("#dialogSTime").css("display", "none");
        });
    } else if (time2 < time1) {
        //结束日期要大于开始日期
        $("#dialogTime").css("display", "block");
        $(".weui-dialog__btn").click(function () {
            $("#dialogTime").css("display", "none");
        })
    } else {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/rest/front/activity/insertActivity',
            data: {
                name: name,
                kaName: kaName,
                demand: demand,
                kaDemand: kaDemand,
                starTime: starTime,
                kaStarTime: kaStarTime,
                endTime: endTime,
                kaEndTime: kaEndTime,
                budge: budge,
                kaBudge: kaBudge,
                picture: picture,
                kaPicture: kaPicture,
                productName: productName,
                kaProductName: kaProductName,
                kaProductCode:kaProductCode,
                productCode:productCode
            },
            success: function (data) {
                if (data.success) {
                    var $toast = $('#toast');
                    $toast.fadeIn();
                    setTimeout(function () {
                        $toast.fadeOut();
                        window.location.href = $('#addBtn').attr('data-url');
                    }, 1000);
                } else {
                    weui.alert(data.message);
                }
            }
        });
    }

});


//微信js sdk接口获取gps坐标
$.ajax({
    type: 'GET',
    dataType: 'json',
    url: '/rest/front/activity/toScan',
    data: {
        pageUrl: location.href.split('#')[0]
    },
    success: function (res) {
        if (res.success) {
            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                appId: res.data.appId, // 必填，公众号的唯一标识
                timestamp: res.data.timestamp, // 必填，生成签名的时间戳
                nonceStr: res.data.noncestr, // 必填，生成签名的随机串
                signature: res.data.signature,// 必填，签名，见附录1
                jsApiList: ['getLocation'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
            });
            wx.ready(function () {
                wx.getLocation({
                    type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
                    success: function (res) {
                        var lat = res.latitude; // 纬度，浮点数，范围为90 ~ -90
                        var lng = res.longitude; // 经度，浮点数，范围为180 ~ -180。
                        getLocationAddress(lat, lng);
                    }
                });
            });
        } else {
            weui.alert(res.message);
        }
    }
});

/**
 * 调用后台将gps坐标转化成物理地址
 * @param lat
 * @param lng
 */
function getLocationAddress(lat, lng) {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/activity/getLocationAddress',
        data: {
            lat: lat,
            lng: lng
        },
        success: function (res) {
            if (res.success) {
                $("#address").html(res.data);
            }
        }
    });
}