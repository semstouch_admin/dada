function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var id = getQueryString("activityID");
var status1 = getQueryString("status");
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
//已经选择时，查看被选择城市合伙人列表以及去编辑城市合伙人
$.ajax({
    type: "GET",
    url: "/rest/front/activityjoin/selectCityChoose",
    dataType: "json",
    data: {
        activityID: id,
        kaID: obj.id
    },
    success: function (data) {
        var manList = data.data;
        if (manList == null || manList == '') {
            $('.img-div').show();
        } else {
            var manHtml = "";
            $.each(manList, function (i, o) {
                i++;
                manHtml += '<label class="weui-cell weui-check__label" for="' + i + '" ><div class="weui-cell__hd"><input type="checkbox" class="weui-check" name="choose" id="' + i + '" data-id="' + o.cityID + '" checked/> <i class="weui-icon-checked"></i> </div>' +
                    '<div class="weui-cell__bd"> <p>' + o.cityName + '</p> </div> </label>';
            });
        }
        $(".weui-cells_checkbox").html(manHtml);
    }
});
$(".pull-right").click(function () {
    var url = $(this).attr('data-url');
    var activityID = id;
    var status = status1;
    window.location.href = url + "?activityID=" + activityID + "&status=" + status;
});