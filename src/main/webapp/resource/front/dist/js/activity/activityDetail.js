/**
 * Created by Administrator on 2017/4/17.
 */
/**活动详情页面*/
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}

var id = getQueryString("id");//取活动列表的活动id的值
var insertStatus = getQueryString("status");//取该活动状态的值
var updateStatus = getQueryString("updateStatus");//取该活动状态的值
var acStatus = getQueryString("acStatus");//取该活动acStatus的值
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
//品牌商的活动详情
if (sessionStorage.roleType == 2) {
    $("#beginDetail").show();
    queryNewsDetail();
    //带id到查看城市列表页面
    $("#beginDetail").click(function () {
        var id = $(".swiper-slide").attr('data-id');
        var url = $("#beginDetail").attr('data-url');
        var status = insertStatus;
        url = url + "?id=" + id + "&status=" + status;
        window.location.href = url;
    });
    if (insertStatus == 1) {
        //活动筹备中的活动详情页面
        $("#detailBrand").text("查看参与活动门店数量");

    } else {//活动进行中与已结束的活动详情页面
        $("#detailBrand").text("查看活动照片");
    }
} else if (sessionStorage.roleType == 3) { //KA经理的活动详情页面
    queryKADetail();
    if (insertStatus == 1) {//活动筹备中
        if (updateStatus == "0") {
            $("#KAProgress").show();
        } else {
            $("#KAProgress").show();
            $("#kaDetail").show();
        }
    } else if (insertStatus == 2) {//活动进行中
        $("#KAProgress").show();
        $("#kaDetail").show();
    } else {//活动已结束
        $("#kaDetail").show();
    }
    //带id到编辑活动列表页面
    $("#KAProgress").click(function () {
        var id = $(".swiper-slide").attr('data-id');
        var url = $(this).attr('data-url');
        url = url + "?id=" + id + "&insertStatus=" + insertStatus+"&updateStatus="+updateStatus;
        window.location.href = url;
    });
    //带id到查看城市列表页面
    $("#kaDetail").click(function () {
        var id = $(".swiper-slide").attr('data-id');
        var url = $("#kaDetail").attr('data-url');
        url = url + "?id=" + id+ "&insertStatus=" + insertStatus;
        window.location.href = url;
    });
} else if (sessionStorage.roleType == 4) {//城市合伙人的活动详情页面

    queryPartnerDetail();

}
/**品牌商活动详情页面*/
function queryNewsDetail() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activity/selectActivityDetail",
        data: {
            id: id
        },
        dataType: "json",
        success: function (data) {
            var offline = data.data;
            //详情的的轮播图
            var imgHtml = "";
            var imgArr = offline.picture.split(",");
            for (var i = 0; i < imgArr.length; i++) {
                imgHtml += '<div class="swiper-slide" data-id="' + offline.id + '">';
                imgHtml += '<img src="/' + imgArr[i] + '">';
                imgHtml += '</div>';
            }
            $(".swiper-wrapper").html(imgHtml);
            var mySwiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                autoplay: 2000,
                loop: true,
                paginationClickable: true,
                onImagesReady: function (swiper) {
                    var obj = $('.swiper-slide').find('img');
                    var max;
                    var heightList = new Array;
                    for (var i = 0; i < obj.length; i++) {
                        heightList[i] = $(obj[i]).height()
                    }
                    max = Math.max.apply(null, heightList);
                    $('.swiper-slide').height(max);
                    $('.swiper-wrapper').height(max);
                }
            });
            /**首页的详情页面内容部分**/
            $(".offline-name").val(offline.name);
            $(".offline-demand").val(offline.demand);
            $(".offline-starTime").val(offline.starTime);
            $(".offline-endTime").val(offline.endTime);
            $(".offline-budge").val(offline.budge);
            var product = data.data.productName.split(",");
            var imgArr1 = data.data.productCode.split(",");
            var productHtml = "";
            for (var j = 0; j < product.length; j++) {
                productHtml += '<div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">商品名称</label></div>' +
                    '<div class="weui-cell__bd"><input class="weui-input personnelBtn" type="text" value="' + product[j] + '" readonly="readonly">' +
                    '</div></div>';
                productHtml += ' <div class="weui-cell"><div class="weui-cell__bd" style="margin:0;"> <div class="weui-uploader"> <div class="weui-uploader__hd"> <p class="weui-uploader__title">商品条码 </p> </div> <div class="weui-uploader__bd"> <ul class="card-content-inner fileCode"> <li id="img-div"><img src="/' + imgArr1[j] + '"></li> </ul> </div> </div> </div> </div>'
            }
            $(".product-weui-cell").html(productHtml);
        }
    })
}
/**ka经理活动详情页面*/
function queryKADetail() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activity/selectActivityDetail",
        data: {
            id: id
        },
        dataType: "json",
        success: function (data) {
            var offline = data.data;
            //详情的的轮播图
            var imgHtml = "";
            var imgArr = offline.kaPicture.split(",");
            for (var i = 0; i < imgArr.length; i++) {
                imgHtml += '<div class="swiper-slide" data-id="' + offline.id + '">';
                imgHtml += '<img src="/' + imgArr[i] + '">';
                imgHtml += '</div>';
            }
            $(".swiper-wrapper").html(imgHtml);
            var mySwiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                autoplay: 2000,
                loop: true,
                paginationClickable: true,
                onImagesReady: function (swiper) {
                    var obj = $('.swiper-slide').find('img');
                    var max;
                    var heightList = new Array;
                    for (var i = 0; i < obj.length; i++) {
                        heightList[i] = $(obj[i]).height()
                    }
                    max = Math.max.apply(null, heightList);
                    $('.swiper-slide').height(max);
                    $('.swiper-wrapper').height(max);
                }
            });
            /**首页的详情页面内容部分**/
            $(".offline-name").val(offline.kaName);
            $(".offline-demand").val(offline.kaDemand);
            $(".offline-starTime").val(offline.kaStarTime);
            $(".offline-endTime").val(offline.kaEndTime);
            $(".offline-budge").val(offline.kaBudge);
            var product = data.data.kaProductName.split(",");
            var imgArr1 = data.data.kaProductCode.split(",");
            var productHtml = "";
            for (var j = 0; j < product.length; j++) {
                productHtml += '<div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">商品名称</label></div>' +
                    '<div class="weui-cell__bd"><input class="weui-input personnelBtn" type="text" value="' + product[j] + '" readonly="readonly">' +
                    '</div></div>';
                productHtml += ' <div class="weui-cell"><div class="weui-cell__bd" style="margin:0;"> <div class="weui-uploader"> <div class="weui-uploader__hd"> <p class="weui-uploader__title">商品条码 </p> </div> <div class="weui-uploader__bd"> <ul class="card-content-inner fileCode"> <li id="img-div"><img src="/' + imgArr1[j] + '"></li> </ul> </div> </div> </div> </div>'
            }
            $(".product-weui-cell").html(productHtml);
        }
    })
}
/**城市合伙人活动详情页面*/
function queryPartnerDetail() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activity/selectCityActivityDetail",
        data: {
            id: id
        },
        dataType: "json",
        success: function (data) {
            var offline = data.data;
            //详情的的轮播图
            var imgHtml = "";
            var imgArr = offline.kaPicture.split(",");
            for (var i = 0; i < imgArr.length; i++) {
                imgHtml += '<div class="swiper-slide" data-id="' + offline.id + '">';
                imgHtml += '<img src="/' + imgArr[i] + '">';
                imgHtml += '</div>';
            }
            $(".swiper-wrapper").html(imgHtml);
            var mySwiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                autoplay: 2000,
                loop: true,
                paginationClickable: true,
                onImagesReady: function (swiper) {
                    var obj = $('.swiper-slide').find('img');
                    var max;
                    var heightList = new Array;
                    for (var i = 0; i < obj.length; i++) {
                        heightList[i] = $(obj[i]).height()
                    }
                    max = Math.max.apply(null, heightList);
                    $('.swiper-slide').height(max);
                    $('.swiper-wrapper').height(max);
                }
            });
            /**首页的详情页面内容部分**/
            $(".offline-name").val(offline.kaName);
            $(".offline-demand").val(offline.kaDemand);
            $(".offline-starTime").val(offline.kaStarTime);
            $(".offline-endTime").val(offline.kaEndTime);
            $(".offline-budge").val(offline.kaBudge);
            var product = offline.kaProductName.split(",");
            var imgArr1 = offline.kaProductCode.split(",");
            var productHtml = "";
            for (var j = 0; j < product.length; j++) {
                productHtml += '<div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">商品名称</label></div>' +
                    '<div class="weui-cell__bd"><input class="weui-input personnelBtn" type="text" value="' + product[j] + '" readonly="readonly">' +
                    '</div></div>';
                productHtml += ' <div class="weui-cell"><div class="weui-cell__bd" style="margin:0;"> <div class="weui-uploader"> <div class="weui-uploader__hd"> <p class="weui-uploader__title">商品条码 </p> </div> <div class="weui-uploader__bd"> <ul class="card-content-inner fileCode"> <li id="img-div"><img src="/' + imgArr1[j] + '"></li> </ul> </div> </div> </div> </div>'
            }
            $(".product-weui-cell").html(productHtml);
            //城市合伙人的活动筹备中的详情页面
            if (insertStatus == 1) {
                $("#partnerDetail").show();
                //判断是否加入活动
                if (offline.cityStatus == 0) {
                    //未加入活动
                    $("#partnerBtn").text("参与活动");
                    //跳转到选择门店，填写活动信息
                    $("#partnerDetail").click(function () {
                        var id = $(".swiper-slide").attr('data-id');
                        var url = $(this).attr('data-url');
                        url = url + "?id=" + id;
                        window.location.href = url;
                    });
                } else if (offline.cityStatus == 1) {//加入活动完
                    $("#partnerBtn").text("已经参加活动等待配货送达");
                    //跳转到门店列表
                    $("#partnerDetail").click(function () {
                        var id = $(".swiper-slide").attr('data-id');
                        var url = $(this).attr('data-kaUrl');
                        url = url + "?id=" + id + '&insertStatus=' + insertStatus;
                        window.location.href = url;
                    });
                }
                //城市合伙人的活动进行中的详情页面
            } else if (insertStatus == 2 && acStatus == 1) {
                $("#partnerDetail").show();
                $("#partnerBtn").text("上报数据");
                //跳转到门店列表
                $("#partnerDetail").click(function () {
                    var id = $(".swiper-slide").attr('data-id');
                    var url = $(this).attr('data-kaUrl');
                    url = url + "?id=" + id + '&insertStatus=' + insertStatus;
                    window.location.href = url;
                });
                //城市合伙人的活动结束后的详情页面
            } else if (insertStatus == 2 && acStatus == 0) {
                $("#partnerDetail").hide();
            } else {
                $("#partnerDetail").show();
                $("#partnerBtn").text("查看详情");
                //跳转到门店列表
                $("#partnerDetail").click(function () {
                    var id = $(".swiper-slide").attr('data-id');
                    var url = $(this).attr('data-kaUrl');
                    url = url + "?id=" + id + '&insertStatus=' + insertStatus;
                    window.location.href = url;
                });
            }
        }
    })
}