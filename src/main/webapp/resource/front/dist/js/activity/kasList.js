function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
$(".title").text('店铺列表');
var activityID = getQueryString("id");
var cityID = getQueryString("cityID");
var insertStatus = getQueryString("insertStatus");
//查询ka经理的门店列表
$.ajax({
    type: "GET",
    url: "/rest/front/activitycharge/selectKaStoreList",
    data: {
        activityID: activityID,
        cityID: cityID
    },
    dataType: "json",
    success: function (data) {
        var list = data.data;
        var listHtml = "";
        $.each(list, function (i, o) {
            if (insertStatus == 1) {
                listHtml += '<div class="information-weui-cells weui-cells" > <a class="weui-cell weui-cell_access" href="javascript:;"><div class="weui-cell__bd">' +
                    '<p>' + o.storeName + '</p>' +
                    '<p>地址：' + o.storeAddress + '</p>' +
                    '</div></a>' +
                    '<div class="images-weui-cell  weui-cell"><div class="weui-cell__bd">' +
                    '<p>费用：￥' + o.costAmount + '</p></div>' +
                    '<div class="weui-cell__ft"><p class="detailBtn" data-id="' + o.storeID + '" data-userID="'+o.id+'">查看详情</p></div>' +
                    '</div></div>';
            } else {
                listHtml += '<div class="information-weui-cells weui-cells" > <a class="weui-cell weui-cell_access" href="javascript:;"><div class="weui-cell__bd">' +
                    '<p>' + o.storeName + '</p>' +
                    '<p>地址：' + o.storeAddress + '</p>' +
                    '</div></a>' +
                    '<div class="images-weui-cell  weui-cell"><div class="weui-cell__bd">' +
                    '<p>费用：￥' + o.costAmount + '</p></div>' +
                    '<div class="weui-cell__ft"><p class="pictureBtn" data-storeID="' + o.storeID + '">查看图片</p>&nbsp;&nbsp;&nbsp;&nbsp;<p class="detailBtn" data-id="' + o.storeID + '" data-userID="'+o.id+'">查看详情</p></div>' +
                    '</div></div>';
            }
        });
        $(".kaStoreList").html(listHtml);
        //跳转到查看图片详情页面
        $(".pictureBtn").click(function () {
            var id = activityID;
            var city = cityID;
            var storeID = $(this).attr('data-storeID');
            var pictureUrl = $(".kaStoreList").attr('data-pictureUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID;
            window.location.href = pictureUrl;
        });
        //跳转到查看详情页面，也是活动数据页面
        $(".detailBtn").click(function () {
            var id = activityID;
            var city = cityID;
            var storeID = $(this).attr('data-id');
            var userID= $(this).attr('data-userID');
            var url = $(".kaStoreList").attr('data-chargeUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID + "&insertStatus=" + insertStatus+"&userID=" + userID;
            window.location.href = url;
        })
    }
});
//查询ka经理的总金额
$.ajax({
    type: "GET",
    url: "/rest/front/activitycharge/selectAmountList",
    data: {
        activityID: activityID,
        cityID: cityID
    },
    dataType: "json",
    success: function (data) {
        var list = data.data;
        var totalCostHtml = "";
        var totalGoodsHtml = "";
        totalCostHtml += '<span>推广总费用：' + list.chargeAmount + '</span>';
        totalGoodsHtml += '<span>总补货：' + list.cargoAmount + '箱</span>';
        $(".totalCost").html(totalCostHtml);
        $(".totalGoods").html(totalGoodsHtml);
    }
});

