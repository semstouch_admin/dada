/**
 * Created by Administrator on 2017/8/30.
 */
//获取登陆用户缓存的值，存入
$.ajax({
    type: 'GET',
    dataType: 'json',
    url: '/rest/front/activity/getSession',
    success: function (data) {
        sessionStorage.id = data.data.user.id;
        var str = JSON.stringify(data.data.user);
        sessionStorage.obj = str;
        sessionStorage.roleType = data.data.user.rid;
        //查询名称
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/rest/front/activity/selectUser',
            success: function (data) {
                $(".title").text(data.data.nickname);
            }
        });
        if (sessionStorage.roleType == 2) {
            //品牌商的查询活动列表
            queryBrandOffline();
        } else if (sessionStorage.roleType == 3) {
            //KA经理的查询活动列表
            queryKAOffline();

        } else if (sessionStorage.roleType == 4) {
            //城市合伙人的查询活动列表
            queryPartnerOffline();
        }
    }
});

//查询品牌商的活动列表
function queryBrandOffline() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/activity/selectActivityList',
        success: function (data) {
            var activityList = data.data,
                activityHtml = '';
            if (activityList == null || activityList == '') {
                $('.img-div').show();
            } else {
                var month = '';
                var year = '';
                $.each(activityList, function (i, o) {
                    var year1 = o.starTime.substr(0, 4);//截取年份
                    var mon = o.starTime.slice(5, 7);//截取月份
                    if ((mon != "" && month != mon) || (year1 != "" && year != year1)) {
                        month = mon;
                        year = year1;
                        var monIndividual = o.starTime.slice(5, 6);//截取月份的个位数
                        var monTen = o.starTime.slice(6, 7);//截取月份的十位数
                        if (monIndividual == 0) {
                            activityHtml += ' <div class="month-div"><span><div></div><p>' + monTen + '月</p></span></div>';
                        } else {
                            activityHtml += ' <div class="month-div"><span><div></div><p>' + mon + '月</p></span></div>';
                        }
                    }
                    activityHtml += '<div class="month-weui-cells weui-cells" data-id="' + o.id + '" data-status="' + o.insertStatus + '">' +
                        '<h5>' + o.name + '</h5>';
                    var imgArr = o.picture.split(',');
                    activityHtml += '<img src="/' + imgArr[0] + '" alt="活动图片">';
                    if (o.insertStatus == "1") {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/Preparation.png" alt="筹备中"></div>';
                    } else if (o.insertStatus == "2") {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/progress.png" alt="进行中"></div>';
                    } else {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/end.png" alt="已结束"></div>';
                    }
                    activityHtml += '<a class="weui-cell weui-cell_access" href="/rest/front/activity/toActivityDetail?id=' + o.id + '&status=' + o.insertStatus + '">';
                    if (o.storeMember == "0") {
                        activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div>';
                    } else {
                        activityHtml += '<div class="weui-cell__bd"><p>' + o.storeMember + '家门店参与了活动</p></div>';
                    }
                    activityHtml += '<div class="weui-cell__ft detailBtn">查看详情</div></a></div>';

                });

                $(".offline-month").html(activityHtml);
            }
        }
    })
}
//KA经理的查询活动列表
function queryKAOffline() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/activity/selectActivityList',
        success: function (data) {
            var activity = data.data,
                activityHtml = '';
            if (activity == null || activity == '') {
                $('.img-div').show();
            } else {
                var monthKA = '';
                var yearKA = '';
                $.each(activity, function (i, o) {
                    var year1KA = o.kaStarTime.substr(0, 4);//截取年份
                    var monKA = o.kaStarTime.slice(5, 7);//截取月份
                    if ((monKA != "" && monthKA != monKA) || (year1KA != "" && yearKA != year1KA)) {
                        monthKA = monKA;
                        yearKA = year1KA;
                        var monIndividualKA = o.kaStarTime.slice(5, 6);//截取月份的个位数
                        var monTenKA = o.kaStarTime.slice(6, 7);//截取月份的十位数
                        if (monIndividualKA == 0) {
                            activityHtml += ' <div class="month-div"><span><div></div><p>' + monTenKA + '月</p></span></div>';
                        } else {
                            activityHtml += ' <div class="month-div"><span><div></div><p>' + monKA + '月</p></span></div>';
                        }
                    }
                    activityHtml += '<div class="month-weui-cells weui-cells" data-id="' + o.id + '" data-status="' + o.insertStatus + '">' +
                        '<h5>' + o.kaName + '</h5>';
                    var imgArr = o.kaPicture.split(',');
                    activityHtml += '<img src="/' + imgArr[0] + '" alt="活动图片">';
                    if (o.insertStatus == "1") {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/Preparation.png" alt="筹备中"></div>';
                    } else if (o.insertStatus == "2") {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/progress.png" alt="进行中"></div>';
                    } else {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/end.png" alt="已结束"></div>';
                    }
                    activityHtml += '<a class="weui-cell weui-cell_access" href="/rest/front/activity/toActivityDetail?id=' + o.id + '&status=' + o.insertStatus + '&updateStatus=' + o.updateStatus + '">';
                    if (o.storeMember == "0") {
                        if (o.insertStatus == "1" && o.updateStatus == "0") {
                            activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div><div class="weui-cell__ft detailBtn">编辑发布</div></a></div>';
                        } else {
                            activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div><div class="weui-cell__ft detailBtn">查看详情</div></a></div>';
                        }
                    } else {
                        if (o.insertStatus == "1" && o.updateStatus == "0") {
                            activityHtml += '<div class="weui-cell__bd"><p>共' + o.storeMember + '家门店参与</p></div><div class="weui-cell__ft detailBtn">编辑发布</div></a></div>';
                        } else {
                            activityHtml += '<div class="weui-cell__bd"><p>共' + o.storeMember + '家门店参与</p><p>费用：￥' + o.chargeAmount + '</p></div><div class="weui-cell__ft detailBtn">查看详情</div></a></div>';
                        }
                    }
                });

                $(".offline-month").html(activityHtml);
            }
        }
    })
}
//城市合伙人的查询活动列表
function queryPartnerOffline() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/rest/front/activity/selectCityActivityList',
        success: function (data) {
            var activity = data.data,
                activityHtml = '';
            if (activity == null || activity == '') {
                $('.img-div').show();
            } else {
                var monthKA = '';
                var yearKA = '';
                $.each(activity, function (i, o) {
                    var year1KA = o.kaStarTime.substr(0, 4);//截取年份
                    var monKA = o.kaStarTime.slice(5, 7);//截取月份
                    if ((monKA != "" && monthKA != monKA) || (year1KA != "" && yearKA != year1KA)) {
                        monthKA = monKA;
                        yearKA = year1KA;
                        var monIndividualKA = o.kaStarTime.slice(5, 6);//截取月份的个位数
                        var monTenKA = o.kaStarTime.slice(6, 7);//截取月份的十位数
                        if (monIndividualKA == 0) {
                            activityHtml += ' <div class="month-div"><span><div></div><p>' + monTenKA + '月</p></span></div>';
                        } else {
                            activityHtml += ' <div class="month-div"><span><div></div><p>' + monKA + '月</p></span></div>';
                        }
                    }
                    activityHtml += '<div class="month-weui-cells weui-cells" data-id="' + o.id + '" data-status="' + o.insertStatus + '">' +
                        '<h5>' + o.kaName + '</h5>';
                    var imgArr = o.kaPicture.split(',');
                    activityHtml += '<img src="/' + imgArr[0] + '" alt="活动图片">';
                    if (o.insertStatus == "1") {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/Preparation.png" alt="筹备中"></div>';
                    } else if (o.insertStatus == "2") {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/progress.png" alt="进行中"></div>';
                    } else {
                        activityHtml += '<div class="ribbon"><img src="/resource/front/dist/images/end.png" alt="已结束"></div>';
                    }
                    activityHtml += '<a class="weui-cell weui-cell_access" href="/rest/front/activity/toActivityDetail?id=' + o.id + '&status=' + o.insertStatus + '&acStatus=' + o.acStatus + '">';
                    if (o.storeMember == "0") {
                        if (o.insertStatus == "1" && o.acStatus == "0") {
                            activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div><div class="weui-cell__ft detailBtn">加入活动</div></a></div>';
                        } else if (o.insertStatus == "1" && o.acStatus == "1") {
                            activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div><div class="weui-cell__ft detailBtn">查看活动</div></a></div>';
                        } else if (o.insertStatus == "2" && o.acStatus == "1") {
                            activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div><div class="weui-cell__ft detailBtn">上报数据</div></a></div>';
                        } else if (o.insertStatus == "2" && o.acStatus == "0") {//还没参加活动，得查看活动，acStatus=0
                            activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div><div class="weui-cell__ft detailBtn">查看活动</div></a></div>';
                        } else {
                            activityHtml += '<div class="weui-cell__bd"><p>等待门店加入</p></div><div class="weui-cell__ft detailBtn">查看详情</div></a></div>';
                        }
                    } else {
                        if (o.insertStatus == "1" && o.acStatus == "0") {
                            activityHtml += '<div class="weui-cell__bd"><p>' + o.storeMember + '家门店正在参与活动</p></div><div class="weui-cell__ft detailBtn">加入活动</div></a></div>';
                        } else if (o.insertStatus == "1" && o.acStatus == "1") {
                            activityHtml += '<div class="weui-cell__bd"><p>' + o.storeMember + '家门店正在参与活动</p></div><div class="weui-cell__ft detailBtn">查看活动</div></a></div>';
                        } else if (o.insertStatus == "2" && o.acStatus == "1") {
                            activityHtml += '<div class="weui-cell__bd"><p>' + o.storeMember + '家门店正在参与活动</p></div><div class="weui-cell__ft detailBtn">上报数据</div></a></div>';
                        } else if (o.insertStatus == "2" && o.acStatus == "0") {
                            activityHtml += '<div class="weui-cell__bd"><p>' + o.storeMember + '家门店正在参与活动</p></div><div class="weui-cell__ft detailBtn">查看活动</div></a></div>';
                        } else {
                            activityHtml += '<div class="weui-cell__bd"><p>' + o.storeMember + '家门店正在参与活动</p></div><div class="weui-cell__ft detailBtn">查看详情</div></a></div>';
                        }
                    }

                });

                $(".offline-month").html(activityHtml);
            }
        }
    })
}
