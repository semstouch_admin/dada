/**
 * Created by Administrator on 2017/9/3.
 */
//拿KA经理的
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
function getQueryString1(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
if (sessionStorage.roleType == 3) {
    $(".title").text('店铺列表');
    var activityID = getQueryString("id");
    var cityID = getQueryString("cityID");
    var insertStatus = getQueryString("status");
    queryKa();
    getkA();
} else if (sessionStorage.roleType == 4) {
    var activityID1 = getQueryString1("id");
    var insertStatus1 = getQueryString1("insertStatus");
    $(".title").text('门店列表');
    //判断城市合伙人活动的状态
    $(".pull-left").click(function () {
        var id = activityID1;
        var status=insertStatus1 ;
        var url = $(this).attr('data-url') + "?id=" + id + "&status=" + status;
        window.location.href = url;
    });
    if (insertStatus1 == 2 || insertStatus1 == 1) {
        //城市合伙人进行中活动查询门店列表需要上传活动照片
        queryPartner1();
    } else {
        //城市合伙人进行中活动查询门店列表查看活动照片与信息
        queryPartner2();

    }
}
//查询ka经理的门店列表
function queryKa() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectKaStoreList",
        data: {
            activityID: activityID,
            cityID: cityID
        },
        dataType: "json",
        success: function (data) {
            var list = data.data;
            var listHtml = "";
            $.each(list, function (i, o) {
                listHtml += '<div class="information-weui-cells weui-cells" > <a class="weui-cell weui-cell_access" href="javascript:;"><div class="weui-cell__bd">' +
                    '<p>' + o.storeName + '</p>' +
                    '<p>地址：' + o.storeAddress + '</p>' +
                    '</div></a>' +
                    '<div class="images-weui-cell  weui-cell"><div class="weui-cell__bd">' +
                    '<p>费用：￥' + o.costAmount + '</p></div>' +
                    '<div class="weui-cell__ft"><p class="pictureBtn" data-storeID="' + o.storeID + '">查看图片</p>&nbsp;&nbsp;&nbsp;&nbsp;<p class="detailBtn" data-id="' + o.storeID + '">查看详情</p></div>' +
                    '</div></div>';
            });
            $(".kaStoreList").html(listHtml);
            //跳转到查看图片详情页面
            $(".pictureBtn").click(function () {
                var id = activityID;
                var city = cityID;
                var storeID = $(this).attr('data-storeID');
                var pictureUrl = $(".kaStoreList").attr('data-pictureUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID;
                window.location.href = pictureUrl;
            });
            //跳转到查看详情页面，也是活动数据页面
            $(".detailBtn").click(function () {
                var id = activityID;
                var city = cityID;
                var storeID = $(this).attr('data-id');
                var url = $(".kaStoreList").attr('data-chargeUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID;
                window.location.href = url;
            })
        }
    });
}
//查询ka经理的总金额
function getkA() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectAmountList",
        data: {
            activityID: activityID,
            cityID: cityID
        },
        dataType: "json",
        success: function (data) {
            var list = data.data;
            var totalCostHtml = "";
            var totalGoodsHtml = "";
            totalCostHtml += '<span>推广总费用：' + list.chargeAmount + '</span>';
            totalGoodsHtml += '<span>总补货：' + list.cargoAmount + '箱</span>';
            $(".totalCost").html(totalCostHtml);
            $(".totalGoods").html(totalGoodsHtml);
        }
    });
}
//城市合伙人进行中活动查询门店列表需要上传活动照片
function queryPartner1() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectCsList",
        data: {
            activityID: activityID1,
            cityID: obj.id
        },
        dataType: "json",
        success: function (data) {
            var list = data.data;
            var listHtml = "";
            if (insertStatus1 == 1) {
                $.each(list, function (i, o) {
                    listHtml += '<div class="information-weui-cells weui-cells"> <a class="weui-cell weui-cell_access" href="javascript:;" data-id="' + o.storeID + '"><div class="weui-cell__bd">' +
                        '<p>' + o.storeName + '</p>' +
                        '<p>地址：' + o.storeAddress + '</p>' +
                        '</div><div class="weui-cell__ft"></div></a>' +
                        '</div></div>';
                });
            } else if (insertStatus1 == 2) {
                $.each(list, function (i, o) {
                    listHtml += '<div class="information-weui-cells weui-cells"> <a class="weui-cell weui-cell_access" href="javascript:;" data-id="' + o.storeID + '"><div class="weui-cell__bd">' +
                        '<p>' + o.storeName + '</p>' +
                        '<p>地址：' + o.storeAddress + '</p>' +
                        '</div><div class="weui-cell__ft"></div></a>' +
                        '<div class="images-weui-cell  weui-cell"><div class="weui-cell__bd">' +
                        '<p>已上传' + o.pictureNumber + '次活动照片</p></div>' +
                        //'<div class="weui-cell__ft"><p class="signBtn" data-storeID="' + o.storeID + '">签到详情</p><p class="pictureBtn" data-pictureID="' + o.storeID + '">上传图片</p></div>' +
                        '<div class="weui-cell__ft"><p class="pictureBtn" data-pictureID="' + o.storeID + '">上传图片</p></div>' +
                        '</div></div>';
                });
            }
            $(".kaStoreList").html(listHtml);
            //跳转到上传图片页面
            $(".pictureBtn").click(function () {
                var id = activityID1;
                var city = obj.id;
                var storeID = $(this).attr('data-pictureID');
                var insertStatus = insertStatus1;
                var pictureUrl = $(".kaStoreList").attr('data-pictureUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID + "&insertStatus=" + insertStatus;
                window.location.href = pictureUrl;
            });
            ////跳转到签到详情页面
            //$(".signBtn").click(function () {
            //    var id = activityID1;
            //    var city = obj.id;
            //    var storeID = $(this).attr('data-storeID');
            //    var signUrl = $(".kaStoreList").attr('data-signUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID;
            //    window.location.href = signUrl;
            //});
            //跳转到查看详情页面，也是活动数据页面
            $(".weui-cell_access").click(function () {
                var id = activityID1;
                var city = obj.id;
                var storeID = $(this).attr('data-id');
                var url = $(".kaStoreList").attr('data-chargeUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID;
                window.location.href = url;
            })
        }
    });
}
//城市合伙人进行中活动查询门店列表需要上传活动照片
function queryPartner2() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectCsList",
        data: {
            activityID: activityID1,
            cityID: obj.id
        },
        dataType: "json",
        success: function (data) {
            var list = data.data;
            var listHtml = "";
            $.each(list, function (i, o) {
                listHtml += '<div class="information-weui-cells weui-cells"> <a class="weui-cell weui-cell_access" href="javascript:;" data-id="' + o.storeID + '"><div class="weui-cell__bd">' +
                    '<p>' + o.storeName + '</p>' +
                    '<p>地址：' + o.storeAddress + '</p>' +
                    '</div><div class="weui-cell__ft"></div></a>' +
                    '<div class="images-weui-cell  weui-cell"><div class="weui-cell__bd">' +
                    '<p>已上传' + o.pictureNumber + '次活动照片</p></div>' +
                    '<div class="weui-cell__ft"><p class="pictureBtn" data-storeID="' + o.storeID + '">查看图片</p></div>' +
                    '</div></div>';
            });
            $(".kaStoreList").html(listHtml);
            //跳转到查看图片详情页面
            $(".pictureBtn").click(function () {
                var id = activityID1;
                var city = obj.id;
                var storeID = $(this).attr('data-storeID');
                var insertStatus = insertStatus1;
                var pictureUrl = $(".kaStoreList").attr('data-pictureUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID + "&insertStatus=" + insertStatus;
                window.location.href = pictureUrl;
            });
            //跳转到查看详情页面，也是活动数据页面
            $(".weui-cell_access").click(function () {
                var id = activityID1;
                var city = obj.id;
                var storeID = $(this).attr('data-id');
                var url = $(".kaStoreList").attr('data-chargeUrl') + "?id=" + id + "&city=" + city + "&storeID=" + storeID;
                window.location.href = url;
            })
        }
    });
}





