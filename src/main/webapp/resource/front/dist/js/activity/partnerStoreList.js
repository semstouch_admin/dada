/**
 * Created by Administrator on 2017/9/4.
 */
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var activityID = getQueryString("id");
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
var storeIDs = new Array();
var j = 0;
$.ajax({
    type: "GET",
    url: "/rest/front/csrelation/selectStoreList",
    data: {
        cityID: obj.id,
        activityID: activityID
    },
    dataType: "json",
    success: function (data) {
        var list = data.data;
        if (list == null || list == '') {
            $('.img-div').show();
        } else {
            var listHtml = "";
            $.each(list, function (i, o) {
                listHtml += '<a class="weui-cell weui-cell_access" href="#" data-id="' + o.id + '" data-acStatus="' + o.acStatus + '"><div class="weui-cell__bd">' +
                    '<p>' + o.storeName + '</p><p>地址：' + o.storeAddress + '</p></div>';
                if (o.acStatus == "0") {
                    listHtml += '<div class="weui-cell__ft">未填写</div></a>';
                } else if (o.acStatus == "1") {
                    storeIDs[j] = o.id;
                    j++;
                    listHtml += '<div class="weui-cell__ft">已填写</div></a>';
                }
            });

            $(".information-weui-cells").html(listHtml);
            $(".weui-cell_access").click(function () {
                var storeID = $(this).attr('data-id');
                var id = activityID;
                var acStatus = $(this).attr('data-acStatus');
                var url = $(".information-weui-cells").attr('data-url');
                window.location.href = url + "?storeID=" + storeID + '&id=' + id + '&acStatus=' + acStatus;
            })
        }
    }
});
var prop = 0;//全局变量一个值;
$(".addBtn").click(function () {
    if (prop == 0) {
        prop = 1;
        $.ajax({
            type: "post",
            dataType: "json",
            url: "/rest/front/activitycharge/updateCommitStatus",
            data: {
                activityID: activityID,
                cityID: obj.id,
                storeIDs: storeIDs
            },
            success: function (data) {
                if (data.success) {
                    var $toast = $('#toast');
                    $toast.fadeIn();
                    setTimeout(function () {
                        $toast.fadeOut();
                        window.location.href = "/rest/front/activity/toActivityList";
                    }, 1000);
                } else {
                    weui.alert("提交失败");
                }
            }
        })
    }
});

