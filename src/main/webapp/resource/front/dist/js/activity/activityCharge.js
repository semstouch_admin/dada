function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var activityID = getQueryString("id");
var cityID = getQueryString("city");
var storeID = getQueryString("storeID");
var userID = getQueryString("userID");
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
if (sessionStorage.roleType == 3) {//KA经理角色
    queryKa();
} else if (sessionStorage.roleType == 4) {//城市合伙人
    queryPartner();
}
function queryKa() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectActivityCharge",
        data: {
            activityID: activityID,
            cityID: cityID,
            storeID: storeID
        },
        dataType: "json",
        success: function (data) {
            //堆头费用
            $(".stackBtn").val(data.data.tgCost);
            //场地费用
            $(".fieldBtn").val(data.data.siteCost);
            //补货数量
            $(".goodsBtn").val(data.data.productAmount);
            //促销员费用
            var personCostArr = data.data.personCost.split(",");
            var personHtml = "";
            for (var i = 0; i < personCostArr.length; i++) {
                personHtml += '<div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">促销员费用</label></div>' +
                    '<div class="weui-cell__bd"><input class="weui-input personnelBtn" type="number" value="' + personCostArr[i] + '" /></div></div>';

            }
            $(".personList").html(personHtml);
        }
    });
}
var person = '';
//点击保存
$("#editBtn").click(function () {
    var siteCost = $(".fieldBtn").val(),
        tgCost= $(".stackBtn").val(),
        productAmount = $(".goodsBtn").val();
    //遍历每个促销员的值
    $(".personnelBtn").each(function () {
        person += $(this).val() + ",";
    });
    person = person.substr(0, person.length - 1);
    if (tgCost == '' || siteCost == '' || productAmount == '' || person == '') {
        //信息填写不完整提示
        $("#dialogInformation").css("display", "block");
        $(".weui-dialog__btn").click(function () {
            $("#dialogInformation").css("display", "none");
        })
    } else {
        $.ajax({
            type: "POST",
            url: "/rest/front/activitycharge/updateJson",
            data: {
                id:userID,
                cityID: cityID,
                activityID: activityID,
                storeID: storeID,
                tgCost: tgCost,
                siteCost: siteCost,
                productAmount: productAmount,
                personCost: person
            },
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    weui.alert("成功");
                    setTimeout(function () {
                        window.history.go(-1);
                    }, 1000);
                } else {
                    weui.alert("失败");
                }
            }
        });
    }
});
function queryPartner() {
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectActivityCharge",
        data: {
            activityID: activityID,
            cityID: cityID,
            storeID: storeID
        },
        dataType: "json",
        success: function (data) {
            //堆头费用
            $(".stackBtn").val(data.data.tgCost);
            $(".stackBtn").attr("readonly", true);
            //场地费用
            $(".fieldBtn").val(data.data.siteCost);
            $(".fieldBtn").attr("readonly", true);
            //补货数量
            $(".goodsBtn").val(data.data.productAmount);
            $(".goodsBtn").attr("readonly", true);
            $("#beginDetail").hide();//隐藏保存按钮
            //促销员费用
            var personCostArr = data.data.personCost.split(",");
            var personHtml = "";
            for (var i = 0; i < personCostArr.length; i++) {
                personHtml += '<div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">促销员费用</label></div>' +
                    '<div class="weui-cell__bd"><input class="weui-input" type="number" value="' + personCostArr[i] + '" readonly/></div></div>';

            }
            $(".personList").html(personHtml);
        }
    });
}
