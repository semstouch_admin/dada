/**
 * Created by Administrator on 2017/9/3.
 */

function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var activityID = getQueryString("activityID");
var status = getQueryString("status");
var selections = '';
var cityIDs = new Array();
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
//未选择时，查看全部城市合伙人列表以及去添加城市合伙人
$.ajax({
    type: "GET",
    url: "/rest/front/kcrelation/selectCityList",
    dataType: "json",
    success: function (data) {
        var manList = data.data;
        if (manList == null || manList == '') {
            $('.img-div').show();
        } else {
            var manHtml = "";
            var selectID = '';

            $.each(manList, function (i, o) {
                i++;
                manHtml += '<label class="weui-cell weui-check__label" for="' + i + '" ><div class="weui-cell__hd"><input type="checkbox" class="weui-check" name="choose" id="' + i + '" data-id="' + o.cityID + '" /> <i class="weui-icon-checked"></i> </div>' +
                    '<div class="weui-cell__bd"> <p>' + o.cityName + '</p> </div> </label>';
                selectID += o.cityID + " ";
            });

        $(".weui-cells_checkbox").html(manHtml);
        }
        //点击全选时塞值
        $(".allChecked").click(function () {
            selections = selectID.replace(/(\s*$)/g, "");//去除字符串空格右边
            cityIDs = selections.split(" ");
        });
        //checkbox 全选/取消全选
        var isCheckAll = false;
        $(".allChecked").click(function () {
            if (isCheckAll) {
                $("input[type='checkbox']").each(function () {
                    this.checked = false;
                });
                isCheckAll = false;
            } else {
                $("input[type='checkbox']").each(function () {
                    this.checked = true;
                });
                isCheckAll = true;
            }
        });
        var texts = [];
        //多选input的值
        $('.weui-check').change(function () {
            texts = [];
            $(".weui-check:checked").each(function () {
                texts.push($(this).closest("input").attr("data-id"));
            });
            cityIDs = texts;
        });

    }
});
if (status == 0 || status == 1) {
    $("#saveBtn").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/rest/front/activityjoin/updateCityList',
            data: {
                cityIDs: cityIDs,
                activityID: activityID
            },
            success: function (data) {
                var id = activityID;
                var ids = cityIDs;
                var url = $("#saveBtn").attr('data-url');
                url = url + "?id=" + id + "&ids=" + ids;
                window.location.href = url;
            }
        })
    });
} else {
    $("#saveBtn").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/rest/front/activity/insertActivityjoin',
            data: {
                activityID: activityID,
                cityIDs: cityIDs
            },
            success: function (data) {
                var id = activityID;
                var ids = cityIDs;
                var url = $("#saveBtn").attr('data-url');
                url = url + "?id=" + id + "&ids=" + ids;
                window.location.href = url;
            }
        })
    });
}


