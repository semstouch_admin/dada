function GetRequest() {
    var url = decodeURI(decodeURI(location.search)); //获取url中"?"符后的字串，使用了两次decodeRUI解码
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
        return theRequest;
    }
}
var postData = GetRequest();
var activityID = postData.id;//获取该活动的地区选择
var storeID = postData.storeID;//获取该活动的地区选择
var acStatus = postData.acStatus;//获取该活动的地区选择
var person = '';
var obj = JSON.parse(sessionStorage.obj);//将字符串转换成对象
if (acStatus == "0") {
    $(".title").text("填写信息");
    $(".personnelList").show();
    $(".saveBtn").click(function () {
        var siteCost = $(".fieldBtn").val(),
            tgCost = $(".duitouBtn").val(),
            productAmount = $(".goodsBtn").val();
        //遍历每个促销员的值
        $(".personnelBtn").each(function () {
            person += $(this).val() + ",";
        });
        person = person.substr(0, person.length - 1);
        if (tgCost == '' || siteCost == '' || productAmount == '' || person == '') {
            //信息填写不完整提示
            $("#dialogInformation").css("display", "block");
            $(".weui-dialog__btn").click(function () {
                $("#dialogInformation").css("display", "none");
            })
        } else {
            $.ajax({
                type: "POST",
                url: "/rest/front/activitycharge/insertActivitycharge",
                data: {
                    cityID: obj.id,
                    activityID: activityID,
                    storeID: storeID,
                    tgCost: tgCost,
                    siteCost: siteCost,
                    productAmount: productAmount,
                    personCost: person
                },
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        weui.alert("成功");
                        setTimeout(function () {
                            var id = activityID;
                            window.location.href = '/rest/front/activity/toCityStoreList?id=' + id + '';
                        }, 1000);
                    } else {
                        weui.alert("失败");
                    }
                }
            });
        }
    });
} else if (acStatus == "1") {
    $(".title").text("信息详情");
    var id1 = '';
    var person1 = '';
    $.ajax({
        type: "GET",
        url: "/rest/front/activitycharge/selectActivityCharge",
        data: {
            activityID: activityID,
            cityID: obj.id,
            storeID: storeID
        },
        dataType: "json",
        success: function (data) {
            if (data.data == '' || data.data == null) {
                return;
            }
            id1 = data.data.id;
            //堆头费用
            $(".duitouBtn").val(data.data.tgCost);
            //场地费用
            $(".fieldBtn").val(data.data.siteCost);
            //补货数量
            $(".goodsBtn").val(data.data.productAmount);
            //促销员费用
            var personCostArr = data.data.personCost.split(",");
            var personHtml = "";
            for (var i = 0; i < personCostArr.length; i++) {
                if (i == 0) {
                    personHtml += '<div class="weui-cell"><div class="weui-cell__hd"><label class="weui-label">促销员费用</label></div>' +
                        '<div class="weui-cell__bd"><input class="weui-input personnelBtn" type="number" value="' + personCostArr[i] + '"  style="width: 58%;height: 2.2em;float: left;" onkeyup="value=value.replace(/[^\d.]/g,"")"/>' +
                        '<img class="delBtn" src="/resource/front/dist/images/reduceBtn.png" width="30" style="float: left;"><img class="addBtn" src="/resource/front/dist/images/jiaBtn.png" width="30" style="float: right"></div></div>';

                } else {
                    personHtml += '<div class="weui-cell ab"><div class="weui-cell__hd"><label class="weui-label">促销员费用</label></div>' +
                        '<div class="weui-cell__bd"><input class="weui-input personnelBtn" type="number" value="' + personCostArr[i] + '"  style="width: 58%;height: 2.2em;float: left;" onkeyup="value=value.replace(/[^\d.]/g,"")"/>' +
                        '</div></div>';
                }
            }
            $(".personList2").html(personHtml);
            $(".addBtn").click(function () {
                            $(".weui-cells_form").append('<div class="weui-cell sales-weui-cell"><div class="weui-cell__hd"><label class="weui-label">促销员</label></div><div class="weui-cell__bd"><input class="weui-input personnelBtn" type="text" value="" placeholder="输入工资" name="personCost" onkeyup="clearNoNum(this)"/></div></div>');

                        });
            $(".delBtn").click(function () {
                $(".ab:last").remove();
            });
            //点击数字
            $(".personnelBtn").keyup(function () {
                var reg = $(this).val().match(/\d+\.?\d{0,2}/);
                var txt = '';
                if (reg != null) {
                    txt = reg[0];
                }
                $(this).val(txt);
            }).change(function () {
                $(this).keyup();
            });

        }
    });
    //编辑接口
    $(".saveBtn").click(function () {
        var tgCost = $(".fieldBtn").val(),
            siteCost = $(".duitouBtn").val(),
            productAmount = $(".goodsBtn").val();
        //遍历每个促销员的值
        $(".personnelBtn").each(function () {
            person1 += $(this).val() + ",";
        });
        var p = person1.substr(0, person1.length - 1);
        var person2 = p.substr(0, p.length - 1);

        if (tgCost == '' || siteCost == '' || productAmount == '' || person2 == '') {
            //信息填写不完整提示
            $("#dialogInformation").css("display", "block");
            $(".weui-dialog__btn").click(function () {
                $("#dialogInformation").css("display", "none");
            })
        } else {
            $.ajax({
                type: "POST",
                url: "/rest/front/activitycharge/updateJson",
                data: {
                    id: id1,
                    tgCost: tgCost,
                    siteCost: siteCost,
                    productAmount: productAmount,
                    personCost: person2
                },
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        weui.alert("成功");
                        setTimeout(function () {
                            var id = activityID;
                            window.location.href = '/rest/front/activity/toCityStoreList?id=' + id + '';
                        }, 1000);
                    } else {
                        weui.alert("失败");
                    }

                }
            });
        }
    });
}

